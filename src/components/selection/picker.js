import React from "react"
import { connect } from "react-redux"
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Input,
	FormGroup,
	InputGroup,
} from "reactstrap"
import Flatpickr from "react-flatpickr"
import "flatpickr/dist/themes/light.css";
import "../../assets/scss/plugins/forms/flatpickr/flatpickr.scss"
import moment from "moment"

class DatePicker extends React.Component{
	state = {
	    dateFrom : new Date().fp_incr(-30),
	    dateTo: new Date(),
	    refreshed: false,
	  }

	componentDidUpdate(prevProps, prevState){
		// if(prevState.dateFrom !== this.props.dateFrom || prevState.dateTo !== this.props.dateTo){
		// 	console.log("OK")
		// 	this.refresh()
		// }
	}

	refresh = () => {
		this.setState({
			refreshed: true
		})

		setTimeout(() => {
			this.setState({
				refreshed: false
			})
		}, 10)
	}

	render(){
		let { 
          dateFrom,
          dateTo,
        } = this.state

		return (
			<Modal
	          isOpen={this.props.modal}
	          toggle={this.props.toggleModal}
	          className="modal-dialog-centered modal-sm"
	        >
	          <ModalBody className="pt-2 pl-2 pr-2" style={{
	          	minHeight: "231px"
	          }}>
	          	<h4>Select your custom date range!</h4>
	            <br />

	            {
	            	this.state.refreshed
	            	?
	            	null
	            	:
	            	<>
	            		<div>
			            	<h5 className="text-bold-500">From</h5>
				            <Flatpickr
				              className="form-control"
				              value={dateFrom}
				              options={{ maxDate: Date.parse(dateTo), dateFormat: "d M Y" }}
				              onChange={date => {
				                this.setState({ dateFrom : date });
				                this.refresh()
				              }}
				            />
			            </div>
			            <br />
			            <div>
			            	<h5 className="text-bold-500">To</h5>
				            <Flatpickr
				              className="form-control"
				              value={dateTo}
				              options={{ maxDate: new Date(), minDate: Date.parse(dateFrom), dateFormat: "d M Y" }}
				              onChange={date => {
				                this.setState({ dateTo : date });
				                this.refresh()
				              }}
				            />
			            </div>
	            	</>
	            }
	          </ModalBody>
	          <ModalFooter>
	            <Button onClick={this.props.toggleModal}>
	              Cancel
	            </Button>{" "}
	            <Button color="primary" onClick={() => {
	            	this.props.onSubmit && this.props.onSubmit({
	            		from: Date.parse(this.state.dateFrom),
	            		to: Date.parse(this.state.dateTo),
	            	})
	            }}>
	              Submit
	            </Button>{" "}
	          </ModalFooter>
	        </Modal>
		)
	}
}

export default connect(
	state => ({
		user: state.userForm.data,
	})
)(DatePicker)