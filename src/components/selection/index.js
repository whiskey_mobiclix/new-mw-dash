import React from "react"
import {
    UncontrolledButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from "reactstrap"
import { ChevronDown } from "react-feather"
import Picker from "./picker"

//CSS
import "./style.scss";

class Selection extends React.Component {
	state = {
		modal: false,
		dropdownOpen: false,
		selected: this.props.options.filter(o => o.value === this.props.value)[0] || {}
	}

	componentDidUpdate(prevProps){
		if(prevProps.value !== this.props.value){
			this.setState({
				selected: this.props.options.filter(o => o.value === this.props.value)[0] || {}
			})
		}
	}

	toggleDropdown = () => {
		this.setState({
			dropdownOpen: !this.state.dropdownOpen,
		})
	}

	handleSelect = item => {
		if(item.value === "custom-date-range"){
			this.toggleModal()
			return
		}

		if(item.value === this.state.selected.value) return;

		this.setState({
			selected: item
		})

		this.props.onChange && this.props.onChange(item);
	}

	toggleModal = () => {
		this.setState({
			modal: !this.state.modal,
		})
	}

    render() {
      let options = this.props.options;
      if(this.props.type === "date"){
      	options = options.concat({
      		name: "Add a custom date range",
      		value: "custom-date-range",
      	})
      }

      return(
        <div className={`dropdown mw-selection ${options.length === 1 ? "no-select" : ""}`}>
          	<UncontrolledButtonDropdown
	          isOpen={this.state.dropdownOpen}
	          toggle={this.toggleDropdown}
	        >
	          <DropdownToggle color="primary" caret>
	            {this.state.selected.name}
	            <ChevronDown size={15} />
	            </DropdownToggle>
	          {
	          	options.length > 1
	          	&&
	          	<DropdownMenu>
		          	{
		          		options.map((o, ix) =>
		          			<DropdownItem className={`${o.value === this.state.selected.value ? "selected" : ""} ${o.value==="custom-date-range" ? "custom" : ""}`}
			          			tag="a"
			          			onClick={() => {
			          				this.handleSelect(o)
			          			}} key={ix}
			          		>
			          			{o.name}
			          		</DropdownItem>
		          		)
		          	}

		        </DropdownMenu>
	          }
	        </UncontrolledButtonDropdown>

	        <Picker {...{
	        	modal: this.state.modal,
	        	toggleModal: this.toggleModal,
	        	onSubmit: e => {
	        		this.props.onCustomDateRange && this.props.onCustomDateRange(e)
	        		this.toggleModal()
	        	}
	        }}/>
        </div>
      )
    }
}
export default Selection