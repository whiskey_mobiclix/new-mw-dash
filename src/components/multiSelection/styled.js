import styled from 'styled-components';

const Styled = styled.div`
	display: inline-block;

	.btn-primary {
		border-color: #fff !important;
		background-color: #fff !important;
		color: #565656 !important;
		box-shadow: 0 8px 10px -10px #babfc7;

		&:hover {
			box-shadow: 0 8px 25px -8px #babfc7;
		}
	}

	.show > .dropdown-menu {
		min-width: 100%;

		.dropdown-item {
			padding-top: 5px !important;
			padding-bottom: 5px !important;
		}

		.group-title {
			font-weight: 600;
			display: inline-flex;
			width: 100%;
			justify-content: space-between;
			align-items: center;
		}

		.group-item {
			padding: 0 10px 0 45px !important;
		}

		&:hover {
			.vx-icon {
				color: #fff !important;
			}
		}
	}
`;

export default Styled;
