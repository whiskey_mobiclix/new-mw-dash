import React from 'react';
import {
	UncontrolledButtonDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem
} from 'reactstrap';
import * as Icon from 'react-feather';

import Checkbox from '../@vuexy/checkbox/CheckboxesVuexy';
import Group from './group';
import Item from './item';

import Styled from './styled';

class MultiSelection extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			dropdownOpen: false,
			selected: []
		};
	}

	setSelected = newSelected => {
		this.setState({
			...this.state,
			selected: newSelected
		});
	};

	toggleDropdown = () => {
		this.setState({
			dropdownOpen: !this.state.dropdownOpen
		});
	};

	render() {
		const { dropdownOpen, selected } = this.state;
		const { options } = this.props;

		return (
			<Styled className="dropdown mw-multi-selection">
				<UncontrolledButtonDropdown
					isOpen={dropdownOpen}
					toggle={this.toggleDropdown}
				>
					<DropdownToggle color="primary" caret>
						MultiSelection
						{dropdownOpen ? (
							<Icon.ChevronUp size={15} />
						) : (
							<Icon.ChevronDown size={15} />
						)}
					</DropdownToggle>
					<DropdownMenu>
						{!options[0].childrens &&
							options.map((o, ix) => (
								<Item
									object={o}
									key={ix}
									selected={selected}
									setSelected={this.setSelected}
								/>
							))}

						{options[0].childrens &&
							options.map((o, ix) => (
								<Group
									object={o}
									key={ix}
									selected={selected}
									setSelected={this.setSelected}
								/>
							))}
					</DropdownMenu>
				</UncontrolledButtonDropdown>
			</Styled>
		);
	}
}
export default MultiSelection;
