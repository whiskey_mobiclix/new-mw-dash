import React from 'react';
import { DropdownItem } from 'reactstrap';
import { cloneDeep, remove } from 'lodash';
import * as Icon from 'react-feather';

import Checkbox from '../../@vuexy/checkbox/CheckboxesVuexy';

import Styled from './styled';

class Item extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			itemIsShowed: false
		};
	}

	handleSelect = () => {
		if (
			this.props.selected.filter(o => o.value === this.props.object.value)
				.length <= 0
		) {
			this.props.setSelected([...this.props.selected, this.props.object]);
		} else {
			const temp = cloneDeep(this.props.selected);
			remove(temp, o => o.value === this.props.object.value);

			this.props.setSelected(temp);
		}
	};

	isChecked = () => {
		if (
			this.props.selected.filter(o => o.value === this.props.object.value)
				.length <= 0
		) {
			return false;
		}
		return true;
	};

	render() {
		const { object, setSelected } = this.props;
		const { itemIsShowed } = this.state;

		return (
			<Styled>
				<DropdownItem className="item" tag="a" toggle={false}>
					<Checkbox
						color="primary"
						icon={<Icon.Check className="vx-icon" size={16} />}
						checked={this.isChecked()}
						label={object.name}
						onChange={() => {
							this.handleSelect();
						}}
					/>
				</DropdownItem>
			</Styled>
		);
	}
}
export default Item;
