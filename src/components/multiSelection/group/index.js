import React from 'react';
import { DropdownItem } from 'reactstrap';
import * as Icon from 'react-feather';
import { remove, cloneDeep, reduce, includes } from 'lodash';
import Checkbox from '../../@vuexy/checkbox/CheckboxesVuexy';

import Styled from './styled';
class Group extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			itemIsShowed: false
		};
	}

	handleItemSelect = item => {
		if (this.props.selected.filter(o => o.value === item.value).length <= 0) {
			this.props.setSelected([...this.props.selected, item]);
		} else {
			const temp = cloneDeep(this.props.selected);
			remove(temp, o => o.value === item.value);

			this.props.setSelected(temp);
		}
	};

	isChecked = item => {
		if (this.props.selected.filter(o => o.value === item.value).length <= 0) {
			return false;
		}
		return true;
	};

	selectedCounter = () => {
		let count = 0;
		this.props.object.childrens.forEach(item => {
			if (this.props.selected.filter(o => o.value === item.value).length > 0) {
				count += 1;
			}
		});
		return count;
	};

	handleGroupClick = checked => {
		if (checked) {
			return this.props.setSelected([
				...this.props.selected,
				...this.props.object.childrens
			]);
		}

		if (this.selectedCounter() === this.props.object.childrens.length) {
			const temp = cloneDeep(this.props.selected);
			const values = reduce(
				this.props.object.childrens,
				(result, o) => {
					result.push(o.value);
					return result;
				},
				[]
			);

			remove(temp, o => includes(values, o.value));

			this.props.setSelected(temp);
		} else if (this.selectedCounter() > 0) {
			const temp = [];
			this.props.object.childrens.forEach(obj => {
				if (
					this.props.selected.filter(ele => ele.value === obj.value).length <= 0
				) {
					temp.push(obj);
				}
			});
			this.props.setSelected([...this.props.selected, ...temp]);
		}

		return false;
	};

	isGroupChecked = () => {
		if (this.selectedCounter() > 0) {
			return true;
		}

		return false;
	};

	getGroupIcon = () => {
		if (this.selectedCounter() === this.props.object.childrens.length) {
			return <Icon.Check className="vx-icon" size={16} />;
		} else if (this.selectedCounter() > 0) {
			return <Icon.Minus className="vx-icon" size={16} />;
		}

		return <Icon.Check className="vx-icon" size={16} />;
	};

	render() {
		const { object } = this.props;
		const { itemIsShowed } = this.state;

		return (
			<Styled>
				<DropdownItem className="group-title" tag="a" toggle={false}>
					<Checkbox
						color="primary"
						icon={this.getGroupIcon()}
						checked={this.isGroupChecked()}
						label={object.label}
						onChange={e => {
							this.handleGroupClick(e.target.checked);
						}}
					/>

					<span
						onClick={() => {
							this.setState({
								itemIsShowed: !itemIsShowed
							});
						}}
					>
						{itemIsShowed ? (
							<Icon.ChevronUp size={13} />
						) : (
							<Icon.ChevronDown size={13} />
						)}
					</span>
				</DropdownItem>

				{itemIsShowed && object.childrens && (
					<span>
						{object.childrens.map((item, ele) => (
							<DropdownItem
								className="group-item"
								tag="a"
								key={ele}
								toggle={false}
							>
								<Checkbox
									color="primary"
									icon={<Icon.Check className="vx-icon" size={16} />}
									checked={this.isChecked(item)}
									label={item.name}
									onChange={() => {
										this.handleItemSelect(item);
									}}
								/>
							</DropdownItem>
						))}
					</span>
				)}
			</Styled>
		);
	}
}
export default Group;
