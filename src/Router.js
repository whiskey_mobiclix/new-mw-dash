import React, { Suspense, lazy } from 'react';
import { Router, Switch, Route } from 'react-router-dom';
import { history } from './history';
import { connect } from 'react-redux';
import Spinner from './components/@vuexy/spinner/Loading-spinner';
import { ContextLayout } from './utility/context/Layout';
import { auth } from './redux/actions/auth';
import { setSidebarItems, setSidebarHeader } from './redux/actions/sidebar';
import { setProduct } from "./redux/actions/filter";
import Menu from './utility/helpers/menu';
import { getConfig } from "./redux/actions/app"

//Middlewares
import AuthenticationMiddleware from './utility/middlewares/authentication';
import RoleMiddleware from './utility/middlewares/role';

// Set Layout and Component Using App Route
const RouteConfig = ({
	component: Component,
	fullLayout,
	permission,
	user,
	...rest
}) => (
	<Route
		{...rest}
		render={props => {
			return (
				<ContextLayout.Consumer>
					{context => {
						let LayoutTag =
							fullLayout === true
								? context.fullLayout
								: context.state.activeLayout === 'horizontal'
								? context.horizontalLayout
								: context.VerticalLayout;
						return (
							<LayoutTag {...props} permission={props.user}>
								<Suspense fallback={<Spinner />}>
									<Component {...props} />
								</Suspense>
							</LayoutTag>
						);
					}}
				</ContextLayout.Consumer>
			);
		}}
	/>
);
const mapStateToProps = state => {
	return {
		user: state.auth.login.userRole
	};
};

const AppRoute = connect(mapStateToProps)(RouteConfig);

class AppRouter extends React.Component {
	componentDidMount() {
		this.setupSidebar();
		history.listen(location => {
			this.setupSidebar();
		});
	}

	setupSidebar = () => {
		this.props.setSidebarItems(Menu.list(history.location.pathname, this.props.user.data));
		this.props.setSidebarHeader(
			Menu.header(
				history.location.pathname,
				this.props.user.data.group,
			)
		);

		if(!/^\/product(\/.+)?/.test(history.location.pathname)){
			this.props.setProduct("")
		}
	};

	render() {
		const router = (
			<Router history={history}>
				<Switch>
					<AppRoute
						exact
						path="/"
						component={lazy(() => import('./views/pages/Home'))}
					/>

					{/* Partner */}
					<AppRoute
						exact
						path="/product"
						component={lazy(() => import('./views/pages/overview'))}
					/>
					<AppRoute
						exact
						path="/product/statistics"
						component={lazy(() => import('./views/pages/product-statistics'))}
					/>
					<AppRoute
						exact
						path="/product/overview"
						component={lazy(() => import('./views/pages/product-info'))}
					/>
					<AppRoute
						exact
						path="/product/monthly-statement"
						component={lazy(() => import('./views/pages/monthly-statement'))}
					/>
					<AppRoute
						exact
						path="/product/accquisition"
						component={lazy(() => import('./views/pages/accquisition'))}
					/>
					<AppRoute
						exact
						path="/product/status"
						component={lazy(() => import('./views/pages/product-status'))}
					/>

					<AppRoute
						exact
						path="/product/transactions"
						component={lazy(() => import('./views/pages/product-transactions'))}
					/>
					<AppRoute
						exact
						path="/product/operation"
						component={lazy(() => import('./views/pages/product-operation'))}
					/>
					<AppRoute
						exact
						path="/product/markets-comparision"
						component={lazy(() => import('./views/pages/markets-comparision'))}
					/>

					{/* Finance */}
					<AppRoute
						exact
						path="/finance"
						component={lazy(() => import('./views/pages/finance'))}
					/>
					<AppRoute
						exact
						path="/finance/transactions"
						component={lazy(() => import('./views/pages/finance-transactions'))}
					/>
					<AppRoute
						exact
						path="/finance/discrepency"
						component={lazy(() => import('./views/pages/finance-discrepency'))}
					/>

					{/* Admin */}
					<AppRoute
						exact
						path="/admin"
						component={lazy(() => import('./views/pages/overview'))}
					/>
					<AppRoute
						exact
						path="/admin/accquisition"
						component={lazy(() => import('./views/pages/accquisition'))}
					/>
					<AppRoute
						exact
						path="/admin/comparision-in-depth"
						component={lazy(() => import('./views/pages/comparision-in-depth'))}
					/>
					<AppRoute
						exact
						path="/admin/products"
						component={lazy(() => import('./views/pages/products-list'))}
					/>
					<AppRoute
						exact
						path="/admin/internal-users"
						component={lazy(() => import('./views/pages/users'))}
					/>
					<AppRoute
						exact
						path="/admin/partner-users"
						component={lazy(() => import('./views/pages/users'))}
					/>
					<AppRoute
						exact
						path="/product/partner-users"
						component={lazy(() => import('./views/pages/users'))}
					/>
					<AppRoute
						exact
						path="/product/create-user"
						component={lazy(() => import('./views/pages/user'))}
					/>
					<AppRoute
						exact
						path="/product/edit-user"
						component={lazy(() => import('./views/pages/user'))}
					/>
					<AppRoute
						exact
						path="/admin/create-user"
						component={lazy(() => import('./views/pages/user'))}
					/>
					<AppRoute
						exact
						path="/admin/edit-user"
						component={lazy(() => import('./views/pages/user'))}
					/>
					<AppRoute
						exact
						path="/comming-soon"
						component={lazy(() => import('./views/pages/CommingSoon'))}
						fullLayout
					/>
					<AppRoute
						exact
						path="/product/test"
						component={lazy(() => import('./views/pages/_test'))}
					/>
					<AppRoute
						path="/error/404"
						component={lazy(() => import('./views/pages/errors/404'))}
						fullLayout
					/>
					<AppRoute
						path="*"
						component={lazy(() => import('./views/pages/CommingSoon'))}
						fullLayout
					/>
				</Switch>
			</Router>
		);

		const authenticated = AuthenticationMiddleware(router, this.props);

		return RoleMiddleware(authenticated, this.props);
	}
}

export default connect(
	state => ({
		user: state.user,
	}),
	{
		auth,
		setSidebarItems,
		setSidebarHeader,
		setProduct,
		getConfig,
	}
)(AppRouter);
