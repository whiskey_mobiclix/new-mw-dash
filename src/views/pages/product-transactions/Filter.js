import React from 'react';
import Selection from '../../../components/selection';
import DateFilter from '../../../utility/modules/filter/date';

//CSS
import './style.scss';

class Filter extends React.Component {
	render() {
		return (
			<div className="product-transactions-filter">
				<DateFilter />

				<Selection
					{...{
						value: 'total_trans',
						options: [
							{
								name: 'All transactions',
								value: 'total_trans'
							},
							{
								name: 'Success',
								value: 'success_trans'
							},
							{
								name: 'Failed',
								value: 'fail_trans'
							}
						],
						onChange: selected => {
							this.props.onChange(selected);
						}
					}}
				/>
			</div>
		);
	}
}

export default Filter;
