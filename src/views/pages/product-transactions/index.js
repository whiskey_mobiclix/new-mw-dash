import React from 'react';
import { connect } from 'react-redux';
import Title from '../../../utility/modules/title';
import Filter from './Filter';
import Spinner from '../../../components/@vuexy/spinner/Loading-spinner';
import General from "./general"
import Extra from "./extra"

//CSS
import './style.scss';

class ProductTransactions extends React.Component {
	state = {
		filter: {
			name: 'Total Transactions',
			value: 'total_trans'
		}
	}

	render() {
		if (this.props.app.locked) return <Spinner />;

		return (
			<div className="product-transactions">
				<Title text={"Product's Transactions & Receivable"} />
				<Filter
					onChange={e => {
						this.setState({
							filter: e
						});
					}}
				/>

				<General {...{
					filter: this.state.filter.value,
				}}/>

				<Extra {...{
					filter: this.state.filter.value,
				}}/>
			</div>
		);
	}
}

export default connect(state => ({
	app: state.app,
}))(ProductTransactions);
