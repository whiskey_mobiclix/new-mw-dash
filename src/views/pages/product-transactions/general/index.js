import React from "react"
import { connect } from "react-redux"
import { Card, CardHeader, CardBody, CardTitle } from "reactstrap"
import COLOR from "../../../../utility/helpers/color"
import Date from "../../../../utility/helpers/date"
import Data from "../../../../utility/helpers/data"
import Number from "../../../../utility/helpers/number"
import Comparision from "../../../../utility/modules/comparision"
import { ColumnChart } from "../../../../utility/modules/chart"

class General extends React.Component{
	series = () => {
		const dates = Date.days(this.props.gfilter.daterange.value)
		const statsData = Data.fill(dates, Data.groupByDate(this.props.transactions.data))

		const type = this.props.filter;
		if(type === "total_trans"){
			return [
				{
					name: "Success",
					data: statsData.map(o => parseFloat(o.success_trans || 0))
				},
				{
					name: "Failed",
					data: statsData.map(o => parseFloat(o.fail_trans || 0))
				},
			]
		}

		if(type === "success_trans"){
			return [
				{
					name: "Success",
					data: statsData.map(o => parseFloat(o.success_trans || 0))
				}
			]
		}

		return [
			{
				name: "Failed",
				data: statsData.map(o => parseFloat(o.fail_trans || 0))
			},
		]
	}

	strokes = () => {
		const type = this.props.filter
		if(type === "total_trans"){
			return ['#877df2', '#ff93e3']
		}

		if(type === "success_trans"){
			return ['#877df2']
		}

		return ['#ff93e3']
	};

	colors = () => {
		const type = this.props.filter
		if(type === "total_trans"){
			return [COLOR.$primary, '#ff80df']
		}

		if(type === "success_trans"){
			return [COLOR.$primary]
		}

		return ['#ff80df'];
	}

	render(){
		const dates = Date.days(this.props.gfilter.daterange.value)

		return (
			<div className="product-transactions-general">
				<Card>
					<CardHeader>
						<CardTitle>
							Overview
							<div>
								<Comparision
									{...{
										current: Number.total(
											this.props.transactions.data.map(o => parseFloat(o[this.props.filter] || 0))
										),
										previous: Number.total(
											this.props.transactions.previous.map(o => parseFloat(o[this.props.filter] || 0))
										)
									}}
								/>
							</div>
						</CardTitle>
					</CardHeader>
					<CardBody>
						<div>
							<ColumnChart
								{...{
									options: {
										legend: {
											onItemClick: {
												toggleDataSeries: false
											}
										},
										stroke: {
											show: true,
											colors: this.strokes()
										}
									},
									categories: dates,
									series: this.series(),
									colors: this.colors()
								}}
							/>
						</div>
					</CardBody>
				</Card>
			</div>
		)
	}
}

export default connect(
	state => ({
		gfilter: state.filter,
		transactions: state.statistics.transactions,
	}),
	{}
)(General)