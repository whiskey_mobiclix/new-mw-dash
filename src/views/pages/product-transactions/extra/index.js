import React from "react"
import { Row, Col } from "reactstrap"
import Status from "./status"
import Markets from "./markets"
import MNOs from "./mnos"

class TransactionsExtra extends React.Component{
	render(){
		return (
			<Row>
				<Col lg={4} md={6} sm={12}>
					<Status {...this.props}/>
				</Col>

				<Col lg={4} md={6} sm={12}>
					<Markets {...this.props}/>
				</Col>

				<Col lg={4} md={6} sm={12}>
					<MNOs {...this.props}/>
				</Col>
			</Row>
		)
	}
}

export default TransactionsExtra