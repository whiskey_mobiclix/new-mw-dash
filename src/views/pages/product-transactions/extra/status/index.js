import React from "react"
import { connect } from "react-redux"
import { Card, CardTitle, CardHeader, CardBody } from "reactstrap"
import { PieChart } from "../../../../../utility/modules/chart"
import Number from "../../../../../utility/helpers/number"

class TransactionsStatus extends React.Component{
	labels = () => {
		const type = this.props.filter
		if(type === "total_trans") return ["Success", "Failed"]
		if(type === "success_trans") return ["Success"]
		return ["Failed"]
	}

	series = () => {
		const type = this.props.filter
		if(type === "total_trans") return [
			Number.total(this.props.transactions.data.map(o => parseFloat(o.success_trans || 0))),
			Number.total(this.props.transactions.data.map(o => parseFloat(o.fail_trans || 0))),
		]
		if(type === "success_trans") return [
			Number.total(this.props.transactions.data.map(o => parseFloat(o.success_trans || 0))),
		]
		return [
			Number.total(this.props.transactions.data.map(o => parseFloat(o.fail_trans || 0))),
		]
	}

	render(){
		return (
			<Card>
				<CardHeader className="mb-1">
					<CardTitle>Status</CardTitle>
				</CardHeader>

				<CardBody className="pt-0">
					<div className="pie-chart-wrapper">
						<PieChart {...{
							labels: this.labels(),
							series: this.series()
						}}/>
					</div>
				</CardBody>
			</Card>
		)
	}
}

export default connect(
	state => ({
		transactions: state.statistics.transactions,
	})
)(TransactionsStatus)