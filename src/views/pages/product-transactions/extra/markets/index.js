import React from "react"
import { connect } from "react-redux"
import { Card, CardTitle, CardHeader, CardBody } from "reactstrap"
import { PieChart } from "../../../../../utility/modules/chart"
import Number from "../../../../../utility/helpers/number"
import Data from "../../../../../utility/helpers/data"
import Country from "../../../../../utility/helpers/country"

class TransactionsMarkets extends React.Component{
	render(){
		const statsData = Data.groupByMarkets(this.props.transactions.data).filter(o => o[this.props.filter]).sort((a, b) => {
			if(parseFloat(a[this.props.filter] || 0) < parseFloat(b[this.props.filter] || 0)) return 1
			return -1
		})

		return (
			<Card>
				<CardHeader className="mb-1">
					<CardTitle>Markets</CardTitle>
				</CardHeader>

				<CardBody className="pt-0">
					<div className="pie-chart-wrapper">
						<PieChart {...{
							labels: statsData.map(o => Country.getCountryByCode(o.country) || "Others"),
							series: statsData.map(o => parseFloat(o[this.props.filter] || 0))
						}}/>
					</div>
				</CardBody>
			</Card>
		)
	}
}

export default connect(
	state => ({
		transactions: state.statistics.transactions,
	})
)(TransactionsMarkets)