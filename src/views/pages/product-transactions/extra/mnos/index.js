import React from "react"
import { connect } from "react-redux"
import { Card, CardTitle, CardHeader, CardBody } from "reactstrap"
import { PieChart } from "../../../../../utility/modules/chart"
import Number from "../../../../../utility/helpers/number"
import Data from "../../../../../utility/helpers/data"
import String from "../../../../../utility/helpers/string"

class TransactionsMNOs extends React.Component{
	render(){
		const statsData = Data.groupByMNOs(this.props.transactions.data).filter(o => o[this.props.filter]).sort((a, b) => {
			if(parseFloat(a[this.props.filter] || 0) < parseFloat(b[this.props.filter] || 0)) return 1
			return -1
		})

		return (
			<Card>
				<CardHeader className="mb-1">
					<CardTitle>MNOs</CardTitle>
				</CardHeader>

				<CardBody className="pt-0">
					<div className="pie-chart-wrapper">
						<PieChart {...{
							labels: statsData.map(o => String.capitalize(o.mno.toLowerCase())),
							series: statsData.map(o => parseFloat(o[this.props.filter] || 0))
						}}/>
					</div>
				</CardBody>
			</Card>
		)
	}
}

export default connect(
	state => ({
		transactions: state.statistics.transactions,
	})
)(TransactionsMNOs)