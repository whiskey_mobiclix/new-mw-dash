import React from 'react';

import Starter from './starter';
import Main from './main';

import Styled from './styled';

class FinanceTransactions extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			activedStep: 1
		};
	}

	setActivedStep = step => {
		this.setState({
			...this.state,
			activedStep: step
		});
	};

	render() {
		const { activedStep } = this.state;

		return (
			<Styled>
				<Main />
			</Styled>
		);
	}
}

export default FinanceTransactions;
