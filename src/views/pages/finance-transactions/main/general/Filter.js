import React from "react";
import { connect } from "react-redux"
import Selection from "../../../../../components/selection";
import { Comparision } from "../../../../../utility/modules";

//CSS
import "./style.scss";

class Filter extends React.Component{
	state = {
		active: "totalRevenue",
		values: {
			totalRevenue: 0,
			payableRevenue: 0,
			totalExpenses: 0,
			remainingRevenue: 0,
		}
	}

	handleClick = id => {
		let _state = this.state;
		_state.active = id;
		this.setState(_state);
		this.props.onChange && this.props.onChange({
			active: id,
			value: _state.values[id],
		});
	}

	handleFilterChange = (id, value) => {
		let _state = this.state;
		_state.values[id] = value;

		this.setState(_state);
		this.props.onChange && this.props.onChange({
			active: id,
			value: _state.values[id],
		});
	}

	render(){
		return (
			<div className="finance-transactions-filter">
				<div className="item">
					<div className={`selection-wrapper ${this.state.active === "totalRevenue" ? "active" : ""}`}>
						{
							this.state.active !== "totalRevenue"
							&&
							<div className="item-overlay" onClick={() => {
								this.handleClick("totalRevenue")
							}}/>
						}
						
						<Selection {...{
							value: this.state.values.totalRevenue,
							options: [
								{
									name: "Total revenue",
									value: 0,
								},
							],
							onChange: selected => {
								this.handleFilterChange("totalRevenue", selected.value)
							}
						}} />
					</div>

					<Comparision />
				</div>

				<div className="item">
					<div className={`selection-wrapper ${this.state.active === "payableRevenue" ? "active" : ""}`}>
						{
							this.state.active !== "payableRevenue"
							&&
							<div className="item-overlay" onClick={() => {
								this.handleClick("payableRevenue")
							}}/>
						}
						
						<Selection {...{
							value: this.state.values.payableRevenue,
							options: [
								{
									name: "Total net revenue",
									value: 0,
								},
							]
						}} />
					</div>

					<Comparision />
				</div>

				<div className="item">
					<div className={`selection-wrapper ${this.state.active === "totalExpenses" ? "active" : ""}`}>
						{
							this.state.active !== "totalExpenses"
							&&
							<div className="item-overlay" onClick={() => {
								this.handleClick("totalExpenses")
							}}/>
						}
						<Selection {...{
							value: this.state.values.totalExpenses,
							options: [
								{
									name: "Total transactions",
									value: 0,
								},
							]
						}} />
					</div>

					<Comparision />
				</div>

				<div className="item">
					<div className={`selection-wrapper ${this.state.active === "remainingRevenue" ? "active" : ""}`}>
						{
							this.state.active !== "remainingRevenue"
							&&
							<div className="item-overlay" onClick={() => {
								this.handleClick("remainingRevenue")
							}}/>
						}
						<Selection {...{
							value: this.state.values.remainingRevenue,
							options: [
								{
									name: "Total Remaining Revenue",
									value: 0,
								},
							]
						}} />
					</div>

					<Comparision />
				</div>
			</div>
		)
	}
}

export default connect(
	state => ({
		user: state.user,
	}),
	{}
)(Filter);