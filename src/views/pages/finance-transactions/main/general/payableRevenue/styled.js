import styled from 'styled-components';
import { Row } from 'reactstrap';

const Styled = styled(Row)`
	display: flex;
	justify-content: center;

	> div {
		padding: 20px 20px 0 0;
	}

	.apexcharts-legend-series {
		margin: 0 30px 0 0 !important;

		.apexcharts-legend-marker {
			margin: 0 10px 0 0 !important;
		}
	}
`;

export default Styled;
