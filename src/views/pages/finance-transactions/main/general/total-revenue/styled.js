import styled from 'styled-components';
import { Row } from 'reactstrap';

const Styled = styled(Row)`
	display: flex;
	justify-content: center;

	> div {
		padding: 20px 20px 0 0;
	}
`;

export default Styled;
