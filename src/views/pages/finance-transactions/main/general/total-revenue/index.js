import React from 'react';
import { LineChart } from '../../../../../../utility/modules/chart';

class TotalRevenue extends React.Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		return (
			<div className="finance-total-revenue">
				<LineChart {...{
		            categories: [
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		              "",
		            ],
		            series: [
		              {
		                name: "",
		                data: [140, 180, 150, 205, 160, 295, 125, 255, 205, 305, 240, 295, 305, 240, 295, 140, 180, 150, 205, 160, 295, 125, 255, 205, 305, 240, 295, 305, 240, 295]
		              },
		            ]
		          }}/>
			</div>
		);
	}
}

export default TotalRevenue;
