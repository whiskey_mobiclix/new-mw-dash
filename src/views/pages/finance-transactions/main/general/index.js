import React from 'react';
import { Card, CardBody, Button } from 'reactstrap';

import Filter from './Filter';
import TotalRevenue from './total-revenue';
import PayableRevenue from './payableRevenue';

class General extends React.Component {
	state = {
		filter: {
			active: "totalRevenue"
		}
	}

	render() {
		return (
			<div className="finance-transactions-general">
				<Card>
					<CardBody className="pb-3">
						<Filter {...{
							onChange: e => {
								this.setState({
									filter: e,
								})
							}
						}}/>

						{
							this.state.filter.active === "totalRevenue"
							&&
							<TotalRevenue />
						}
					</CardBody>
				</Card>

				<div style={{
					textAlign: "right",
				}}>
					<p>You can download a full report in RAW data here by click the below button.</p>
					<Button.Ripple className="mr-1 mb-1 bg-gradient-primary" color="none">Download full report (RAW)</Button.Ripple>
				</div>
			</div>
		);
	}
}
export default General;
