import styled from 'styled-components';
import { Row } from 'reactstrap';

const Styled = styled(Row)`
	.finance-general-filter {
		display: flex;

		.item {
			width: 25%;

			.dropdown-toggle {
				padding-left: 0 !important;
			}

			.mw-comparision {
				margin-top: 10px;
			}

			.title-wrapper {
				position: relative;
				display: inline-block;

				.tab-title {
					min-height: 40px;
				}

				&.active {
					border-bottom: 3px solid #7a6ff0;
				}

				.item-overlay {
					position: absolute;
					top: 0;
					left: -10px;
					right: -10px;
					bottom: 0;
					z-index: 10;
				}
			}
		}
	}
`;

export default Styled;
