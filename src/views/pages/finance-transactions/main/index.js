import React from 'react';

import Title from '../../../../utility/modules/title';
import Filter from '../../../../utility/modules/filter';
import General from './general';

import Styled from './styled';

class Main extends React.Component {
	render() {
		return (
			<Styled>
				<Title text="Transactions Dashboard" />

				<Filter />
				<General />
			</Styled>
		);
	}
}

export default Main;
