import styled from 'styled-components';

const Styled = styled.div`
	.main-filter {
		.mw-selection {
			margin: 10px;

			&:first-child {
				margin-left: 0;
			}
		}

		.reload-icon {
			margin-left: 15px;
			cursor: pointer;
			transition: transform 250ms;

			&:hover {
				transform: rotate(90deg);
			}
		}

		.submit-btn {
			margin-left: 30px;
		}
	}
`;

export default Styled;
