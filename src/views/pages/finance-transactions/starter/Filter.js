import React from 'react';

import Selection from '../../../../components/selection';

class Filter extends React.Component {
	render() {
		return (
			<div className="starter-filter">
				<Selection
					{...{
						value: null,
						options: [
							{
								name: 'Select a time range',
								value: null
							}
						],
						onChange: selected => {
							console.log(selected);
						}
					}}
				/>

				<Selection
					{...{
						value: null,
						options: [
							{
								name: 'Select a market',
								value: null
							}
						],
						onChange: selected => {
							console.log(selected);
						}
					}}
				/>

				<Selection
					{...{
						value: null,
						options: [
							{
								name: 'Select a MNOs',
								value: null
							}
						],
						onChange: selected => {
							console.log(selected);
						}
					}}
				/>

				<Selection
					{...{
						value: null,
						options: [
							{
								name: 'Select Payment Gateway',
								value: null
							}
						],
						onChange: selected => {
							console.log(selected);
						}
					}}
				/>
			</div>
		);
	}
}

export default Filter;
