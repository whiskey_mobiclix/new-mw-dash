import styled from 'styled-components';

const Styled = styled.div`
	text-align: center;

	.title {
		margin-top: 5rem;
		text-align: center;

		.page-title {
			padding: 0;
		}

		.sub-title {
			margin-top: 1rem;
			margin-bottom: .5rem;
		}
	}

	.starter-filter {
		display: flex;
		flex-wrap: wrap;
		justify-content: center;

		.mw-selection {
			margin: 10px;
		}
	}

	.submit-btn {
		margin: 20px 20px 30px 20px !important;
	}
`;

export default Styled;
