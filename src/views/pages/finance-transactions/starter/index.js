import React from 'react';
import { Button } from 'reactstrap';

import Title from '../../../../utility/modules/title';

import Filter from './Filter';

import Styled from './styled';

class FinanceTransactions extends React.Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		const { setActivedStep } = this.props;
		return (
			<Styled>
				<div className="title">
					<Title text="Transactions Dashboard" />

					<div className="sub-title">
						Please choose details for the transaction summary you want to see.
					</div>
				</div>

				<Filter />

				<Button.Ripple
					className="submit-btn"
					color="primary"
					onClick={() => {
						setActivedStep(2);
					}}
				>
					Submit
				</Button.Ripple>
			</Styled>
		);
	}
}

export default FinanceTransactions;
