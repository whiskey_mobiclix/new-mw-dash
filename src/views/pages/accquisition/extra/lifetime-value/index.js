import React from "react"
import { Card, CardHeader, CardTitle, CardBody } from "reactstrap"
import { LineChart } from "../../../../../utility/modules/chart";
import { Comparision } from "../../../../../utility/modules";

class LifetimeValue extends React.Component {
  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>
            <div style={{
              marginBottom: ".5rem"
            }}>
              <Comparision />
            </div>
            Lifetime Value
          </CardTitle>
        </CardHeader>
        <CardBody>
          <LineChart {...{
            categories: [
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              ""
            ],
            series: [
              {
                name: "",
                data: [140, 180, 150, 205, 160, 295, 125, 255, 205, 305, 240, 295]
              },
            ]
          }}/>
        </CardBody>
      </Card>
    )
  }
}
export default LifetimeValue;