import React from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody
} from "reactstrap"

import { PieChart } from "../../../../../utility/modules/chart";
import { Comparision } from "../../../../../utility/modules";

class MarketingSpend extends React.Component {
  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>
            <div style={{
              marginBottom: ".5rem"
            }}>
              <Comparision />
            </div>
            Marketing spend
          </CardTitle>
        </CardHeader>

        <CardBody className="pt-0">
          <PieChart {...{
            labels: ["Desktop", "Mobile", "Tablet"],
            series: [58.6, 34.9, 6.5],
          }}/>
        </CardBody>
      </Card>
    )
  }
}
export default MarketingSpend