import React from 'react';
import { Row, Col } from 'reactstrap';

import ChurnRate from './churn-rate';
import BillingSuccessRate from './billing-success-rate';
import Roas from './roas';
import LifetimeValue from './lifetime-value';
import CPA from './cpa';
import MarketingSpend from './marketing-spend';

class AccquisitionExtra extends React.Component {
	render() {
		return (
			<div className="accquisition-extra">
				<Row>
					<Col lg={6} md={6} sm={12}>
						<ChurnRate />
					</Col>

					<Col lg={6} md={6} sm={12}>
						<BillingSuccessRate />
					</Col>

					{/* <Col lg={4} md={6} sm={12}>
						<Roas />
					</Col>

					<Col lg={4} md={6} sm={12}>
						<LifetimeValue />
					</Col>

					<Col lg={4} md={6} sm={12}>
						<CPA />
					</Col>

					<Col lg={4} md={6} sm={12}>
						<MarketingSpend />
					</Col> */}
				</Row>
			</div>
		);
	}
}

export default AccquisitionExtra;
