import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Card, CardHeader, CardTitle, CardBody } from 'reactstrap';

import Number from '../../../../../utility/helpers/number';
import { LineChart } from '../../../../../utility/modules/chart';
import { Comparision } from '../../../../../utility/modules';
import Data from '../../../../../utility/helpers/data';
import Date from '../../../../../utility/helpers/date';

class ChurnRate extends React.Component {
	render() {
		const dates = Date.days(this.props.gfilter.daterange.value)
		const statsData = Data.fill(dates, Data.groupByDate(this.props.users.data))

		return (
			<Card>
				<CardHeader>
					<CardTitle>
						<div
							style={{
								marginBottom: '.5rem'
							}}
						>
							<Comparision
								unit="%"
								current={Number.rate(Number.total(this.props.users.data.map(o => parseInt(o.churn_users || 0))), Number.total(this.props.users.data.map(o => parseInt(o.total_users || 0))))}
								previous={Number.rate(Number.total(this.props.users.previous.map(o => parseInt(o.churn_users || 0))), Number.total(this.props.users.previous.map(o => parseInt(o.total_users || 0))))}
							/>
						</div>
						ChurnRate
					</CardTitle>
				</CardHeader>
				<CardBody>
					<LineChart
						options={{
							xaxis: {
								labels: { show: false }
							},
							tooltip: {
								y: {
									formatter: value => value + ' %'
								}
							}
						}}
						categories={this.props.users.data.map(o =>
							moment(o.time).format('DD MMM YYYY')
						)}
						series={[
							{
								name: 'Churn Rate',
								data: statsData.map(o => parseInt(o.churn_users || 0) / parseInt(o.total_users || 1) * 100)
							}
						]}
					/>
				</CardBody>
			</Card>
		);
	}
}
export default connect(
	state => ({
		users: state.statistics.users,
		gfilter: state.filter,
	}),
	{}
)(ChurnRate);
