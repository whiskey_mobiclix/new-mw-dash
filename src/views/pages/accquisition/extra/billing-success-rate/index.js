import React from 'react';
import { connect } from 'react-redux';
import { Card, CardHeader, CardTitle, CardBody } from 'reactstrap';
import moment from 'moment';

import Number from '../../../../../utility/helpers/number';
import { LineChart } from '../../../../../utility/modules/chart';
import { Comparision } from '../../../../../utility/modules';
import Data from '../../../../../utility/helpers/data';
import Date from '../../../../../utility/helpers/date';

class BillingSuccessRate extends React.Component {
	render() {
		const dates = Date.days(this.props.gfilter.daterange.value)
		const statsData = Data.fill(dates, Data.groupByDate(this.props.transactions.data));

		return (
			<Card>
				<CardHeader>
					<CardTitle>
						<div
							style={{
								marginBottom: '.5rem'
							}}
						>
							<Comparision
								unit="%"
								unit="%"
						current={Number.rate(Number.total(this.props.transactions.data.map(o => parseInt(o.success_trans || 0))), Number.total(this.props.transactions.data.map(o => parseInt(o.total_trans || 0))))}
						previous={Number.rate(Number.total(this.props.transactions.previous.map(o => parseInt(o.success_trans || 0))), Number.total(this.props.transactions.previous.map(o => parseInt(o.total_trans || 0))))}
							/>
						</div>
						Billing Success Rate
					</CardTitle>
				</CardHeader>
				<CardBody>
					<LineChart
						options={{
							xaxis: {
								labels: { show: false }
							},
							tooltip: {
								y: {
									formatter: value => value + ' %'
								}
							}
						}}
						categories={this.props.transactions.data.map(o =>
							moment(o.time).format('DD MMM YYYY')
						)}
						series={[
							{
								name: 'Churn Rate',
								data: statsData.map(o => parseFloat(o.success_trans || 0) / parseFloat(o.total_trans || 1) * 100)
							}
						]}
					/>
				</CardBody>
			</Card>
		);
	}
}
export default connect(
	state => ({
		transactions: state.statistics.transactions,
		gfilter: state.filter,
	}),
	{}
)(BillingSuccessRate);
