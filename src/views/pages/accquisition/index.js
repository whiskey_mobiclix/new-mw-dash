import React from 'react';
import { connect } from 'react-redux';
import Filter from '../../../utility/modules/filter';
import Title from '../../../utility/modules/title';

import General from './general';
import Extra from './extra';
import Spinner from '../../../components/@vuexy/spinner/Loading-spinner';

class Accquisition extends React.Component {
	render() {
		if (this.props.app.locked) return <Spinner />

		return (
			<div className="accquisition">
				<Title text={'Accquisition Overview'} />
				<Filter />

				<General />
				<Extra />
			</div>
		);
	}
}

export default connect(state => ({
	app: state.app
}))(Accquisition);
