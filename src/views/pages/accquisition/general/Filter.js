import React from 'react';
import { connect } from 'react-redux';

import Number from '../../../../utility/helpers/number';
import Selection from '../../../../components/selection';
import { Comparision } from '../../../../utility/modules';

//CSS
import './style.scss';

class Filter extends React.Component {
	state = {
		active: 'billing',
		values: {
			churnrate: 0,
			billing: 0,
			roas: 0,
			lifetime: 0
		}
	};

	handleClick = id => {
		let _state = this.state;
		_state.active = id;
		this.setState(_state);
		this.props.onChange &&
			this.props.onChange({
				active: id,
				value: _state.values[id]
			});
	};

	handleFilterChange = (id, value) => {
		let _state = this.state;
		_state.values[id] = value;

		this.setState(_state);
		this.props.onChange &&
			this.props.onChange({
				active: id,
				value: _state.values[id]
			});
	};

	render() {
		return (
			<div className="accquisition-filter">
				<div className="item">
					<div
						className={`selection-wrapper ${
							this.state.active === 'billing' ? 'active' : ''
						}`}
					>
						{this.state.active !== 'billing' && (
							<div
								className="item-overlay"
								onClick={() => {
									this.handleClick('billing');
								}}
							/>
						)}

						<Selection
							{...{
								value: this.state.values.billing,
								options: [
									{
										name: 'Billing Success Rate',
										value: 0
									}
								]
							}}
						/>
					</div>

					<Comparision
						unit="%"
						current={Number.rate(Number.total(this.props.transactions.data.map(o => parseInt(o.success_trans || 0))), Number.total(this.props.transactions.data.map(o => parseInt(o.total_trans || 0))))}
						previous={Number.rate(Number.total(this.props.transactions.previous.map(o => parseInt(o.success_trans || 0))), Number.total(this.props.transactions.previous.map(o => parseInt(o.total_trans || 0))))}
					/>
				</div>

				<div className="item">
					<div
						className={`selection-wrapper ${
							this.state.active === 'churnrate' ? 'active' : ''
						}`}
					>
						{this.state.active !== 'churnrate' && (
							<div
								className="item-overlay"
								onClick={() => {
									this.handleClick('churnrate');
								}}
							/>
						)}

						<Selection
							{...{
								value: this.state.values.churnrate,
								options: [
									{
										name: 'Churn Rate',
										value: 0
									}
								],
								onChange: selected => {
									this.handleFilterChange('churnrate', selected.value);
								}
							}}
						/>
					</div>

					<Comparision
						unit="%"
						current={Number.rate(Number.total(this.props.users.data.map(o => parseInt(o.churn_users || 0))), Number.total(this.props.users.data.map(o => parseInt(o.total_users || 0))))}
						previous={Number.rate(Number.total(this.props.users.previous.map(o => parseInt(o.churn_users || 0))), Number.total(this.props.users.previous.map(o => parseInt(o.total_users || 0))))}
					/>
				</div>

				{/* 
				<div className="item">
					<div
						className={`selection-wrapper ${
							this.state.active === 'roas' ? 'active' : ''
						}`}
					>
						{this.state.active !== 'roas' && (
							<div
								className="item-overlay"
								onClick={() => {
									this.handleClick('roas');
								}}
							/>
						)}

						<Selection
							{...{
								value: this.state.values.roas,
								options: [
									{
										name: 'ROAS',
										value: 0
									}
								]
							}}
						/>
					</div>
					<Comparision />
				</div>

				<div className="item">
					<div
						className={`selection-wrapper ${
							this.state.active === 'lifetime' ? 'active' : ''
						}`}
					>
						{this.state.active !== 'lifetime' && (
							<div
								className="item-overlay"
								onClick={() => {
									this.handleClick('lifetime');
								}}
							/>
						)}

						<Selection
							{...{
								value: this.state.values.lifetime,
								options: [
									{
										name: 'Lifetime Value',
										value: 0
									}
								]
							}}
						/>
					</div>

					<Comparision />
				</div>

				<div className="item">
					<div
						className={`selection-wrapper ${
							this.state.active === 'cpa' ? 'active' : ''
						}`}
					>
						{this.state.active !== 'cpa' && (
							<div
								className="item-overlay"
								onClick={() => {
									this.handleClick('cpa');
								}}
							/>
						)}

						<Selection
							{...{
								value: this.state.values.lifetime,
								options: [
									{
										name: 'CPA',
										value: 0
									}
								]
							}}
						/>
					</div>

					<Comparision />
				</div> */}
			</div>
		);
	}
}

export default connect(
	state => ({
		users: state.statistics.users,
		transactions: state.statistics.transactions
	}),
	{}
)(Filter);
