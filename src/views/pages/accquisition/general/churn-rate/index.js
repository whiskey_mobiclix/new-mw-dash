import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { ColumnChart } from '../../../../../utility/modules/chart';
import Number from '../../../../../utility/helpers/number';
import Data from '../../../../../utility/helpers/data';
import Date from '../../../../../utility/helpers/date';

class ChurnRate extends React.Component {
	render() {
		const dates = Date.days(this.props.gfilter.daterange.value);
		const statsData = Data.fill(dates, Data.groupByDate(this.props.users.data));

		return (
			<div className="accquisition-general-churnrate">
				<ColumnChart
					options={{
						xaxis: {
							tooltip: {
								enabled: false
							}
						},
						tooltip: {
							x: {
								show: true
							},
							y: {
								formatter: value => value + ' %'
							}
						}
					}}
					categories={dates}
					series={[
						{
							name: 'Churn Rate',
							data: statsData.map(o =>
								Number.format(
									(parseInt(o.churn_users || 0) /
										parseInt(o.total_users || 1)) *
										100,
									2
								)
							)
						}
					]}
				/>
			</div>
		);
	}
}

export default connect(
	state => ({
		users: state.statistics.users,
		gfilter: state.filter
	}),
	{}
)(ChurnRate);
