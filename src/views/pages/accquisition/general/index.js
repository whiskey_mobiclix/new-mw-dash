import React from 'react';
import Filter from './Filter';
import { Card, CardBody, CardHeader, CardTitle } from 'reactstrap';

import ChurnRate from './churn-rate';
import SuccessRate from './success-rate';

class General extends React.Component {
	state = {
		filter: {
			active: 'billing',
			value: ''
		}
	};

	render() {
		return (
			<div className="accquisition-general">
				<Card>
					<CardHeader>
						<CardTitle>
							Overview Statistics
						</CardTitle>
					</CardHeader>
					
					<CardBody className="pt-0">
						<Filter
							{...{
								onChange: e => {
									this.setState({
										filter: e
									});
								}
							}}
						/>

						{this.state.filter.active === 'churnrate' && <ChurnRate />}

						{this.state.filter.active === 'billing' && <SuccessRate />}
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default General;
