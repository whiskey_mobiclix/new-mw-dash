import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';

import { ColumnChart } from '../../../../../utility/modules/chart';
import Number from '../../../../../utility/helpers/number';
import Data from '../../../../../utility/helpers/data';
import Date from '../../../../../utility/helpers/date';

class SuccessRate extends React.Component {
	render() {
		const dates = Date.days(this.props.gfilter.daterange.value);
		const statsData = Data.fill(
			dates,
			Data.groupByDate(this.props.transactions.data)
		);

		return (
			<div className="accquisition-general-success-rate">
				<ColumnChart
					options={{
						xaxis: {
							tooltip: {
								enabled: false
							}
						},
						tooltip: {
							x: {
								show: true
							},
							y: {
								formatter: value => value + ' %'
							}
						}
					}}
					categories={dates}
					series={[
						{
							name: 'Billing Success Rate',
							data: statsData.map(
								o =>
									(parseInt(o.success_trans || 0) /
										parseInt(o.total_trans || 1)) *
									100
							)
						}
					]}
				/>
			</div>
		);
	}
}

export default connect(
	state => ({
		transactions: state.statistics.transactions,
		gfilter: state.filter
	}),
	{}
)(SuccessRate);
