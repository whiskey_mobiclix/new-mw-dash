import React from 'react';
import { connect } from 'react-redux';
import {
	Button,
	Card,
	CardBody,
	Row,
	Col,
	Form,
	FormGroup,
	Input,
	Label,
	Alert
} from 'reactstrap';
import { Mail, Lock } from 'react-feather';
// import { history } from "../../../../history"
// import Checkbox from "../../../../components/@vuexy/checkbox/CheckboxesVuexy"
// import googleSvg from "../../../../assets/img/svg/google.svg"

import loginImg from '../../../../assets/img/pages/login.png';
import '../../../../assets/scss/pages/authentication.scss';

import { auth } from '../../../../redux/actions/auth';
import REGEX from '../../../../utility/constants/regex';
import String from "../../../../utility/helpers/string"

class Login extends React.Component {
	state = {
		activeTab: '1',
		email: '',
		password: '',
		loading: false,
		error: ''
	};

	// toggle = tab => {
	//   if (this.state.activeTab !== tab) {
	//     this.setState({
	//       activeTab: tab
	//     })
	//   }
	// }

	validate = () => {
		if (!this.state.email || !this.state.password) return false;
		if (!REGEX.email.test(this.state.email)) return false;
		return true;
	};

	submit = () => {
		if (this.state.loading || !this.validate()) return;

		this.setState({
			loading: true,
			error: ''
		});

		this.props
			.auth(this.state.email, this.state.password)
			.catch(e => {
				try {
					this.setState({
						error: String.capitalize(e.response.data.message || "Something went wrong, please try again")
					});
				} catch (err) {
					this.setState({
						error: err.message
					});
				}
			})
			.finally(() => {
				this.setState({
					loading: false
				});
			});
	};

	render() {
		return (
			<Row className="m-0 justify-content-center">
				<Col
					sm="8"
					xl="7"
					lg="10"
					md="8"
					className="d-flex justify-content-center"
				>
					<Card className="bg-authentication login-card rounded-0 mb-0 w-100">
						<Row className="m-0">
							<Col
								lg="6"
								className="d-lg-block d-none text-center align-self-center px-1 py-0"
							>
								<img src={loginImg} alt="loginImg" />
							</Col>
							<Col lg="6" md="12" className="p-0">
								<Card className="rounded-0 mb-0 px-2">
									<CardBody>
										<h4>Login</h4>
										<p
											style={{
												marginBottom: '2rem'
											}}
										>
											Welcome back, please login to your account.
										</p>

										{!!this.state.error.length && (
											<Alert
												color="warning"
												style={{
													marginTop: '-1rem',
													marginBottom: '2rem'
												}}
											>
												{this.state.error}
											</Alert>
										)}

										<Form onSubmit={e => e.preventDefault()}>
											<FormGroup className="form-label-group position-relative has-icon-left">
												<Input
													type="email"
													placeholder="Email"
													value={this.state.email}
													onChange={e =>
														this.setState({ email: e.target.value })
													}
												/>
												<div className="form-control-position">
													<Mail size={15} />
												</div>
												<Label>Email</Label>
											</FormGroup>
											<FormGroup className="form-label-group position-relative has-icon-left">
												<Input
													type="password"
													placeholder="Password"
													value={this.state.password}
													onChange={e =>
														this.setState({ password: e.target.value })
													}
												/>
												<div className="form-control-position">
													<Lock size={15} />
												</div>
												<Label>Password</Label>
											</FormGroup>
											{/*<FormGroup className="d-flex justify-content-between align-items-center">
                            <Checkbox
                              color="primary"
                              icon={<Check className="vx-icon" size={16} />}
                              label="Remember me"
                            />
                            <div className="float-right">
                              Forgot Password?
                            </div>
                          </FormGroup>*/}
											<div className="d-flex justify-content-between">
												<div />
												{/*<Button.Ripple color="primary" outline>
                             Register                           
                            </Button.Ripple>*/}
												<Button.Ripple
													color="primary"
													type="submit"
													onClick={this.submit}
												>
													{this.state.loading ? 'Signing In...' : 'Sign In'}
												</Button.Ripple>
											</div>
										</Form>
									</CardBody>
									{/*<div className="auth-footer">
                        <div className="divider">
                          <div className="divider-text">OR</div>
                        </div>
                        <div className="footer-btn">
                          <Button.Ripple className="btn-facebook" color="">
                            <Facebook size={14} />
                          </Button.Ripple>
                          <Button.Ripple className="btn-twitter" color="">
                            <Twitter size={14} stroke="white" />
                          </Button.Ripple>
                          <Button.Ripple className="btn-google" color="">
                            <img src={googleSvg} alt="google" height="15" width="15" />
                          </Button.Ripple>
                          <Button.Ripple className="btn-github" color="">
                            <GitHub size={14} stroke="white" />
                          </Button.Ripple>
                        </div>
                      </div>*/}
								</Card>
							</Col>
						</Row>
					</Card>
				</Col>
			</Row>
		);
	}
}
export default connect(state => ({}), {
	auth
})(Login);
