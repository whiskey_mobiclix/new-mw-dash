import React from 'react';

import Title from '../../../utility/modules/title';
import Wizard from '../../../components/@vuexy/wizard/WizardComponent';

import FirstStep from './firstStep';
import SecondStep from './secondStep';

import Styled from './styled';

class Discrepency extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			activeStep: 0,
			steps: [
				{
					title: 1,
					content: <FirstStep />
				},
				{
					title: 2,
					content: <SecondStep />
				},
				{
					title: 3,
					content: <SecondStep />
				}
			]
		};
	}

	handleActiveStep = step => {
		this.setState({
			activeStep: step
		});
	};

	render() {
		const { steps } = this.state;

		return (
			<Styled>
				<div className="discrepency-title">
					<Title text="Discrepency Check Page" />
				</div>

				<Wizard
					enableAllSteps
					activeStep={this.state.activeStep}
					steps={steps}
					pagination={false}
				/>
			</Styled>
		);
	}
}

export default Discrepency;
