import styled from 'styled-components';

const Styled = styled.div`
	text-align: center;

	.filter {
		display: flex;
		flex-wrap: wrap;
		justify-content: center;

		.mw-selection {
			margin: 10px;
		}
	}

	.file-upload {
		margin-top: 30px;
	}
`;

export default Styled;
