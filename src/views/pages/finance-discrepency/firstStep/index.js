import React from 'react';
import { Button } from 'reactstrap';

import Filter from './Filter';
import FileUpload from './FileUpload';

import Styled from './styled';

class FirstStep extends React.Component {
	render() {
		return (
			<Styled>
				<div className="sub-tilte">
					Please choose details for the discrepency you want to check
				</div>

				<Filter />

				<FileUpload />

				<Button.Ripple className="mt-2" color="primary">
					Next Step
				</Button.Ripple>
			</Styled>
		);
	}
}

export default FirstStep;
