import React, { createRef } from 'react';
import Dropzone from 'react-dropzone';
import { Upload } from 'react-feather';

class FileUpload extends React.Component {
	render() {
		const dropzoneRef = createRef();
		const openDialog = () => {
			// Note that the ref is set async,
			// so it might be null at some point
			if (dropzoneRef.current) {
				dropzoneRef.current.open();
			}
		};

		return (
			<div className="file-upload">
				<Dropzone
					noClick
					noKeyboard
					ref={dropzoneRef}
					onDrop={acceptedFiles => console.log(acceptedFiles)}
				>
					{({ getRootProps, getInputProps, acceptedFiles }) => (
						<section>
							<div {...getRootProps()}>
								<input {...getInputProps()} />

								<div
									className="d-inline-flex flex-column align-items-center"
									type="button"
									onClick={openDialog}
								>
									<Upload size={40} />
									<span className="mt-1">
										{acceptedFiles.length > 0
											? acceptedFiles[0].path
											: 'Upload Statement (Multiple row files, excel, PDF, ...)'}
									</span>
								</div>
							</div>
						</section>
					)}
				</Dropzone>
			</div>
		);
	}
}

export default FileUpload;
