import styled from 'styled-components';

const Styled = styled.div`
	.discrepency-title {
		margin-top: 30px;
		text-align: center;

		.page-title {
			padding: 0;
		}
	}

	.vx-wizard {
		width: 300px;
		left: calc(50% - 150px);

		a {
			background-color: transparent !important;

			&::after {
				display: none;
			}

			&::before {
				display: none;
			}
		}
	}
`;

export default Styled;
