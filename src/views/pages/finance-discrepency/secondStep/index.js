import React from 'react';
import { Row, Col, Input, Card, CardBody, Button } from 'reactstrap';

import Styled from './styled';

class FirstStep extends React.Component {
	render() {
		return (
			<Styled>
				<div className="sub-title">
					<span>Please input these data</span>
					<span className="font-weight-bold">
						All based on T-Mobile Statement
					</span>
				</div>

				<Row className="justify-content-center mt-2 text-left">
					<Col xs={12} sm={12} md={8} lg={7}>
						<Card>
							<CardBody>
								<Row className="mt-2 mb-2 align-items-center">
									<Col sm={2} />
									<Col sm={5}>Total Success Transaction</Col>
									<Col sm={5}>Total Revenue</Col>
								</Row>

								<Row className="mt-2 mb-2 align-items-center">
									<Col sm={2}>Kuwait</Col>
									<Col sm={5}>
										<Input type="text" />
									</Col>
									<Col sm={5}>
										<Input type="text" />
									</Col>
								</Row>

								<Row className="mt-2 mb-2 align-items-center">
									<Col sm={2}>Egypt</Col>
									<Col sm={5}>
										<Input type="text" />
									</Col>
									<Col sm={5}>
										<Input type="text" />
									</Col>
								</Row>
							</CardBody>
						</Card>
					</Col>
				</Row>

				<Button.Ripple className="mt-1" color="primary">
					Submit & Check
				</Button.Ripple>
			</Styled>
		);
	}
}

export default FirstStep;
