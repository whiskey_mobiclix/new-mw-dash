import styled from 'styled-components';

const Styled = styled.div`
	text-align: center;

	.sub-title {
		display: inline-flex;
		flex-direction: column;
	}
`;

export default Styled;
