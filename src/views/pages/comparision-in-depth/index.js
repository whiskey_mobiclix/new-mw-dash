import React from "react"
import Title from "../../../utility/modules/title"
import { Card, CardBody, CardHeader, CardTitle } from "reactstrap"
import ProductsChart from "./chart/products"
import MarketsChart from "./chart/markets"
import VersusSelection from "../../../utility/modules/versus-selection"
import Selection from "../../../components/selection"

class ComparisionInDepth extends React.Component{
	render(){
		return (
			<div className="comparision-in-depth">
				<Title text={"Comparision In-Depth"}/>

				<div style={{
					padding: "0 0 2rem",
					display: "flex",
					justifyContent: "space-between"
				}}>
					<VersusSelection {...{
						items: [
							{
								value: 0,
								options: [
									{
										name: "Subscribers (users)",
										value: 0,
									},
									{
										name: "Active (users)",
										value: 1,
									}
								]
							},
							{
								value: 0,
								options: [
									{
										name: "Active users",
										value: 0,
									},
									{
										name: "Unsubscribed users",
										value: 1,
									}
								]
							},
						]
					}}/>

					<Selection {...{
						value: 12,
						options: [
							{
								name: "Last 12 months",
								value: 12,
							}
						]
					}}/>
				</div>

				<Card>
					<CardHeader>
						<CardTitle>
							Most growing product in <Selection {...{
								value: 0,
								options: [
									{
										name: "Singapore",
										value: 0,
									},
									{
										name: "Iraq",
										value: 1,
									}
								]
							}}/>
						</CardTitle>
					</CardHeader>
					
					<CardBody>
						<ProductsChart />
					</CardBody>
				</Card>

				<Card>
					<CardHeader>
						<CardTitle>
							Most growing markets for <Selection {...{
								value: 0,
								options: [
									{
										name: "Mocoplay",
										value: 0,
									}
								]
							}}/>
						</CardTitle>
					</CardHeader>
					
					<CardBody>
						<MarketsChart />
					</CardBody>
				</Card>
			</div>
		)
	}
}

export default ComparisionInDepth