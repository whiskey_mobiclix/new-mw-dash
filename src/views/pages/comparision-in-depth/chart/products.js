import React from "react"
import Chart from "react-apexcharts"

//CSS
import "./style.scss"

let $primary = "#7367F0",
$success = "#28C76F",
$danger = "#EA5455",
$warning = "#FF9F43",
$info = "#00cfe8",
$primary_light = "#9c8cfc",
$warning_light = "#FFC085",
$danger_light = "#f29292",
$info_light = "#1edec5",
$stroke_color = "#b9c3cd",
$label_color = "#e7eef7",
$purple = "#df87f2",
$white = "#fff"

class ApexColumnCharts extends React.Component {
  state = {
    options: {
      chart: {
        stacked: false,
        toolbar: { show: false }
      },
      plotOptions: {
        bar: {
          columnWidth: "50%",
          horizontal: false,
        }
      },
      colors: [$primary, $info, $danger, $warning],
      dataLabels: {
        enabled: false
      },
      grid: {
        borderColor: $label_color,
        padding: {
          left: 0,
          right: 0
        }
      },
      legend: {
        show: true,
        position: "top",
        horizontalAlign: "left",
        offsetX: 0,
        fontSize: "14px",
        fontWeight: "500",
        markers: {
          radius: 50,
          width: 10,
          height: 10
        }
      },
      stroke: {
	      show: true,
	      width: 5,
	      colors: ['transparent']
	  },
      xaxis: {
        labels: {
          style: {
            colors: $stroke_color
          }
        },
        axisTicks: {
          show: false
        },
        categories: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
        ],
        axisBorder: {
          show: false
        }
      },
      yaxis: {
        tickAmount: 5,
        labels: {
          style: {
            color: $stroke_color
          }
        }
      },
      tooltip: {
        x: { show: false }
      }
    },
    series: [
      {
        name: "Mocoplay",
        data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
      },
      {
        name: "Moco4K",
        data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
      },
      {
        name: "Babystep",
        data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
      },
      {
        name: "WWE",
        data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
      },
    ]
  }

  render() {
    return (
      <Chart
        options={this.state.options}
        series={this.state.series}
        type="bar"
        height={350}
      />
    )
  }
}
export default ApexColumnCharts