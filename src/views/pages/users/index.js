import React from "react"
import { Link } from "react-router-dom"
import Title from "../../../utility/modules/title"
import { Card, CardBody, Row, Col, Button } from "reactstrap"
import Item from "./item"
import USERS from "../../../utility/mockup/users"
import Selection from "../../../components/selection"
import { history } from "../../../history"
import { getManagedUsers } from "../../../redux/actions/users"
import Spinner from "../../../components/@vuexy/spinner/Loading-spinner"
import { Scrollbars } from "react-custom-scrollbars"
import { Box } from "react-feather"

class Users extends React.Component{
	state = {
		loading: true,
		data: [],
		filter: "",
	}

	componentDidMount(){
		getManagedUsers().then(res => {
			this.setState({
				data: (res.data || []).filter(o => o.email),
			})
		}).catch(e => {

		}).finally(() => {
			this.setState({
				loading: false,
			})
		})
	}

	render(){
		if(this.state.loading) return <Spinner />

		const pathname = history.location.pathname;
		const type = /partner/.test(pathname) ? "partner" : "internal"

		let _data = this.state.data;

		if(this.state.filter){
			_data = _data.filter(o => o.status === this.state.filter)
		}

		return (
			<div className="users-list">
				<Title text={`${type} Users Management`}/>

				{
					type === "partner"
					&&
					<div style={{
						padding: "0 0 2rem",
					}}>
						<Selection {...{
							value: this.state.filter,
							options: [
								{
									name: "All users",
									value: "",
								},
								{
									name: "Active users",
									value: "USER_STATUS_ACTIVE",
								},
								{
									name: "Inactive users",
									value: "USER_STATUS_INACTIVE",
								},
							],
							onChange: e => {
								this.setState({
									filter: e.value,
								})
							}
						}}/>
					</div>
				}

				<Card className="mw-tb">
					<CardBody>
						<Scrollbars style={{
							height: `${_data.length ? (_data.length*84 + 60) : 110}px`,
							width: "100%",
						}}>
							<div style={{
								minWidth: "720px",
							}}>
								<div className="mw-tb-row bold">
									<span style={{
										display: "block",
										width: "20%",
										fontSize: "16px",
										paddingRight: "20px",
										textAlign: "center",
									}}>#</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Name</span>

									<span style={{
										display: "block",
										width: "50%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Status</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Activity</span>

									<span style={{
										width: "50%",
										fontSize: "16px",
										paddingRight: "20px",
										textAlign: "center",
										display: "inline-block",
									}}>Action</span>
								</div>

								<div className="mw-dvdr line"/>
								

								{
									_data.length
									?
									_data.map((o, ix) => 
										<React.Fragment key={ix}>
											<Item {...{
												...o,
												number: ix + 1,
												onUpdateStatus: status => {
													this.setState({
														data: this.state.data.map(d => {
															if(d.id === o.id){
																return {
																	...d,
																	status
																}
															}

															return d;
														})
													})
												}
											}}/>
										</React.Fragment>
									)
									:
									<div className="mw-tb-empty">
										There's no data found. You can create new user <Link to="/product/create-user">here</Link>
									</div>	
								}
							</div>
						</Scrollbars>
					</CardBody>
				</Card>
			</div>
		)
	}
}

export default Users