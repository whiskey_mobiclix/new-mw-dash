import React from 'react';
import { connect } from 'react-redux';
import {
	Button,
	Modal,
	ModalBody,
	ModalFooter,
	Input,
	FormGroup,
	InputGroup,
	Alert,
	Label
} from 'reactstrap';
import { Eye, EyeOff } from 'react-feather';
import String from '../../../../utility/helpers/string';
import { changePassword } from '../../../../redux/actions/form/user';
import './style.scss';

class ResetPassword extends React.Component {
	state = {
		showPassword: false,
		showConfirmPassword: false,
		password: '',
		confirmPassword: '',
		error: {
			password: '',
			confirmPassword: ''
		},
		success: false,
		responseError: '',
		loading: false
	};

	validate = () => {
		let flag = true,
			errors = this.state.error;
		const { password, confirmPassword } = this.state;

		if (!password) {
			flag = false;
			errors = {
				...errors,
				password: 'Please enter password'
			};
		}

		if (!confirmPassword) {
			flag = false;
			errors = {
				...errors,
				confirmPassword: 'Please confirm password'
			};
		} else if (confirmPassword !== password) {
			flag = false;
			errors = {
				...errors,
				confirmPassword: 'Confirm password does not match'
			};
		}

		this.setState({
			error: errors
		});

		return flag;
	};

	handleCancel = () => {
		if (!this.state.loading) {
			this.props.toggleModal();
			this.setState({
				responseError: '',
				success: false
			});
		}
	};

	handleReset = () => {
		if (this.state.loading || !this.validate()) return;

		this.setState({
			responseError: '',
			success: false,
			loading: true
		});

		changePassword(this.props.userID, this.state.password)
			.then(res => {
				this.setState({
					responseError: '',
					success: true,
					loading: false
				});

				setTimeout(() => {
					this.props.toggleModal();

					this.setState({
						success: false,
						password: '',
						confirmPassword: ''
					});
				}, 3000);
			})
			.catch(e => {
				try {
					if (e.response.data.code === 1) {
					} else {
						throw new Error('');
					}
				} catch (err) {
					this.setState({
						responseError: 'Something went wrong, please try again.'
					});
					this.setState({
						loading: false
					});
				}
			});
	};

	render() {
		return (
			<div className="reset-password-modal">
				<Modal
					isOpen={this.props.modal}
					toggle={this.props.toggleModal}
					className="modal-dialog-centered"
					backdrop={'static'}
				>
					<div className="mw-modal-overlay" />

					<ModalBody className="pt-2 pl-2 pr-2">
						<h4>Please confirm your action!</h4>
						Do you want to reset this user's password?
						<br />
						<br />
						<div style={{
							padding: "0 0 10px"
						}}>
							{this.state.success && (
								<Alert color="success">
									Congratulation! User's password has been created.
								</Alert>
							)}
							{!!this.state.responseError.length && (
								<Alert color="danger">{this.state.responseError}</Alert>
							)}
						</div>
						<FormGroup className="mb-0">
							<Label
								for="basicInput"
								style={{
									marginBottom: '5px'
								}}
							>
								User's new password
							</Label>
							<div className="has-icon-right position-relative">
								<Input
									tabIndex={1}
									type={this.state.showPassword ? 'text' : 'password'}
									placeholder="Enter a new password"
									value={this.state.password}
									onChange={e => {
										this.setState({
											password: e.target.value
										});
									}}
									onKeyUp={e => {
										if (e.which === 13) {
											this.handleReset();
										}
									}}
								/>
								<div className="form-control-position">
									<button
										style={{
											border: 0,
											background: 0,
											padding: 0,
											outline: 0,
											color: '#aaa'
										}}
										onClick={() => {
											this.setState({
												showPassword: !this.state.showPassword,
												error: {
													...this.state.error,
													password: ''
												}
											});
										}}
									>
										{!this.state.showPassword ? (
											<Eye size={15} />
										) : (
											<EyeOff size={15} />
										)}
									</button>
								</div>
							</div>
							{!!this.state.error.password.length && (
								<span className="frm-error">{this.state.error.password}</span>
							)}
						</FormGroup>
						<br />
						<FormGroup className="mb-0">
							<Label
								for="basicInput"
								style={{
									marginBottom: '5px'
								}}
							>
								Confirm user's new password
							</Label>
							<div className="has-icon-right position-relative">
								<Input
									tabIndex={2}
									type={this.state.showConfirmPassword ? 'text' : 'password'}
									placeholder="Confirm new password"
									value={this.state.confirmPassword}
									onChange={e => {
										this.setState({
											confirmPassword: e.target.value,
											error: {
												...this.state.error,
												confirmPassword: ''
											}
										});
									}}
									onKeyUp={e => {
										if (e.which === 13) {
											this.handleReset();
										}
									}}
								/>
								<div className="form-control-position">
									<button
										style={{
											border: 0,
											background: 0,
											padding: 0,
											outline: 0,
											color: '#aaa'
										}}
										onClick={() => {
											this.setState({
												showConfirmPassword: !this.state.showConfirmPassword
											});
										}}
									>
										{!this.state.showConfirmPassword ? (
											<Eye size={15} />
										) : (
											<EyeOff size={15} />
										)}
									</button>
								</div>
							</div>

							{!!this.state.error.confirmPassword.length && (
								<span className="frm-error">
									{this.state.error.confirmPassword}
								</span>
							)}
						</FormGroup>
					</ModalBody>
					<ModalFooter
						style={{
							zIndex: '10'
						}}
					>
						<Button onClick={this.handleCancel}>Cancel</Button>{' '}
						<Button color="primary" tabIndex={3} onClick={this.handleReset}>
							{this.state.loading ? 'Loading...' : 'Reset'}
						</Button>{' '}
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}

export default connect(state => ({
	user: state.userForm.data
}))(ResetPassword);
