import React from 'react';
import { connect } from 'react-redux';
import { history } from '../../../../history';
import moment from 'moment';
import { setUserFormData, changeStatus } from '../../../../redux/actions/form/user';
import {
	Button,
	UncontrolledButtonDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
	Row,
	Col
} from 'reactstrap';
import { MoreHorizontal } from 'react-feather';
import ResetPasswordModal from '../reset-password';

//CSS
import './style.scss';

class UserItem extends React.Component {
	state = {
		status: {
			value: this.props.status,
			loading: false,
		},
		showActions: false,
		modal: false,

	};

	componentDidUpdate(prevProps){
		if(prevProps.status !== this.props.status){
			this.setState({
				status: {
					...this.state.status,
					value: this.props.status
				}
			})
		}
	}

	status = key => {
		if(this.state.status.loading) return <span className="status">Updating...</span>

		switch (key) {
			case 'USER_STATUS_ACTIVE':
				return <span className="status status-active">Active</span>;
			case 'USER_STATUS_INACTIVE':
				return <span className="status status-inactive">Inactive</span>;
				return 'None';
		}
	};

	toggleModal = () => {
		this.setState({
			modal: !this.state.modal,
			showActions: false
		});
	};

	handleChangeStatus = () => {
		const status = this.state.status.value,
		newStatus = (status === "USER_STATUS_ACTIVE" ? "USER_STATUS_INACTIVE" : "USER_STATUS_ACTIVE")

		if(this.state.status.loading) return

		this.setState({
			status: {
				...this.state.status,
				loading: true,
			}
		})	
		changeStatus(this.props.id, newStatus).then(res => {
			this.setState({
				status: {
					...this.state.status,
					value: newStatus,
				}
			})

			this.props.onUpdateStatus && this.props.onUpdateStatus(newStatus)
		}).catch(e => {
		}).finally(() => {
			this.setState({
				status: {
					...this.state.status,
					loading: false,
				}
			})
		})
	}

	render() {
		return (
			<div className="user-item">
				<div className={`mw-tb-row ${this.props.number % 2 === 0 ? 'odd' : ''}`}>
					<span
						style={{
							paddingRight: '20px',
							width: '20%',
							fontSize: '15px',
							display: 'inline-block',
							textAlign: 'center'
						}}
					>
						{this.props.number}
					</span>

					<span
						style={{
							paddingRight: '20px',
							width: '100%',
							fontSize: '15px'
						}}
					>
						<span>
							<b>{this.props.name}</b>
							<br/>
							<i>{this.props.email}</i>
						</span>
					</span>

					<span
						style={{
							paddingRight: '20px',
							width: '50%',
							fontSize: '15px'
						}}
					>
						{this.status(this.state.status.value)}
					</span>

					<span
						style={{
							paddingRight: '20px',
							width: '100%',
							fontSize: '15px'
						}}
					>
						Last login at:
						<i
							style={{
								marginLeft: '3px',
								fontWeight: '400'
							}}
						>
							{this.props.lastLoginAt
								? moment(parseInt(this.props.lastLoginAt * 1000)).format(
										'HH:mm, DD MMM'
								  )
								: '(Not yet)'}
						</i>
					</span>

					<span
						style={{
							paddingRight: '20px',
							width: '50%',
							fontSize: '15px',
							textAlign: 'center',
							display: 'inline-block'
						}}
					>
						<div className="action-group">
							<button
								onClick={() => {
									this.setState({
										showActions: true
									});
								}}
							>
								<MoreHorizontal size={20} />
							</button>

							{this.state.showActions && (
								<>
									<div
										className="action-overlay"
										onClick={() => {
											this.setState({
												showActions: false
											});
										}}
									/>

									<div className="action-dropdown">
										<button onClick={this.toggleModal}>
											Reset password
										</button>
										<button onClick={() => {
											this.setState({
												showActions: false
											});
											this.handleChangeStatus();
										}}>
											{this.state.status.value === "USER_STATUS_ACTIVE" ? "Deactivate" : "Active"} this user
										</button>
									</div>
								</>
							)}
						</div>
					</span>
				</div>

				<ResetPasswordModal
					modal={this.state.modal}
					toggleModal={this.toggleModal}
					userID={this.props.id}
				/>
			</div>
		);
	}
}

export default connect(
	state => ({
		me: state.user
	}),
	{
		setUserFormData
	}
)(UserItem);
