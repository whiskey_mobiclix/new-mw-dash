import React from "react"
import {
  Card,
  CardHeader,
  CardBody,
  Row,
  Col,
} from "reactstrap"
import csImg from "../../assets/img/pages/rocket.png"
import "../../assets/scss/pages/coming-soon.scss"
import Countdown from "react-countdown-now"
import moment from "moment"

class ComingSoon extends React.Component {
  renderTimer = ({ days, hours, minutes, seconds }) => {
    return (
      <React.Fragment>
        <div className="clockCard px-1">
          <p>{days}</p>
          <p className="bg-amber clockFormat lead px-1 black"> Days </p>
        </div>
        <div className="clockCard px-1">
          <p>{hours}</p>
          <p className="bg-amber clockFormat lead px-1 black"> Hours </p>
        </div>
        <div className="clockCard px-1">
          <p>{minutes}</p>
          <p className="bg-amber clockFormat lead px-1 black"> Minutes </p>
        </div>
        <div className="clockCard px-1">
          <p>{seconds}</p>
          <p className="bg-amber clockFormat lead px-1 black"> Seconds </p>
        </div>
      </React.Fragment>
    )
  }

  render() {
    return (
      <Row className="d-flex align-items-center justify-content-center m-0">
        <Col xl="5" md="8" className="px-md-0 px-2">
          <Card className="mb-0">
            <CardHeader className="justify-content-center">
              <h2>We are launching soon</h2>
            </CardHeader>
            <CardBody className="text-center">
              <img src={csImg} alt="csImg" className="img-fluid width-150" />
              <div className="text-center getting-started pt-2 d-flex justify-content-center flex-wrap">
                <Countdown
                  date={Date.now() + moment("2020/05/30").valueOf() - moment().valueOf()}
                  renderer={this.renderTimer}
                />
              </div>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
  }
}
export default ComingSoon