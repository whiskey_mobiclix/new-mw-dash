import React from "react"
import Selection from "../../../components/selection"

//CSS
import "./style.scss"

class Filter extends React.Component{
	render(){
		return (
			<div className="product-operation-filter">
				<Selection {...{
					value: 6,
					options: [
						{
							name: "Last 6 months",
							value: 6,
						},
					],
				}}/>
			</div>
		)
	}
}

export default Filter