import React from "react"
import Title from "../../../utility/modules/title"
import Filter from "./Filter"
import { LineChart, BarChart } from "../../../utility/modules/chart"
import { Card, CardBody, CardHeader, CardTitle, Row, Col } from "reactstrap"
import Comparision from "../../../utility/modules/comparision"

//CSS
import "./style.scss"

class ProductOperation extends React.Component{
	render(){
		return (
			<div className="product-operation">
				<Title text={"Operation Perfomance"}/>
				
				<Filter />

				<Row>
					<Col lg={4} md={6}>
						<Card>
					        <CardHeader className="mb-1">
					          <CardTitle>
					            Avg. Site loading time (sec)
					          </CardTitle>
					        </CardHeader>

					        <CardBody className="pt-0">
					        	<div className="pie-chart-wrapper">
						          <LineChart {...{
						            categories: [
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              ""
						            ],
						            series: [
						              {
						                name: "",
						                data: [140, 180, 150, 205, 160, 295, 125, 255, 205, 305, 240, 295]
						              },
						            ]
						          }}/>
						        </div>
					        </CardBody>
					      </Card>
					</Col>

					<Col lg={4} md={6}>
						<Card>
					        <CardHeader className="mb-1">
					          <CardTitle>
					            Avg. Redirection speed (sec)
					          </CardTitle>
					        </CardHeader>

					        <CardBody className="pt-0">
					        	<div className="pie-chart-wrapper">
						          <LineChart {...{
						            categories: [
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              ""
						            ],
						            series: [
						              {
						                name: "",
						                data: [140, 180, 150, 255, 205, 305, 240, 295, 205, 160, 295, 125]
						              },
						            ]
						          }}/>
						        </div>
					        </CardBody>
					      </Card>
					</Col>

					<Col lg={4} md={6}>
						<Card>
					        <CardHeader className="mb-1">
					          <CardTitle>
					            Avg. Response time (sec)
					          </CardTitle>
					        </CardHeader>

					        <CardBody className="pt-0">
					        	<div className="pie-chart-wrapper">
						          <LineChart {...{
						            categories: [
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              "",
						              ""
						            ],
						            series: [
						              {
						                name: "",
						                data: [255, 205, 305, 240, 295, 140, 180, 150, 205, 160, 295, 125]
						              },
						            ]
						          }}/>
						        </div>
					        </CardBody>
					      </Card>
					</Col>
				</Row>

				<h3 className="product-operation-title">Site speed (sec)</h3>

				<Row>
					<Col lg={4} md={6}>
						<Card>
							<CardHeader>
								<CardTitle>
									By platforms
								</CardTitle>
							</CardHeader>
							<CardBody className="pt-2 pr-3 pl-3 pb-2">
								<BarChart {...{
									series: [
										{
											name: "Windows",
											value: .9,
										},
										{
											name: "iOS",
											value: 1.1,
										},
										{
											name: "Android",
											value: 1.3,
										},
										{
											name: "Mac OS",
											value: 1.7,
										},
									]
								}}/>
							</CardBody>
						</Card>
					</Col>

					<Col lg={4} md={6}>
						<Card>
							<CardHeader>
								<CardTitle>
									By MNOs
								</CardTitle>
							</CardHeader>
							<CardBody className="pt-2 pr-3 pl-3 pb-2">
								<BarChart {...{
									series: [
										{
											name: "Centili",
											value: .7,
										},
										{
											name: "Orange",
											value: 1.1,
										},
										{
											name: "T-Mobile",
											value: 1.3,
										},
									]
								}}/>
							</CardBody>
						</Card>
					</Col>

					<Col lg={4} md={6}>
						<Card>
							<CardHeader>
								<CardTitle>
									By markets
								</CardTitle>
							</CardHeader>
							<CardBody className="pt-2 pr-3 pl-3 pb-2">
								<BarChart {...{
									series: [
										{
											name: "Poland",
											value: .7,
										},
										{
											name: "Indonesia",
											value: 1.1,
										},
										{
											name: "Malaysia",
											value: 1.3,
										},
										{
											name: "Egypt",
											value: 1.3,
										},
									]
								}}/>
							</CardBody>
						</Card>
					</Col>
				</Row>
			</div>
		)
	}
}

export default ProductOperation