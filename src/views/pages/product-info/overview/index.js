import React from "react"
import { Card, CardBody, Row, Col } from "reactstrap"
import { FileText } from "react-feather"

class ProductOverview extends React.Component{
	render(){
		const content = this.props.content

		return (
			<>
				<Card>
					<CardBody className="pt-3">
						<div className="mw-text-section">
							<h3>About Product</h3>
							<p dangerouslySetInnerHTML={{__html: content.general.about}} />
						</div>

						<div className="mw-text-section">
							<h3>Address</h3>
							<p>
								<b><a target="_blank" rel="noopener noreferrer" href={content.general.address}>{content.general.address}</a></b>
							</p>
						</div>

						<div className="mw-text-section">
							<h3>Vertical & Category</h3>
							<p>
								{content.general.vertical.toUpperCase()}
							</p>
						</div>

						<div className="mw-text-section">
							<h3>Documentations</h3>
							<p>
								{
									content.general.documents.map((o, ix) => 
										<a href={o.url} target="_blank" className="mw-doc-item" key={ix}>
											<FileText />
											<span>{o.name}</span>
										</a>
									)
								}
							</p>
						</div>
					</CardBody>
				</Card>

				<Card>
					<CardBody>
						<div className="mw-text-section mw-tb">
							<h3 className="mb-3">Point Of Contacts</h3>
							
							<Row className={this.props.number % 2 === 0 ? "odd" : ""}>
								<Col>
									<div className="mw-tb-row bold">
										<span style={{
											paddingRight: "20px",
											width: "50%",
											fontSize: "15px",
										}}>
											<span>
												<span target="_blank">Name</span>
											</span>
										</span>

										<span style={{
											paddingRight: "20px",
											width: "50%",
											fontSize: "15px",
										}}>Role</span>

										<span style={{
											paddingRight: "20px",
											width: "100%",
											fontSize: "15px",
										}}>Email</span>

										<span style={{
											paddingRight: "20px",
											width: "50%",
											fontSize: "15px",
										}}>
											Action
										</span>
									</div>
								</Col>
							</Row>

							<div className="mw-dvdr line"/>

							{
								content.general.contacts.map((o, ix) =>
									<React.Fragment key={ix}>
										<Row>
											<Col>
												<div className="mw-tb-row">
													<span style={{
														paddingRight: "20px",
														width: "50%",
														fontSize: "15px",
													}}>
														<span className="bold">
															<span target="_blank">{o.name}</span>
														</span>
													</span>

													<span style={{
														paddingRight: "20px",
														width: "50%",
														fontSize: "15px",
													}}>{o.role}</span>

													<span style={{
														paddingRight: "20px",
														width: "100%",
														fontSize: "15px",
													}}><i>{o.email}</i></span>

													<span style={{
														paddingRight: "20px",
														width: "50%",
														fontSize: "15px",
													}}>
														<a href={`mailto:${o.email}`}>Contact</a>
													</span>
												</div>
											</Col>
										</Row>

										<div className="mw-dvdr"/>
									</React.Fragment>
								)
							}

						</div>
					</CardBody>
				</Card>
			</>
		)
	}
}

export default ProductOverview