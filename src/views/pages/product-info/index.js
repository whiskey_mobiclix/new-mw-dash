import React from "react"
import { connect } from "react-redux"
import Title from "../../../utility/modules/title"
import Tab from "../../../utility/modules/tab"
import Information from "../../../utility/information"
import { Redirect } from "react-router-dom"

//CSS
import "./style.scss"

import Overview from "./overview"

class ProductInfo extends React.Component{
	state = {
		active: 0,
	}

	render(){
		let product = "", content = {}

		try{
			product = this.props.filter.product.value
			content = Information[product]
		}catch(e){
			console.log(e)
		}

		if(!content) return <Redirect to={"/error/404"}/>

		return (
			<div className="product-info">
				<Title text={"Product Information"}/>

				{
					this.state.active == 0
					&&
					<Overview {...{content}}/>
				}
			</div>
		)
	}
}

export default connect(
	state => ({
		filter: state.filter,
	})
)(ProductInfo)