import React from "react"
import { connect } from "react-redux"
import { Card, CardTitle, CardHeader, CardBody } from "reactstrap"
import { Rss } from "react-feather"
import StatusBlock from "../../../../utility/modules/status-block"
import Config from "../../../../utility/helpers/config"
import { Scrollbars } from 'react-custom-scrollbars'

class Gateways extends React.Component{
	render(){
		const data = Config.groupByMarkets(this.props.config, this.props.product)

		return (
			<div className="product-status-gateway">
				<Card>
					<CardHeader>
						 <div className="card-heading">
							<CardTitle>
								<Rss size={20} className="mr-1"/>
								Gateways
							</CardTitle>
							<p>Intergrating networks and aggregators in all markets.</p>
						</div>
					</CardHeader>

					<CardBody className="pb-3 pr-3 pl-3">
						<div className="titles">
							<div>Market</div>
							<div>Network</div>
							<div>Aggregator</div>
						</div>

						<div className="contents">
							<Scrollbars style={{
								height: "162px",
							}}>
								<div style={{
									width: "max-content",
								}}>
									{
										data.map((o, ix) => 
											<StatusBlock {...{
												data: o,
												key: ix,
											}}/>
										)
									}
								</div>
							</Scrollbars>
						</div>

						<div style={{
							clear: "both",
						}}/>
					</CardBody>
				</Card>
			</div>
		)
	}
}

export default connect(
	state => ({
		config: state.app.config,
		product: state.filter.product.value,
	})
)(Gateways)