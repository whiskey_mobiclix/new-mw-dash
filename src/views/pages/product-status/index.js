import React from "react"
import { connect } from "react-redux"
import { Layers, Rss } from "react-feather"
import Title from "../../../utility/modules/title"
import Gateways from "./gateways"
import Markets from "./markets"

//CSS
import "./style.scss"

class ProductStatus extends React.Component{
	render(){
		return (
			<div className="product-markets mw-tb">
				<Title text={"Product Status"}/>

				<Markets />
				<Gateways />
			</div>
		)
	}
}

export default connect(
	state => ({
		app: state.app,
	})
)(ProductStatus)