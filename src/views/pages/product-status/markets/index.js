import React from "react"
import { connect } from "react-redux"
import { Card, CardTitle, CardHeader, CardBody } from "reactstrap"
import { Layers } from "react-feather"
import StatusBlock from "../../../../utility/modules/status-block"
import Config from "../../../../utility/helpers/config"
import { Scrollbars } from 'react-custom-scrollbars'

class Markets extends React.Component{
	render(){
		const data = Config.groupByRegions(this.props.config, this.props.product)

		return (
			<div className="product-status-markets">
				<Card>
					<CardHeader>
						 <div className="card-heading">
							<CardTitle>
								<Layers size={20} className="mr-1"/>
								Markets
							</CardTitle>
							<p>Lauching networks in all markets.</p>
						</div>
					</CardHeader>

					<CardBody className="pb-3 pr-3 pl-3">
						<div className="titles">
							<div>Region</div>
							<div>Market</div>
							<div>Network</div>
						</div>

						<div className="contents">
							<Scrollbars style={{
								height: "162px",
							}}>
								<div style={{
									width: "max-content",
								}}>
									{
										data.map((o, ix) => 
											<StatusBlock {...{
												data: o,
												key: ix,
											}}/>
										)
									}
								</div>
							</Scrollbars>
						</div>

						<div style={{
							clear: "both",
						}}/>
					</CardBody>		
				</Card>
			</div>
		)
	}
}

export default connect(
	state => ({
		config: state.app.config,
		product: state.filter.product.value,
	})
)(Markets)