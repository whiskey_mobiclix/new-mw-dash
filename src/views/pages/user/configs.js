export const types = [
	{
		label: "Admin",
		value: "admin",
	},
	{
		label: "Partner",
		value: "partner",
	},
]


export const roles = [
	{
        "label": "C-Level",
        "value": "C-Level",
    },
    // {
    //     "label": "Executive",
    //     "value": "Executive",
    // },
    // {
    //     "label": "Investors",
    //     "value": "Investors",
    // },
    {
        "label": "Junior-Level",
        "value": "Junior-Level",
    },
    {
        "label": "Mid-Level",
        "value": "Mid-Level",
    },
    // {
    //     "label": "Vetrical-Manager",
    //     "value": "Vetrical-Manager",
    // }
]

export const products = [
	{
		label: "GameOn",
		value: "c-level",
	},
	{
		label: "Vanguard",
		value: "mid-level",
	},
]

export const permissions = [
	{
		name: "View product details",
		value: 1,
	},
	{
		name: "View finance overview",
		value: 2,
	},
	{
		name: "Create partner users",
		value: 3,
	},
	{
		name: "Use finance tools",
		value: 4,
	},
	{
		name: "View monthly statement",
		value: 5,
	},
	{
		name: "View accquisition",
		value: 6,
	},
]