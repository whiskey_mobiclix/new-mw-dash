import React from 'react';
import { connect } from 'react-redux';
import {
	Row,
	Col,
	Input,
	FormGroup,
	InputGroup,
	InputGroupAddon,
	Button
} from 'reactstrap';
import { setUserFormData } from '../../../../redux/actions/form/user';
import COLOR from '../../../../utility/helpers/color';
import { Eye, EyeOff } from 'react-feather';

class UserInformation extends React.Component {
	state = {
		showPassword: false,
		showConfirmPassword: false,
	};

	render() {
		return (
			<div className="user-information">
				<Row>
					<Col>
						<h5 className="my-1 text-bold-600">
							Full name{' '}
							<span
								style={{
									color: COLOR.$danger
								}}
							>
								*
							</span>
						</h5>
						<FormGroup className="form-label-group">
							<Input
								type="text"
								tabIndex={1}
								placeholder="Enter user name"
								value={this.props.user.current.name}
								onChange={e => {
									this.props.onNameChange && this.props.onNameChange();
									this.props.setUserFormData({
										...this.props.user.current,
										name: e.target.value
									});
								}}
								onKeyUp={e => {
									if (e.which === 13) {
										this.props.onSubmit && this.props.onSubmit();
									}
								}}
							/>
							{!!this.props.error.name.length && (
								<span className="frm-error">{this.props.error.name}</span>
							)}
						</FormGroup>
					</Col>
				</Row>

				<Row>
					<Col>
						<h5 className="my-1 text-bold-600">
							Email{' '}
							<span
								style={{
									color: COLOR.$danger
								}}
							>
								*
							</span>
						</h5>
						<FormGroup className="form-label-group">
							<Input
								type="email"
								tabIndex={2}
								placeholder="Enter user email"
								value={this.props.user.current.email}
								onChange={e => {
									this.props.onEmailChange && this.props.onEmailChange();
									this.props.setUserFormData({
										...this.props.user.current,
										email: e.target.value
									});
								}}
								onKeyUp={e => {
									if (e.which === 13) {
										this.props.onSubmit && this.props.onSubmit();
									}
								}}
							/>

							{!!this.props.error.email.length && (
								<span className="frm-error">{this.props.error.email}</span>
							)}
						</FormGroup>
					</Col>
				</Row>
				{!this.props.user.current.editing && (
					<>
						<Row>
							<Col>
								<h5 className="my-1 text-bold-600">
									Password{' '}
									<span
										style={{
											color: COLOR.$danger
										}}
									>
										*
									</span>
								</h5>
								<FormGroup className="form-label-group has-icon-right position-relative">
									<Input
										className="has-icon-left"
										type={this.state.showPassword ? 'text' : 'password'}
										tabIndex={3}
										placeholder="Enter user password"
										value={this.props.user.current.password}
										onChange={e => {
											this.props.onPasswordChange &&
												this.props.onPasswordChange();
											this.props.setUserFormData({
												...this.props.user.current,
												password: e.target.value
											});
										}}
										onKeyUp={e => {
											if (e.which === 13) {
												this.props.onSubmit && this.props.onSubmit();
											}
										}}
									/>
									{!!this.props.error.password.length && (
										<span className="frm-error">
											{this.props.error.password}
										</span>
									)}
									<div className="form-control-position">
										<button
											style={{
												border: 0,
												background: 0,
												padding: 0,
												outline: 0,
												color: '#aaa'
											}}
											onClick={() => {
												this.setState({
													showPassword: !this.state.showPassword
												});
											}}
										>
											{!this.state.showPassword ? (
												<Eye size={15} />
											) : (
												<EyeOff size={15} />
											)}
										</button>
									</div>
								</FormGroup>
							</Col>
						</Row>

						<Row>
							<Col>
								<h5 className="my-1 text-bold-600">
									Confirm Password{' '}
									<span
										style={{
											color: COLOR.$danger
										}}
									>
										*
									</span>
								</h5>
								<FormGroup className="form-label-group has-icon-right position-relative">
									<Input
										className="has-icon-left"
										type={this.state.showConfirmPassword ? 'text' : 'password'}
										tabIndex={4}
										placeholder="Confirm password"
										value={this.props.user.current.confirmPassword}
										onChange={e => {
											this.props.onConfirmPasswordChange &&
												this.props.onConfirmPasswordChange();
											this.props.setUserFormData({
												...this.props.user.current,
												confirmPassword: e.target.value
											});
										}}
										onKeyUp={e => {
											if (e.which === 13) {
												this.props.onSubmit && this.props.onSubmit();
											}
										}}
									/>
									{!!this.props.error.confirmPassword.length && (
										<span className="frm-error">
											{this.props.error.confirmPassword}
										</span>
									)}
									<div className="form-control-position">
										<button
											style={{
												border: 0,
												background: 0,
												padding: 0,
												outline: 0,
												color: '#aaa'
											}}
											onClick={() => {
												this.setState({
													showConfirmPassword: !this.state.showConfirmPassword
												});
											}}
										>
											{!this.state.showConfirmPassword ? (
												<Eye size={15} />
											) : (
												<EyeOff size={15} />
											)}
										</button>
									</div>
								</FormGroup>
							</Col>
						</Row>
					</>
				)}
			</div>
		);
	}
}

export default connect(
	state => ({
		user: state.userForm.data
	}),
	{
		setUserFormData
	}
)(UserInformation);
