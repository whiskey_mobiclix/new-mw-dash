import React from 'react';
import { connect } from 'react-redux';
import Title from '../../../utility/modules/title';
import {
	Card,
	CardBody,
	Row,
	Col,
	Input,
	FormGroup,
	Label,
	Button,
	Alert
} from 'reactstrap';
import './style.scss';
import '../../../assets/scss/plugins/forms/react-select/_react-select.scss';
import { setUserFormData } from '../../../redux/actions/form/user';
import { history } from '../../../history';
import { createUser } from '../../../redux/actions/form/user';
import REGEX from '../../../utility/constants/regex';
import Response from "../../../utility/helpers/response"

import General from './general';
import Information from './information';

class CreateUser extends React.Component {
	state = {
		name: this.props.user.current.name,
		loading: false,
		error: {
			email: '',
			name: '',
			password: '',
			confirmPassword: ''
		},
		responseError: "",
		success: false
	};

	componentWillUnmount() {
		if (this.props.user.current.editing) {
			this.props.setUserFormData(this.props.user.init);
		}
	}

	validate = () => {
		let flag = true;
		const { email, name, password, confirmPassword } = this.props.user.current;
		let _errors = this.state.error;

		if (!email) {
			_errors = {
				..._errors,
				email: 'Please enter your email address'
			};
			flag = false;
		} else if (!REGEX.email.test(email)) {
			_errors = {
				..._errors,
				email: 'Email format is invalid'
			};
			flag = false;
		}

		if (!name) {
			_errors = {
				..._errors,
				name: "Please enter user's name"
			};
			flag = false;
		}

		if (!password) {
			_errors = {
				..._errors,
				password: "Please enter user's password"
			};
			flag = false;
		}

		if (confirmPassword !== password) {
			_errors = {
				..._errors,
				confirmPassword: 'Confirm password does not match'
			};
			flag = false;
		}

		this.setState({
			error: _errors
		});

		return flag;
	};

	handleClick = () => {
		if (this.state.loading || !this.validate()) return;

		this.setState({
			loading: true,
			responseError: "",
		});

		if(!this.props.user.current.editing){
			//Create user
			if(this.props.me.data.group === "USER_GROUP_PARTNER"){
				this.createUserByPartner()
			}
		} else {
			//Edit user
		}
	};

	createUserByPartner = () => {
		const { name, email, password } = this.props.user.current

		const userData = {
			name,
			email,
			password,
			product: this.props.me.data.managed_product_code,
		}

		createUser(userData).then(res => {
			this.setState({
				responseError: "",
				success: true,
			})

			setTimeout(() => {
				this.setState({
					success: false,
					loading: false,
				})
				this.props.setUserFormData(this.props.user.init)
			}, 3000)
		}).catch(e => {
			try{
				if(e.response.data.code === Response.codes.DUPLICATE){
					this.setState({
						responseError: "User's email has already existed. Please create new user with another email.",
					})
					this.setState({
						loading: false,
					})
				} else {
					throw new Error("")
				}
			}catch(err){
				this.setState({
					responseError: "Something went wrong, please try again.",
				})
				this.setState({
					loading: false,
				})
			}
		})
	}

	render() {
		return (
			<div className="create-user mw-tb pt-2">
				<Title
					text={
						this.props.user.current.editing
							? `Edit user: ${this.state.name}`
							: 'Create new user'
					}
				/>
				<p
					style={{
						paddingLeft: '18px',
						marginBottom: '30px'
					}}
				>
					Please enter the fields below to create new user.<br />Note: All feilds are required
				</p>

				<Card>
					<CardBody className="pl-3 pr-3 pb-3">
						<Information
							{...{
								error: this.state.error,
								onNameChange: () => {
									this.setState({
										error: {
											...this.state.error,
											name: ''
										}
									});
								},
								onEmailChange: () => {
									this.setState({
										error: {
											...this.state.error,
											email: ''
										}
									});
								},
								onPasswordChange: () => {
									this.setState({
										error: {
											...this.state.error,
											password: ''
										}
									});
								},
								onConfirmPasswordChange: () => {
									this.setState({
										error: {
											...this.state.error,
											confirmPassword: ''
										}
									});
								},
								onSubmit: () => {
									this.handleClick();
								}
							}}
						/>
						<General />

						{this.state.success && (
							<Alert color="success">
								Congratulation! User account has been created.
							</Alert>
						)}

						{!!this.state.responseError.length && (
							<Alert color="danger">
								{this.state.responseError}
							</Alert>
						)}

						<div
							style={{
								textAlign: 'right',
								marginTop: '30px'
							}}
						>
							{!this.state.loading && (
								<Button.Ripple
									className="mr-1"
									color="light"
									onClick={() => {
										if (!this.props.user.current.editing) {
											this.props.setUserFormData(this.props.user.init);
											this.setState({
												error: {
													email: '',
													name: '',
													password: '',
													confirmPassword: ''
												},
												responseError: "",
												success: false,
											});
										} else {
											history.goBack();
										}
									}}
								>
									{this.props.user.current.editing
										? 'Cancel'
										: 'Reset the form'}
								</Button.Ripple>
							)}

							{this.props.user.current.editing && (
								<Button.Ripple
									className="mr-1"
									color="light"
									onClick={() => {
										this.toggleModal();
									}}
								>
									Reset password
								</Button.Ripple>
							)}

							<Button.Ripple
								tabIndex={5}
								className="bg-gradient-primary"
								color="none"
								onClick={this.handleClick}
							>
								{this.state.loading
									? 'Loading ...'
									: this.props.user.current.editing
									? 'Save'
									: 'Create'}
							</Button.Ripple>
						</div>
					</CardBody>
				</Card>
			</div>
		);
	}
}

export default connect(
	state => ({
		user: state.userForm.data,
		me: state.user,
	}),
	{
		setUserFormData
	}
)(CreateUser);
