import React from "react"
import { Row, Col, FormGroup } from "reactstrap"
import { permissions } from "../configs"

//CSS
import "./style.scss"

class UserAdvanceSettings extends React.Component{
	state = {
		search: "",
	}

	render(){
		const items = permissions.filter(
			o => o.name.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1
			||
			(o.value + "").toLowerCase().indexOf(this.state.search.toLowerCase()) > -1
		)

		return (
			<div className="user-advance-settings mt-1">
				<Row>
	            	<Col>
			            <h5 className="my-1 text-bold-600">List permissions</h5>

			            <FormGroup className="mb-0">
			                <Row>
			                	{
			                		items.map((o, ix) =>
			                			<Col lg={6} key={ix}>
							                <div id="p-1" style={{
							                	marginBottom: "15px"
							                }}>
								                <span>{o.name}</span>
								            </div>
							            </Col>
			                		)
			                	}

			                	{
			                		!items.length
			                		&&
			                		<Col>
			                			<div style={{
			                				fontSize: "13px",
			                				color: "#888",
			                				fontStyle: "italic"
			                			}}>There's no result.</div>
			                		</Col>
			                	}
					        </Row>
			            </FormGroup>
		            </Col>
	            </Row>
			</div>
		)
	}
}

export default UserAdvanceSettings