import React from "react"
import { connect } from "react-redux"
import { Row, Col } from "reactstrap"
import Select from "react-select"
import { types, roles, products } from "../configs"
import COLOR from "../../../../utility/helpers/color"
import Wrapper from "../../../../utility/modules/wrapper"
import Advance from "../advance"

class UserGeneral extends React.Component{
	render(){
		const defaultRole = this.props.user.current.editing ? roles.filter(o => o.label === this.props.user.current.role)[0] || {} : roles[0]

		return (
			<div className="user-general">
				<Wrapper group={["USER_GROUP_ADMIN"]}>
					<Row>
						<Col>
							<h5 className="my-1 text-bold-600">Select account type <span style={{
				            	color: COLOR.$danger,
				            }}>*</span></h5>
				            <Select
				                classNamePrefix="select"
				                defaultValue={types[0]}
				                name="color"
				                options={types}
				            />
			            </Col>
		            </Row>

		            <div className="mw-dvdr"/>
	            </Wrapper>

	            <Wrapper group={["USER_GROUP_ADMIN"]}>
		            <Row>
		            	<Col>
				            <h5 className="my-1 text-bold-600">Select product <span style={{
				            	color: COLOR.$danger,
				            }}>*</span></h5>
				            <Select
				                classNamePrefix="select"
				                defaultValue={products[0]}
				                name="color"
				                options={products}
				            />
			            </Col>
		            </Row>

		            <div className="mw-dvdr"/>
	            </Wrapper>

	            <Wrapper group={["USER_GROUP_ADMIN"]}>
		            <Row>
		            	<Col>
				            <h5 className="my-1 text-bold-600">Select role <span style={{
				            	color: COLOR.$danger,
				            }}>*</span></h5>
				            <Select
				                classNamePrefix="select"
				                defaultValue={defaultRole}
				                name="color"
				                options={roles}
				            />
			            </Col>
		            </Row>

	            	<Advance />
	            </Wrapper>
			</div>
		)
	}
}

export default connect(
	state => ({
		user: state.userForm.data,
	})
)(UserGeneral)