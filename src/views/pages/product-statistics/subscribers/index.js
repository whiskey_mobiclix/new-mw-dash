import React from "react"
import { connect } from "react-redux"
import { Card, CardTitle, CardHeader, CardBody, Row, Col } from "reactstrap"
import Comparision from "../../../../utility/modules/comparision"
import { PieChart } from "../../../../utility/modules/chart"
import Number from "../../../../utility/helpers/number"
import Data from "../../../../utility/helpers/data"
import Country from "../../../../utility/helpers/country"
import String from "../../../../utility/helpers/string"

class SubscribersStatistics extends React.Component{
	render(){
		const marketsData = Data.groupByMarkets(this.props.users.data).filter(o => o.total_users).sort((a, b) => {
			if(parseInt(a.total_users || 0) < parseInt(b.total_users || 0)) return 1
			return -1
		})

		const mnosData = Data.groupByMNOs(this.props.users.data).filter(o => o.total_users).sort((a, b) => {
			if(parseInt(a.total_users || 0) < parseInt(b.total_users || 0)) return 1
			return -1
		})

		return (
			<div className="statistics-subscribers">
				<h3 className="product-statistics-title">
					Total New Users: <Comparision {...{
						current: Number.total(this.props.users.data.map(o => parseInt(o.new_users || 0))),
						previous: Number.total(this.props.users.previous.map(o => parseInt(o.new_users || 0))),
					}}/>
				</h3>

				<Row>
					<Col lg={4} md={6}>
						<Card>
					        <CardHeader className="mb-1">
					          <CardTitle>
					            MNOs
					          </CardTitle>
					        </CardHeader>

					        <CardBody className="pt-0">
					        	<div className="pie-chart-wrapper">
						          <PieChart {...{
						            labels: mnosData.map(o => String.capitalize(o.mno.toLowerCase())),
						            series: mnosData.map(o => parseInt(o.total_users || 0))
						          }}/>
						        </div>
					        </CardBody>
					      </Card>
					</Col>

					<Col lg={4} md={6}>
						<Card>
					        <CardHeader className="mb-1">
					          <CardTitle>
					            Markets
					          </CardTitle>
					        </CardHeader>

					        <CardBody className="pt-0">
					        	<div className="pie-chart-wrapper">
						          <PieChart {...{
						            labels: marketsData.map(o => Country.getCountryByCode(o.country)),
						            series: marketsData.map(o => parseInt(o.total_users || 0))
						          }}/>
						        </div>
					        </CardBody>
					      </Card>
					</Col>

					{/*<Col lg={4} md={6}>
						<Card>
					        <CardHeader className="mb-1">
					          <CardTitle>
					            Aggregators
					          </CardTitle>
					        </CardHeader>

					        <CardBody className="pt-0">
					        	<div className="pie-chart-wrapper">
						          <PieChart {...{
						            labels: ["centili", "Apigate", "Direct"],
						            series: [58.6, 34.9, 6.5],
						          }}/>
						        </div>
					        </CardBody>
					      </Card>
					</Col>*/}
				</Row>
			</div>
		)
	}
}

export default connect(
	state => ({
		users: state.statistics.users,
	}),
	{}
)(SubscribersStatistics)