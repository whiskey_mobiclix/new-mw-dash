import React from 'react';
import { connect } from 'react-redux';
import { Filter } from '../../../utility/modules';
import Title from '../../../utility/modules/title';
import Spinner from '../../../components/@vuexy/spinner/Loading-spinner';
import Subscribers from './subscribers';
import Transactions from './transactions';
// import ActiveUsers from "./active-users"
import Revenue from './revenue';

//CSS
import './style.scss';

class Statistics extends React.Component {
	render() {
		if (this.props.app.locked) return <Spinner />;

		return (
			<div className="product-statistics">
				<Title text={'Product Statistics'} />
				<Filter />

				<Revenue />
				<Transactions />
				<Subscribers />
			</div>
		);
	}
}

export default connect(state => ({
	app: state.app,
	statistics: state.statistics
}))(Statistics);
