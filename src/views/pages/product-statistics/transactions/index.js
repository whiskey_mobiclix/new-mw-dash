import React from 'react';
import { connect } from 'react-redux';
import { Card, CardTitle, CardHeader, CardBody, Row, Col } from 'reactstrap';

import Number from '../../../../utility/helpers/number';
import Data from '../../../../utility/helpers/data';
import Comparision from '../../../../utility/modules/comparision';
import { PieChart } from '../../../../utility/modules/chart';
import Country from '../../../../utility/helpers/country';
import String from '../../../../utility/helpers/string';

class Transactions extends React.Component {
	render() {
		const marketsData = Data.groupByMarkets(this.props.transactions.data)
			.filter(o => o.total_trans)
			.sort((a, b) => {
				if (parseFloat(a.total_trans || 0) < parseFloat(b.total_trans || 0))
					return 1;
				return -1;
			});

		const mnosData = Data.groupByMNOs(this.props.transactions.data)
			.filter(o => o.total_trans)
			.sort((a, b) => {
				if (parseFloat(a.total_trans || 0) < parseFloat(b.total_trans || 0))
					return 1;
				return -1;
			});

		return (
			<>
				<h3 className="product-statistics-title">
					Total Transactions Made:{' '}
					<Comparision
						{...{
							current: Number.total(
								this.props.transactions.data.map(o =>
									parseFloat(o.total_trans || 0)
								)
							),
							previous: Number.total(
								this.props.transactions.previous.map(o =>
									parseFloat(o.total_trans || 0)
								)
							)
						}}
					/>
				</h3>

				<Row>
					<Col lg={4} md={6}>
						<Card>
							<CardHeader className="mb-1">
								<CardTitle>MNOs</CardTitle>
							</CardHeader>

							<CardBody className="pt-0">
								<div className="pie-chart-wrapper">
									<PieChart
										{...{
											labels: mnosData.map(o =>
												String.capitalize(o.mno.toLowerCase())
											),
											series: mnosData.map(o => parseFloat(o.total_trans || 0))
										}}
									/>
								</div>
							</CardBody>
						</Card>
					</Col>

					<Col lg={4} md={6}>
						<Card>
							<CardHeader className="mb-1">
								<CardTitle>Markets</CardTitle>
							</CardHeader>

							<CardBody className="pt-0">
								<div className="pie-chart-wrapper">
									<PieChart
										{...{
											labels: marketsData.map(o =>
												Country.getCountryByCode(o.country)
											),
											series: marketsData.map(o =>
												parseFloat(o.total_trans || 0)
											)
										}}
									/>
								</div>
							</CardBody>
						</Card>
					</Col>

					{/* <Col lg={4} md={6}>
						<Card>
							<CardHeader className="mb-1">
								<CardTitle>Aggregators</CardTitle>
							</CardHeader>

							<CardBody className="pt-0">
								<div className="pie-chart-wrapper">
									<PieChart
										{...{
											labels: ['centili', 'Apigate', 'Direct'],
											series: [80, 9, 100]
										}}
									/>
								</div>
							</CardBody>
						</Card>
					</Col> */}
				</Row>
			</>
		);
	}
}

export default connect(
	state => ({
		transactions: state.statistics.transactions
	}),
	{}
)(Transactions);
