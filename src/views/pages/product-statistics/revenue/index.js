import React from 'react';
import { connect } from 'react-redux';
import { Card, CardTitle, CardHeader, CardBody, Row, Col } from 'reactstrap';
import Comparision from '../../../../utility/modules/comparision';
import { PieChart } from '../../../../utility/modules/chart';
import Number from '../../../../utility/helpers/number';
import Data from '../../../../utility/helpers/data';
import Country from '../../../../utility/helpers/country';
import String from '../../../../utility/helpers/string';

class RevenueStatistics extends React.Component {
	render() {
		const marketsData = Data.groupByMarkets(this.props.revenue.data)
			.filter(o => o.Net_Revenue)
			.sort((a, b) => {
				if (parseFloat(a.Net_Revenue || 0) < parseFloat(b.Net_Revenue || 0))
					return 1;
				return -1;
			});

		const mnosData = Data.groupByMNOs(this.props.revenue.data)
			.filter(o => o.Net_Revenue)
			.sort((a, b) => {
				if (parseFloat(a.Net_Revenue || 0) < parseFloat(b.Net_Revenue || 0))
					return 1;
				return -1;
			});

		const aggregatorsData = Data.groupByAggregators(this.props.revenue.data)
			.filter(o => o.Net_Revenue)
			.sort((a, b) => {
				if (parseFloat(a.Net_Revenue || 0) < parseFloat(b.Net_Revenue || 0))
					return 1;
				return -1;
			});

		return (
			<div className="statistics-subscribers">
				<h3 className="product-statistics-title">
					Total Net Revenue:{' '}
					<Comparision
						{...{
							current: Number.total(
								this.props.revenue.data.map(o => parseFloat(o.Net_Revenue || 0))
							),
							previous: Number.total(
								this.props.revenue.previous.map(o =>
									parseFloat(o.Net_Revenue || 0)
								)
							)
						}}
					/>
				</h3>

				<Row>
					<Col lg={4} md={6}>
						<Card>
							<CardHeader className="mb-1">
								<CardTitle>MNOs</CardTitle>
							</CardHeader>

							<CardBody className="pt-0">
								<div className="pie-chart-wrapper">
									<PieChart
										{...{
											labels: mnosData.map(o =>
												String.capitalize(o.mno.toLowerCase())
											),
											series: mnosData.map(o => parseFloat(o.Net_Revenue || 0))
										}}
									/>
								</div>
							</CardBody>
						</Card>
					</Col>

					<Col lg={4} md={6}>
						<Card>
							<CardHeader className="mb-1">
								<CardTitle>Markets</CardTitle>
							</CardHeader>

							<CardBody className="pt-0">
								<div className="pie-chart-wrapper">
									<PieChart
										{...{
											labels: marketsData.map(o =>
												Country.getCountryByCode(o.country)
											),
											series: marketsData.map(o =>
												parseFloat(o.Net_Revenue || 0)
											)
										}}
									/>
								</div>
							</CardBody>
						</Card>
					</Col>

					<Col lg={4} md={6}>
						<Card>
							<CardHeader className="mb-1">
								<CardTitle>Aggregators</CardTitle>
							</CardHeader>

							<CardBody className="pt-0">
								<div className="pie-chart-wrapper">
									<PieChart
										{...{
											labels: aggregatorsData.map(o =>
												String.capitalize(o.aggregator.toLowerCase())
											),
											series: aggregatorsData.map(o =>
												parseFloat(o.Net_Revenue || 0)
											)
										}}
									/>
								</div>
							</CardBody>
						</Card>
					</Col>
				</Row>
			</div>
		);
	}
}

export default connect(
	state => ({
		revenue: state.statistics.revenue
	}),
	{}
)(RevenueStatistics);
