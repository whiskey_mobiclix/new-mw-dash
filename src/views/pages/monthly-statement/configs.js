export const dataTypes = [
	{
		name: 'New users',
		key: 'usersMonthlyStatement.data.new_users'
	},
	{
		name: 'Active users',
		key: 'usersMonthlyStatement.data.new_active_users'
	},
	{
		name: 'Unsubscribed users',
		eog: true,
		key: 'usersMonthlyStatement.data.churn_users'
	},
	{
		name: 'Total transactions',
		key: 'transactionsMonthlyStatement.data.total_trans'
	},
	{
		name: 'Gross Revenue ($)',
		key: 'revenueMonthlyStatement.data.Gross_Revenue'
	},
	{
		name: 'Net Revenue ($)',
		key: 'revenueMonthlyStatement.data.Net_Revenue',
	},
];
