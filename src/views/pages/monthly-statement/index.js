import React from 'react';
import { connect } from 'react-redux';
import { Card, CardBody, Row, Col } from 'reactstrap';
import COLOR from '../../../utility/helpers/color';
import Title from '../../../utility/modules/title';
import Selection from '../../../components/selection';
import MonthFilter from "../../../utility/modules/filter/month"
import Number from '../../../utility/helpers/number';
import Comparision from '../../../utility/modules/comparision';
import { Octagon } from 'react-feather';
import { getMonthlyStatementData } from '../../../redux/actions/statistics';
import { dataTypes } from './configs';
import Spinner from '../../../components/@vuexy/spinner/Loading-spinner';
import Download from "./download"
import Date from "../../../utility/helpers/date"

//CSS
import './style.scss';

class MonthlyStatement extends React.Component {
	componentDidMount() {
		if(!this.props.statistics.usersMonthlyStatement.data.length){
			this.props.getMonthlyStatementData({
				product: this.props.filter.product.value,
				dates: Date.rangeByMonths(this.props.filter.monthrange.value),
			})
		}
	}

	getValueByKey = (key, date) => {
		try {
			const splited = key.split('.');
			let res = this.props.statistics;
			let val = 0;

			splited.forEach(o => {
				res = res[o];
				if (o === 'data') {
					val = (res.filter(r => r.time == date)[0] || {})[splited[splited.length - 1]] || 0
				}
			});

			return Math.round(val * 100) / 100;
		} catch (e) {
			return 0;
		}
	};

	getTotalByKey = key => {
		try {
			const splited = key.split('.');
			let _data = this.props.statistics;
			let current = [];
			let previous = [];

			splited.forEach(o => {
				if (o === 'data') {
					current = _data.data;
					previous = _data.previous;
				}

				_data = _data[o];
			});

			return {
				current: Number.total(
					current.map(oo => oo[splited[splited.length - 1]])
				),
				previous: Number.total(
					previous.map(oo => oo[splited[splited.length - 1]])
				)
			};
		} catch (e) {
			return {
				current: 0,
				previous: 0
			};
		}
	};

	render() {
		if(this.props.app.locked) return <Spinner />

		const dates = Date.months(this.props.filter.monthrange.value)

		return (
			<div className="monthly-statement mw-tb">
				<Title text="Monthly Statement" />
				<div>
					<MonthFilter />
				</div>

				<div className="mw-tb-title">
					{/*<h3>Ip owners Receivable: <span style={{
						color: COLOR.$primary
					}}>$ {Number.format(Number.total(Array.findByAttribute(IP_OWNERS_RECEIVABLE, "name", "Ip owner Receivable").data.map(o => o.value)))}</span></h3>*/}
					<p>
						Monthly statement is a written record prepared by a financial
						institution, usually once a month, listing all credit card
						transactions for an account, including purchases, payments, fees and
						finance charges.
					</p>
				</div>

				<Card>
					<CardBody className="pl-3 pr-3">
						<Row>
							<Col lg={3} md={4} sm={5} />
							<Col lg={9} md={8} sm={7}>
								<div
									className="mw-tb-row bold"
									style={{
										padding: '1rem 0'
									}}
								>
									{dates.map((o, ix) => (
										<span
											style={{
												width: '100%',
												display: 'block',
												textAlign: 'right'
											}}
											key={ix}
										>
											{o}
										</span>
									))}
								</div>
							</Col>
						</Row>

						<div className="mw-dvdr line" />

						{dataTypes.map((o, ix) => (
							<React.Fragment key={ix}>
								<Row key={ix}>
									<Col lg={3} md={4} sm={5}>
										<div className="mw-tb-label">
											<span
												style={{
													marginRight: '8px',
													transform: 'translateY(-1px)',
													display: 'inline-block'
												}}
											>
												<Octagon size={15} />
											</span>
											{o.name}
											{!!o.description && (
												<>
													<br />
													<span className="mw-tb-note">({o.description})</span>
												</>
											)}
											<div
												style={{
													fontWeight: '400'
												}}
											>
												(
												<span
													style={{
														fontSize: '14px'
													}}
												>
													Total:{' '}
												</span>
												<Comparision {...this.getTotalByKey(o.key)} />)
											</div>
										</div>
									</Col>
									<Col lg={9} md={8} sm={7}>
										<div className="mw-tb-row">
											{dates.map((oo, iix) => (
												<span
													key={iix}
													style={{
														display: 'block',
														width: '100%',
														textAlign: 'right',
														fontStyle: 'italic'
													}}
												>
													{Number.format(this.getValueByKey(o.key, oo))}
												</span>
											))}
										</div>
									</Col>
								</Row>

								<div className={`mw-dvdr ${o.eog ? 'line' : ''}`} />
							</React.Fragment>
						))}
					</CardBody>
				</Card>

				{/*<div className="mw-tb-title">
					<h3>Ip owners Royalties: <span style={{
						color: COLOR.$primary
					}}>$ {Number.format(Number.total(Array.findByAttribute(IP_OWNERS_ROYALTIES, "name", "Royalties").data.map(o => o.value)))}</span></h3>
				</div>

				<Card>
					<CardBody>
						{
							IP_OWNERS_ROYALTIES.map((o, ix) =>
								<Row key={ix}>
									<Col lg={3} md={4} sm={5}>
										<div className="mw-tb-label">
											{o.name}
										</div>
									</Col>
									<Col lg={9} md={8} sm={7}>
										<div className="mw-tb-row">
											{
												o.data.map((k, i) =>
													<span key={i} style={{
															display: "block",
															width: "100%",
															textAlign: "right",
													}}>{Number.format(k.value)}</span>
												)
											}
										</div>
									</Col>
								</Row>
							)
						}
					</CardBody>
				</Card>*/}

				<Download />
			</div>
		);
	}
}

export default connect(
	state => ({
		app: state.app,
		filter: state.filter,
		statistics: state.statistics
	}),
	{
		getMonthlyStatementData,
	}
)(MonthlyStatement);
