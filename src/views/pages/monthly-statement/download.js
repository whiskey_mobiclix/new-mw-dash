import React from "react"
import { connect } from "react-redux"
import Download from "../../../utility/modules/download"
import Date from "../../../utility/helpers/date"


class MonthlyStateMonthDownload extends React.Component{
	data = () => {
		const dates = Date.months(this.props.filter.monthrange.value)
		let res = []
		res.push([""].concat(dates))
		res.push(["New users"].concat(dates.map(o => (this.props.statistics.usersMonthlyStatement.data.filter(d => d.time === o)[0] || {}).new_users || 0)))
		res.push(["Active users"].concat(dates.map(o => (this.props.statistics.usersMonthlyStatement.data.filter(d => d.time === o)[0] || {}).new_active_users || 0)))
		res.push(["Unsubscribed users"].concat(dates.map(o => (this.props.statistics.usersMonthlyStatement.data.filter(d => d.time === o)[0] || {}).churn_users || 0)))
		res.push(["Gross revenue"].concat(dates.map(o => (this.props.statistics.revenueMonthlyStatement.data.filter(d => d.time === o)[0] || {}).Gross_Revenue || 0)))
		res.push(["Net revenue"].concat(dates.map(o => (this.props.statistics.revenueMonthlyStatement.data.filter(d => d.time === o)[0] || {}).Net_Revenue || 0)))
		res.push(["Total transactions"].concat(dates.map(o => (this.props.statistics.transactionsMonthlyStatement.data.filter(d => d.time === o)[0] || {}).total_trans || 0)))

		return res
	}

	render(){
		const dates = Date.months(this.props.filter.monthrange.value)
		console.log(dates.map(o => (this.props.statistics.usersMonthlyStatement.data.filter(d => d.time === o)[0] || {}).new_users || 0))

		return (
			<div className="monthly-statement-download" style={{
				textAlign: "right",
			}}>
				<Download {...{
					data: this.data(),
				}}/>
			</div>
		)
	}
}

export default connect(
	state => ({
		statistics: state.statistics,
		filter: state.filter,
	}),
	{}
)(MonthlyStateMonthDownload)