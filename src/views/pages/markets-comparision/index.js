import React from "react"
import { connect } from "react-redux"
import Title from "../../../utility/modules/title"
import Selection from "../../../components/selection"
import { Card, CardTitle, CardHeader, CardBody, Row, Col } from "reactstrap"
import Chart from "./chart"
import Spinner from '../../../components/@vuexy/spinner/Loading-spinner';
import Data from "../../../utility/helpers/data"
import Date from "../../../utility/helpers/date"
import Country from "../../../utility/helpers/country"
import MonthFilter from "../../../utility/modules/filter/month"
import { getMonthlyStatementData } from "../../../redux/actions/statistics"

//CSS
import "./style.scss"

class MarketsComparision extends React.Component{
	state = {
		type: "usersMonthlyStatement.new_users",
	}

	componentDidMount(){
		if(!this.props.statistics.usersMonthlyStatement.data.length){
			this.props.getMonthlyStatementData({
				product: this.props.filter.product.value,
				dates: Date.rangeByMonths(this.props.filter.monthrange.value),
			})
		}
	}

	statsData = () => {
		const splited = this.state.type.split(".")
		const data = Data.groupByDateAndMarkets(this.props.statistics[splited[0]].rawData).sort((a, b) => {
			if(Country.getCountryByCode(a.country) && !Country.getCountryByCode(b.country)) return -1
			return 1
		})
		
		return data
	}

	statsValue = (data, dt) => {
		const splited = this.state.type.split(".")
		return Math.round(((data.filter(o => o.time == dt)[0] || {})[splited[1]] || 0) * 100) / 100
	}

	render(){
		if(this.props.app.locked) return <Spinner />

		const statsData = this.statsData()
		const dts = Date.months(this.props.filter.monthrange.value)

		return (
			<div className="markets-comparision">
				<Title text={"Markets comparision"}/>
				<MonthFilter />

				<div style={{
					height: "1px",
					marginBottom: "2rem",
				}}/>

				<Card className="mw-tb pl-2 pr-2 pb-2">
					<CardHeader>
						<CardTitle>
							<Selection {...{
								value: this.state.type,
								options: [
									{
										name: "Total Subscribers",
										value: "usersMonthlyStatement.new_users",
									},
									{
										name: "Total Unsubscribers",
										value: "usersMonthlyStatement.churn_users",
									},
									{
										name: "Total Transactions",
										value: "transactionsMonthlyStatement.total_trans",
									},
									{
										name: "Total Net Revenue",
										value: "revenueMonthlyStatement.Net_Revenue",
									},
									{
										name: "Total Gross Revenue",
										value: "revenueMonthlyStatement.Gross_Revenue",
									},
								],
								onChange: e => {
									this.setState({
										type: e.value,
									})
								}
							}} />
							in the {this.props.filter.monthrange.options.filter(o => o.value === this.props.filter.monthrange.value)[0].name}
						</CardTitle>
					</CardHeader>

					<CardBody className="pt-3">
						<Row>
							<Col lg={2} md={3} sm={4}></Col>
							<Col lg={10} md={9} sm={8}>
								<div className="mw-tb-row bold">
									{
										dts.map((o, ix) =>
											<span key={ix} style={{
												display: "block",
												width: "100%",
												textAlign: "right",
												fontSize: "16px",
											}}>{o}</span>
										)
									}
								</div>
							</Col>
						</Row>

						<div className="mw-dvdr line"/>

						{
							statsData.map((o, ix) =>
								<Row key={ix}>
									<Col lg={2} md={3} sm={4}>
										<span className="mw-tb-label">
											{
												Country.getCountryByCode(o.country)
												&&
												<img src={`${process.env.REACT_APP_RESOURCE_DOMAIN}/images/flags/${o.country.toLowerCase()}.svg`} />
											}
											{
												Country.getCountryByCode(o.country) || "(Others)"
											}
										</span>
									</Col>
									<Col lg={10} md={9} sm={8}>
										<div className="mw-tb-row">
											{
												dts.map((d, ixx) =>
													<span key={ixx} style={{
														display: "block",
														width: "100%",
														textAlign: "right",
														fontSize: "16px",
													}}>
														{this.statsValue(o.data, d)}
													</span>
												)
											}
										</div>
									</Col>
								</Row>
							)
						}
					</CardBody>
				</Card>

				<Card>
					<CardBody className="pt-2">
						<Row>
							<Col lg={12}>
								<Chart {...{
									dates: dts,
									data: statsData,
									type: this.state.type.split(".")[1]
								}}/>
							</Col>
						</Row>
					</CardBody>
				</Card>
			</div>
		)
	}
}

export default connect(
	state => ({
		statistics: state.statistics,
		filter: state.filter,
		app: state.app,
	}),
	{
		getMonthlyStatementData,
	}
)(MarketsComparision)