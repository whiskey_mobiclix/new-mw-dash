import React from 'react';
import { ColumnChart } from '../../../utility/modules/chart';
import COLORS from '../../../utility/helpers/color';
import Country from '../../../utility/helpers/country';
import moment from 'moment';

class ApexColumnCharts extends React.Component {
	fill = (dates, data) => {
		return dates.map(
			o =>
				parseFloat(
					(data.filter(d => d.time === o)[0] || {})[this.props.type]
				) || 0
		);
	};

	render() {
		return (
			<ColumnChart
				options={{
					chart: {
						stacked: false
					},
					stroke: {
						show: true,
						width: 3,
						colors: COLORS.base()
					},
					plotOptions: {
						bar: {
							columnWidth: '70%',
							horizontal: false
						}
					},
					colors: COLORS.base(),
					xaxis: {
						labels: {
							rotateAlways: false,
							formatter: function(value, timestamp) {
								return new moment(value).format('MMM YYYY'); // The formatter function overrides format property
							}
						}
					}
				}}
				categories={this.props.dates}
				series={this.props.data.map(o => ({
					name: Country.getCountryByCode(o.country) || "Others",
					data: this.fill(this.props.dates, o.data)
				}))}
			/>
		);
	}
}
export default ApexColumnCharts;
