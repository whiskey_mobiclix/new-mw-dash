import React from 'react';
import { connect } from 'react-redux';

import Title from '../../../utility/modules/title';
import General from './general';
import Filter from '../../../utility/modules/filter';
import { getStatisticsData } from '../../../redux/actions/statistics';
import Spinner from '../../../components/@vuexy/spinner/Loading-spinner';

import Styled from './styled';

class Finance extends React.Component {
	componentDidMount() {
		if (!this.props.users.data.length) {
			this.props.getStatisticsData(this.props.filter.daterange.value);
		}
	}

	render() {
		if (this.props.app.locked) return <Spinner />;
		return (
			<Styled className="finance">
				<Title text="Dashboard Overview" />
				<Filter />
				<General />
			</Styled>
		);
	}
}

export default connect(
	state => ({
		users: state.statistics.users,
		filter: state.filter,
		app: state.app
	}),
	{
		getStatisticsData
	}
)(Finance);
