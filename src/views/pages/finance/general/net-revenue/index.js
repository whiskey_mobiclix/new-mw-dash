import React from 'react';
import { Card, CardHeader, CardTitle, CardBody } from 'reactstrap';
import moment from 'moment';
import { connect } from 'react-redux';

import { ColumnChart } from '../../../../../utility/modules/chart';
import { Comparision } from '../../../../../utility/modules';

class NetRevenue extends React.Component {
	render() {
		return (
			<Card>
				<ColumnChart
					categories={this.props.revenue.data.map(o =>
						moment(o.time).valueOf()
					)}
					series={[
						{
							name: 'Net Revenue',
							data: this.props.revenue.data.map(o => o.Net_Revenue)
						}
					]}
				/>
			</Card>
		);
	}
}
export default connect(state => ({
	revenue: state.statistics.revenue
}))(NetRevenue);
