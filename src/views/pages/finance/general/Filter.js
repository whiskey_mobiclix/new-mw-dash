import React from 'react';
import { connect } from 'react-redux';
import Selection from '../../../../components/selection';
import { Comparision } from '../../../../utility/modules';
import Number from '../../../../utility/helpers/number';

//CSS
import './style.scss';

class Filter extends React.Component {
	state = {
		active: 'grossRevenue',
		values: {
			grossRevenue: 0,
			netRevenue: 0,
			totalExpenses: 0
		}
	};

	handleClick = id => {
		let _state = this.state;
		_state.active = id;
		this.setState(_state);
		this.props.onChange &&
			this.props.onChange({
				active: id,
				value: _state.values[id]
			});
	};

	handleFilterChange = (id, value) => {
		let _state = this.state;
		_state.values[id] = value;

		this.setState(_state);
		this.props.onChange &&
			this.props.onChange({
				active: id,
				value: _state.values[id]
			});
	};

	render() {
		return (
			<div className="finance-general-filter">
				<div className="item">
					<div
						className={`selection-wrapper ${
							this.state.active === 'grossRevenue' ? 'active' : ''
						}`}
					>
						{this.state.active !== 'grossRevenue' && (
							<div
								className="item-overlay"
								onClick={() => {
									this.handleClick('grossRevenue');
								}}
							/>
						)}

						<Selection
							{...{
								value: this.state.values.grossRevenue,
								options: [
									{
										name: 'Gross revenue',
										value: 0
									}
								],
								onChange: selected => {
									this.handleFilterChange('grossRevenue', selected.value);
								}
							}}
						/>
					</div>

					<Comparision
						current={Number.total(
							this.props.revenue.data.map(o => o.Gross_Revenue)
						)}
						previous={Number.total(
							this.props.revenue.previous.map(o => o.Gross_Revenue)
						)}
					/>
				</div>

				<div className="item">
					<div
						className={`selection-wrapper ${
							this.state.active === 'netRevenue' ? 'active' : ''
						}`}
					>
						{this.state.active !== 'netRevenue' && (
							<div
								className="item-overlay"
								onClick={() => {
									this.handleClick('netRevenue');
								}}
							/>
						)}

						<Selection
							{...{
								value: this.state.values.netRevenue,
								options: [
									{
										name: 'Net revenue',
										value: 0
									}
								]
							}}
						/>
					</div>

					<Comparision
						current={Number.total(
							this.props.revenue.data.map(o => o.Net_Revenue)
						)}
						previous={Number.total(
							this.props.revenue.previous.map(o => o.Net_Revenue)
						)}
					/>
				</div>

				{/* <div className="item">
					<div
						className={`selection-wrapper ${
							this.state.active === 'totalExpenses' ? 'active' : ''
						}`}
					>
						{this.state.active !== 'totalExpenses' && (
							<div
								className="item-overlay"
								onClick={() => {
									this.handleClick('totalExpenses');
								}}
							/>
						)}
						<Selection
							{...{
								value: this.state.values.totalExpenses,
								options: [
									{
										name: 'Total expenses',
										value: 0
									}
								]
							}}
						/>
					</div>

					<Comparision />
				</div> */}
			</div>
		);
	}
}

export default connect(
	state => ({
		user: state.user,
		revenue: state.statistics.revenue
	}),
	{}
)(Filter);
