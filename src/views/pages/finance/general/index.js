import React from 'react';
import { Card, CardBody } from 'reactstrap';

import Filter from './Filter';
import GrossRevenue from './gross-revenue';
import NetRevenue from './net-revenue';

class General extends React.Component {
	state = {
		filter: {
			active: 'grossRevenue'
		}
	};

	render() {
		return (
			<div className="finance-dashboard-general">
				<Card>
					<CardBody>
						<Filter
							{...{
								onChange: e => {
									this.setState({
										filter: e
									});
								}
							}}
						/>

						{this.state.filter.active === 'grossRevenue' && <GrossRevenue />}

						{this.state.filter.active === 'netRevenue' && <NetRevenue />}
					</CardBody>
				</Card>
			</div>
		);
	}
}
export default General;
