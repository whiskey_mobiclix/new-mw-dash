import React from 'react';

import Chart from 'react-apexcharts';

import { Col } from 'reactstrap';

import Styled from './styled';

let $primary = '#7367F0',
	$success = '#28C76F',
	$danger = '#EA5455',
	$warning = '#FF9F43',
	$info = '#00cfe8',
	$primary_light = '#9c8cfc',
	$warning_light = '#FFC085',
	$danger_light = '#f29292',
	$info_light = '#1edec5',
	$stroke_color = '#b9c3cd',
	$label_color = '#e7eef7',
	$purple = '#df87f2',
	$white = '#fff';

class PayableRevenue extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			options: {
				chart: {
					stacked: true,
					toolbar: { show: false }
				},
				plotOptions: {
					bar: {
						columnWidth: '50%',
						horizontal: true
					}
				},
				colors: [$warning, $primary],
				dataLabels: {
					enabled: false
				},
				grid: {
					borderColor: $label_color,
					padding: {
						left: 0,
						right: 0,
						bottom: 20
					}
				},
				legend: {
					show: true,
					position: 'bottom',
					horizontalAlign: 'left',
					offsetX: 50,
					fontSize: '14px',
					markers: {
						radius: 2,
						width: 12,
						height: 12
					}
				},
				xaxis: {
					labels: {
						style: {
							colors: $stroke_color
						}
					},
					axisTicks: {
						show: false
					},
					categories: ['Orange', '(direc) T-Mobile', 'Vodafone'],
					axisBorder: {
						show: false
					}
				},
				yaxis: {
					tickAmount: 5,
					labels: {
						style: {
							color: $stroke_color
						}
					}
					// title: {
					// 	text: 'Payable Revenue (USD)',
					// 	rotate: -90,
					// 	offsetX: -100,
					// 	offsetY: -200
					// }
				},
				tooltip: {
					x: { show: false }
				},
				title: {
					text: 'Payable Revene (USD',
					align: 'left',
					floating: true
				}
			},
			series: [
				{
					name: 'Advance Payments',
					data: [180000, 185000, 210000, 200000]
				},
				{
					name: 'Remaining Payments',
					data: [180000, 185000, 210000, 200000]
				}
			]
		};
	}

	render() {
		return (
			<Styled>
				<Col xs={12} sm={12} md={8}>
					<Chart
						options={this.state.options}
						series={this.state.series}
						type="bar"
						height={350}
					/>
				</Col>
			</Styled>
		);
	}
}

export default PayableRevenue;
