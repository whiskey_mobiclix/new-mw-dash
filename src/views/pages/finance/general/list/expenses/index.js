import React from 'react';
import { Card, CardBody, CardHeader, CardTitle } from 'reactstrap';
import { TabContent, TabPane, Nav, NavItem, NavLink } from 'reactstrap';
import classnames from 'classnames';

import ListView from './listView';

import Styled from './styled';

class Expenses extends React.Component {
	state = {
		active: '1'
	};

	toggle = tab => {
		if (this.state.active !== tab) {
			this.setState({ active: tab });
		}
	};

	render() {
		return (
			<Styled>
				<Card>
					<CardBody>
						<Nav tabs>
							<NavItem>
								<NavLink
									className={classnames({
										active: this.state.active === '1'
									})}
									onClick={() => {
										this.toggle('1');
									}}
								>
									Fixed Coats
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink
									className={classnames({
										active: this.state.active === '2'
									})}
									onClick={() => {
										this.toggle('2');
									}}
								>
									Payrolls
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink
									className={classnames({
										active: this.state.active === '3'
									})}
									onClick={() => {
										this.toggle('3');
									}}
								>
									Others
								</NavLink>
							</NavItem>
						</Nav>
						<TabContent activeTab={this.state.active}>
							<TabPane tabId="1">
								<ListView />
							</TabPane>
							<TabPane tabId="2">
								<ListView />
							</TabPane>
							<TabPane tabId="3">
								<ListView />
							</TabPane>
						</TabContent>
					</CardBody>
				</Card>
			</Styled>
		);
	}
}

export default Expenses;
