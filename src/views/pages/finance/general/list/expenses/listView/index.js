import React from 'react';
import * as Icon from 'react-feather';
import { Input, Button, Badge } from 'reactstrap';
import DataTable from 'react-data-table-component';

import Styled from './styled';

const CustomHeader = props => {
	return (
		<div className="d-flex flex-wrap justify-content-between">
			<div className="add-new">
				<Button.Ripple color="primary">Add New</Button.Ripple>
			</div>
			<div className="position-relative has-icon-left mb-1">
				<Input value={props.value} onChange={e => props.handleFilter(e)} />
				<div className="form-control-position">
					<Icon.Search size="15" />
				</div>
			</div>
		</div>
	);
};

class ListView extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			columns: [
				{
					name: 'Type',
					selector: 'type',
					sortable: true,
					maxWidth: '130px',
					center: true,
					cell: row => <span>{row.type}</span>
				},
				{
					name: 'Office',
					selector: 'office',
					sortable: true,
					center: true,
					maxWidth: '150px',
					cell: row => <span>{row.office}</span>
				},
				{
					name: 'Details',
					selector: 'details',
					sortable: true,
					width: '150px',
					center: true,
					cell: row => <span>{row.details}</span>
				},
				{
					name: 'Payment Cycle',
					selector: 'paymentCycle',
					sortable: true,
					center: true,
					width: '150px',
					cell: row => <span>{row.paymentCycle}</span>
				},
				{
					name: 'Amount',
					selector: 'amount',
					sortable: true,
					center: true,
					width: '140px',
					cell: row => <span>$ {row.amount}</span>
				},
				{
					name: 'Due Date',
					selector: 'dueDate',
					sortable: true,
					center: true,
					cell: row => <span>{row.dueDate}</span>
				},
				{
					name: 'Status',
					selector: 'status',
					sortable: true,
					center: true,
					cell: () => <span>test</span>
				}
			],
			data: [
				{
					type: 'Office Rental',
					office: 'Vietnam',
					details: '',
					amount: 30000,
					paymentCycle: '6-months',
					dueDate: 'Aug 2019'
				},
				{
					type: 'Office Rental',
					office: 'Singapore',
					details: '',
					amount: 30000,
					paymentCycle: 'Annual',
					dueDate: 'Aug 2019'
				},
				{
					type: 'Billings',
					office: 'Vietnam',
					details: '',
					amount: 30000,
					paymentCycle: 'Monthly',
					dueDate: 'Aug 2019'
				}
			],
			value: ''
		};
	}

	render() {
		const { data, columns } = this.state;

		return (
			<Styled>
				<DataTable
					className="dataTable-custom"
					data={data}
					columns={columns}
					noHeader
					highlightOnHover
					// pagination
					// subHeader
					// subHeaderComponent={
					// 	<CustomHeader value={value} handleFilter={this.handleFilter} />
					// }
				/>
			</Styled>
		);
	}
}

export default ListView;
