import styled from 'styled-components';
import { Row } from 'reactstrap';
import { MWDashboardColorRGB } from '../../../../../../../utility/constants/color';

const Styled = styled(Row)`
	.sort-by {
		display: flex;
		align-items: center;
		justify-content: flex-end;
		height: fit-content;

		.label {
			margin-right: 10px;
		}

		.sort-dropdown {
			border: 1px solid rgba(${MWDashboardColorRGB.product9}, 0.3);
			background-color: white;
			border-radius: 5rem;
		}
	}

	.header {
		font-weight: 500;
		text-transform: uppercase;
	}
`;

export default Styled;
