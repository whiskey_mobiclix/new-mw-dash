import React from 'react';
import * as Icon from 'react-feather';
import { Input, Button, Badge } from 'reactstrap';
import DataTable from 'react-data-table-component';

import Styled from './styled';

const CustomHeader = props => {
	return (
		<div className="d-flex flex-wrap justify-content-between">
			<div className="add-new">
				<Button.Ripple color="primary">Add New</Button.Ripple>
			</div>
			<div className="position-relative has-icon-left mb-1">
				<Input value={props.value} onChange={e => props.handleFilter(e)} />
				<div className="form-control-position">
					<Icon.Search size="15" />
				</div>
			</div>
		</div>
	);
};

class ListView extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			columns: [
				{
					name: 'Product',
					selector: 'product',
					sortable: true,
					maxWidth: '130px',
					center: true,
					cell: row => <span title={row.produc}>{row.product}</span>
				},
				{
					name: 'Partner Name',
					selector: 'partner',
					sortable: true,
					center: true,
					maxWidth: '150px',
					cell: row => <span>{row.partner}</span>
				},
				{
					name: 'Minimun Guarantee Amount',
					selector: 'status',
					sortable: true,
					width: '230px',
					center: true,
					cell: row => <span>$ {row.guaranteeAmount}</span>
				},
				{
					name: 'Time-frame',
					selector: 'timeFrame',
					sortable: true,
					center: true,
					width: '130px',
					cell: row => <span>{row.timeFrame}</span>
				},
				{
					name: 'Loss Carried Forward = 0 by',
					selector: 'date',
					sortable: true,
					center: true,
					width: '240px',
					cell: row => <span>{row.date}</span>
				},
				{
					name: 'Status',
					selector: 'status',
					sortable: true,
					center: true,
					cell: () => <span>test</span>
				}
			],
			data: [
				{
					product: 'Hopster',
					partner: 'Jamiee',
					guaranteeAmount: 30000,
					timeFrame: '6-months',
					date: 'Aug 2019'
				},
				{
					product: 'Acronis',
					partner: 'Jamiee',
					guaranteeAmount: 20000,
					timeFrame: 'Annual',
					date: 'Oct 2019'
				},
				{
					product: 'M-Secure',
					partner: 'Jamiee',
					guaranteeAmount: 20000,
					timeFrame: '6-months',
					date: 'Feb 2020'
				}
			],
			value: ''
		};
	}

	render() {
		const { data, columns } = this.state;

		return (
			<Styled>
				<DataTable
					className="dataTable-custom"
					data={data}
					columns={columns}
					noHeader
					highlightOnHover
					// pagination
					// subHeader
					// subHeaderComponent={
					// 	<CustomHeader value={value} handleFilter={this.handleFilter} />
					// }
				/>
			</Styled>
		);
	}
}

export default ListView;
