import styled from 'styled-components';

const Styled = styled.div`
	.card-title {
		display: flex;
		align-items: center;

		.mw-comparision {
			margin-left: 20px;

			span:first-child {
				color: rgb(115, 103, 240);
			}
		}
	}
`;
export default Styled;
