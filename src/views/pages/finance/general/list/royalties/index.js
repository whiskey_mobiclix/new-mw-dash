import React from 'react';
import { Card, CardBody, CardHeader, CardTitle } from 'reactstrap';

import Comparision from '../../../../../../utility/modules/comparision';
import ListView from './listView';

import Styled from './styled';

class Royalties extends React.Component {
	render() {
		return (
			<Styled>
				<Card>
					<CardHeader>
						<div className="card-heading">
							<CardTitle>
								Royalties for Product Partner <Comparision />
							</CardTitle>
						</div>
					</CardHeader>

					<CardBody>
						<ListView />
					</CardBody>
				</Card>
			</Styled>
		);
	}
}

export default Royalties;
