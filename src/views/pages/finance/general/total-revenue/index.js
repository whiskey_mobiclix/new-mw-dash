import React from "react"
import { Card } from "reactstrap"
import { ColumnChart } from "../../../../../utility/modules/chart";
// import { Comparision } from "../../../../../utility/modules";

class TotalRevenue extends React.Component {
  render() {
    return (
      <Card>
        <ColumnChart {...{
            categories: [
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
            ],
            series: [
              {
                name: "",
                data: [140, 180, 305, 240, 295, 150, 205, 160, 295, 240, 295, 150, 205, 160, 295, 140, 180, 305, 240, 295, 150, 205, 160, 295, 240, 295, 150, 205, 160, 295]
              },
            ]
          }}/>
      </Card>
    )
  }
}
export default TotalRevenue