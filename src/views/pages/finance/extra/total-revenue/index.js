import React from "react"
import { Card, CardBody, Row, Col } from "reactstrap"
import Comparision from "../../../../../utility/modules/comparision"

//CSS
import "./style.scss"

class TotalRevenueExtra extends React.Component{
	render(){
		return (
			<div className="total-revenue-extra">
				<h3>
					Royalties for Product Partner:
					<Comparision />
				</h3>

				<Card>
					<CardBody className="mw-tb">
						<Row>
							<Col>
								<div className="mw-tb-row bold">
									<span style={{
										display: "block",
										width: "50%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Product</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Partner name</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Minimum<br />Guarantee Amount</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Time Frame</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Lost<br />Carried Forward = 0</span>

									<span style={{
										display: "block",
										width: "50%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Status</span>
								</div>
							</Col>
						</Row>

						<div className="mw-dvdr line"/>

						<Row>
							<Col>
								<div className="mw-tb-row">
									<span
										className="bold"
										style={{
											display: "block",
											width: "50%",
											fontSize: "16px",
											paddingRight: "20px",
										}}>Acronis</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Joniee</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>$ 30,000</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>6 months</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Aug 2019</span>

									<span style={{
										display: "block",
										width: "50%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Pending</span>
								</div>
							</Col>
						</Row>

						<Row className="odd">
							<Col>
								<div className="mw-tb-row">
									<span
										className="bold"
										style={{
											display: "block",
											width: "50%",
											fontSize: "16px",
											paddingRight: "20px",
										}}>Babystep</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Joniee</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>$ 30,000</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>6 months</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Aug 2019</span>

									<span style={{
										display: "block",
										width: "50%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Pending</span>
								</div>
							</Col>
						</Row>

						<Row>
							<Col>
								<div className="mw-tb-row">
									<span
										className="bold"
										style={{
											display: "block",
											width: "50%",
											fontSize: "16px",
											paddingRight: "20px",
										}}>Hopster</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Joniee</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>$ 30,000</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>6 months</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Aug 2019</span>

									<span style={{
										display: "block",
										width: "50%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Pending</span>
								</div>
							</Col>
						</Row>
					</CardBody>
				</Card>
			</div>
		)
	}
}

export default TotalRevenueExtra