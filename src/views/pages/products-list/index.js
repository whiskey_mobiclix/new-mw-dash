import React from "react"
import Title from "../../../utility/modules/title"
import { Card, CardBody, Row, Col } from "reactstrap"
import Item from "./item"
import PRODUCTS from "../../../utility/mockup/products"
import Selection from "../../../components/selection"

class ProductsList extends React.Component{
	render(){
		return (
			<div className="products-list">
				<Title text={"All products"}/>

				{/*<div style={{
					padding: "0 0 2rem",
				}}>
					<Selection {...{
						value: "",
						options: [
							{
								name: "All verticals",
								value: "",
							},
							{
								name: "Game",
								value: "game",
							},
							{
								name: "Sport",
								value: "sport",
							},
							{
								name: "SVODs",
								value: "svods",
							},
						]
					}}/>
					<span className="mr-1"/>
					<Selection {...{
						value: "",
						options: [
							{
								name: "All payment methods",
								value: "",
							},
							{
								name: "Credit card",
								value: "credit",
							},
							{
								name: "Mobile payment",
								value: "mobile",
							},
						]
					}}/>
				</div>*/}

				<Card className="mw-tb" style={{
					marginTop: "30px",
				}}>
					<CardBody>
						<Row>
							<Col>
								<div className="mw-tb-row bold">
									<span style={{
										display: "block",
										width: "20%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>#</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Name</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Payment Methods</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Status</span>

									<span style={{
										display: "block",
										width: "100%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Last activity</span>

									<span style={{
										display: "block",
										width: "50%",
										fontSize: "16px",
										paddingRight: "20px",
									}}>Action</span>
								</div>
							</Col>
						</Row>

						<div className="mw-dvdr line"/>
						

						{
							PRODUCTS.map((o, ix) => 
								<React.Fragment key={ix}>
									<Item {...{
										...o,
										number: ix + 1,
									}}/>
								</React.Fragment>
							)
						}
					</CardBody>
				</Card>
			</div>
		)
	}
}

export default ProductsList