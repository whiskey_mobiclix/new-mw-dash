import React from "react"
import { connect } from "react-redux"
import { Row, Col, Button } from "reactstrap"
import { history } from "../../../../history"
import { setProduct } from "../../../../redux/actions/filter"
import { setUsersStatisticsData } from '../../../../redux/actions/statistics/users';
import { setTransactionsStatisticsData } from '../../../../redux/actions/statistics/transactions';
import { setRevenueStatisticsData } from '../../../../redux/actions/statistics/revenue';
import { setMarketingStatisticsData } from '../../../../redux/actions/statistics/marketing';
import Data from "../../../../utility/helpers/data"

//CSS
import "./style.scss"

class ProductItem extends React.Component{
	render(){
		return (
			<div className="product-item">
				<Row className={this.props.number % 2 === 0 ? "odd" : ""}>
					<Col>
						<div className="mw-tb-row">
							<span style={{
								paddingRight: "25px",
								width: "20%",
								fontSize: "15px",
							}}>{this.props.number}</span>

							<span style={{
								paddingRight: "25px",
								width: "100%",
								fontSize: "15px",
							}}>
								<span>
									<i>{this.props.vertical}</i>
									<br />
									<a href={this.props.address} target="_blank" rel="noopener noreferrer">{this.props.name}</a>
								</span>
							</span>

							<span style={{
								paddingRight: "25px",
								width: "100%",
								fontSize: "15px",
							}}>{this.props.methods}</span>

							<span style={{
								paddingRight: "25px",
								width: "100%",
								fontSize: "15px",
							}}>{this.props.status}</span>

							<span style={{
								paddingRight: "25px",
								width: "100%",
								fontSize: "15px",
							}}>{this.props.activity}</span>

							<span style={{
								paddingRight: "25px",
								width: "50%",
								fontSize: "15px",
							}}>
								<Button.Ripple className="mr-1 mb-1 bg-gradient-primary" color="none" onClick={() => {
									this.props.setProduct(this.props.code)
										
									this.props.setUsersStatisticsData({
										data: Data.filter(
											this.props.statistics.users.rawData,
											"",
											"",
											this.props.code,
										),
										previous: Data.filter(
											this.props.statistics.users.rawPrevious,
											"",
											"",
											this.props.code,
										)
									});
									this.props.setTransactionsStatisticsData({
										data: Data.filter(
											this.props.statistics.transactions.rawData,
											"",
											"",
											this.props.code,
										),
										previous: Data.filter(
											this.props.statistics.transactions.rawPrevious,
											"",
											"",
											this.props.code,
										)
									});
									this.props.setRevenueStatisticsData({
										data: Data.filter(
											this.props.statistics.revenue.rawData,
											"",
											"",
											this.props.code,
										),
										previous: Data.filter(
											this.props.statistics.revenue.rawPrevious,
											"",
											"",
											this.props.code,
										)
									});
									this.props.setMarketingStatisticsData({
										data: Data.filter(
											this.props.statistics.marketing.rawData,
											"",
											"",
											this.props.code,
										),
										previous: Data.filter(
											this.props.statistics.marketing.rawPrevious,
											"",
											"",
											this.props.code,
										)
									});

									history.push(`/product`)
								}}>
									Details
								</Button.Ripple>
							</span>
						</div>
					</Col>
				</Row>
			</div>
		)
	}
}

export default connect(
	state => ({
		filter: state.filter,
		statistics: state.statistics,
	}),
	{
		setProduct,
		setUsersStatisticsData,
		setTransactionsStatisticsData,
		setRevenueStatisticsData,
		setMarketingStatisticsData
	}
)(ProductItem)