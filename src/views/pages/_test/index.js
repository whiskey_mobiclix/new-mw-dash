import React from "react"
import "./style.scss"

class Test extends React.Component{
	state = {
		position: 0,
	}

	componentDidMount(){
		setInterval(() => {
			this.setState({
				position: this.state.position + 1,
			})
		}, 50)
	}

	render(){
		return <div className="animation" style={{
			// border: "1px solid #333",
			transform: "scale(2)",
			width: "435px",
			height: "942px",
			background: `url(${process.env.REACT_APP_RESOURCE_DOMAIN}/images/i6.png)`,
			backgroundPosition: `center ${this.state.position * -942}px`,
    		backgroundSize: "100%",
		}}>
		</div>
	}
}

export default Test