import React from "react";
import { connect } from "react-redux"
import Selection from "../../../../components/selection";
import { Comparision } from "../../../../utility/modules";
import Number from "../../../../utility/helpers/number"
import Permissions from "../../../../utility/helpers/permissions"
import Wrapper from "../../../../utility/modules/wrapper"
import Data from "../../../../utility/helpers/data"

//CSS
import "./style.scss";

class Filter extends React.Component{
	state = {
		active: "users",
		values: {
			users: "total_users",
			transactions: "total_trans",
			marketing: "cost",
			revenue: "Gross_Revenue",
		},
		total: {
			users: {
				current: 0,
				previous: 0,
			},
			transactions: {
				current: 0,
				previous: 0,
			},
			revenue: {
				current: 0,
				previous: 0,
			},
			marketing: {
				current: 0,
				previous: 0,
			}
		}
	}

	componentDidMount(){
		this.setState({
			total: this.total()
		})
	}

	componentDidUpdate(prevProps){
		if(prevProps.statistics !== this.props.statistics){
			this.setState({
				total: this.total()
			})
		}
	}

	total = () => {
		return {
			users: {
				current: Number.total(this.props.statistics.users.data.map(o => parseInt(o.total_users || 0))),
				previous: Number.total(this.props.statistics.users.previous.map(o => parseInt(o.total_users || 0))),
			},
			transactions: {
				current: Number.total(this.props.statistics.transactions.data.map(o => parseFloat(o.total_trans || 0))),
				previous: Number.total(this.props.statistics.transactions.previous.map(o => parseFloat(o.total_trans || 0))),
			},
			revenue: {
				current: Number.total(this.props.statistics.revenue.data.map(o => parseFloat(o.Gross_Revenue || 0))),
				previous: Number.total(this.props.statistics.revenue.previous.map(o => parseFloat(o.Gross_Revenue || 0))),
			},
			marketing: {
				current: Number.total(this.props.statistics.marketing.data.map(o => parseFloat(o.cost || 0) / 1000000)),
				previous: Number.total(this.props.statistics.marketing.previous.map(o => parseFloat(o.cost || 0) / 1000000)),
			}
		}
	}

	handleClick = id => {
		let _state = this.state;
		_state.active = id;
		this.setState(_state);
		this.props.onChange && this.props.onChange({
			active: id,
			value: _state.values[id],
		});
	}

	handleFilterChange = (id, value) => {
		let _state = this.state;
		_state.values[id] = value;

		this.setState(_state);
		this.props.onChange && this.props.onChange({
			active: id,
			value: _state.values[id],
		});
	}

	render(){
		return (
			<div className="product-general-filter">
				<Wrapper resource={Permissions.USERS_STATS}>
					<div className="item">
						<div className={`selection-wrapper ${this.state.active === "users" ? "active" : ""}`}>
							{
								this.state.active !== "users"
								&&
								<div className="item-overlay" onClick={() => {
									this.handleClick("users")
								}}/>
							}
							
							<Selection {...{
								value: this.state.values.users,
								options: [
									{
										name: "Total Users",
										value: "total_users",
									},
									{
										name: "Total New Users",
										value: "new_users",
									},
									{
										name: "Total Active Users",
										value: "new_active_users",
									},
									{
										name: "Total Unsubscribed Users",
										value: "churn_users",
									},
								],
								onChange: selected => {
									this.handleFilterChange("users", selected.value)
									this.setState({
										total: {
											...this.state.total,
											users: {
												current: Number.total(this.props.statistics.users.data.map(o => parseFloat(o[selected.value] || 0))),
												previous: Number.total(this.props.statistics.users.previous.map(o => parseFloat(o[selected.value] || 0))),
											}
										}
									})
								}
							}} />
						</div>

						<Comparision {...{
							current: this.state.total.users.current,
							previous: this.state.total.users.previous,
						}}/>
					</div>
				</Wrapper>

				<Wrapper resource={Permissions.TRANSACTIONS_STATS}>
					<div className="item">
						<div className={`selection-wrapper ${this.state.active === "transactions" ? "active" : ""}`}>
							{
								this.state.active !== "transactions"
								&&
								<div className="item-overlay" onClick={() => {
									this.handleClick("transactions")
								}}/>
							}
							
							<Selection {...{
								value: this.state.values.transactions,
								options: [
									{
										name: "All Transactions",
										value: "total_trans",
									},
									{
										name: "Success Transactions",
										value: "success_trans",
									},
									{
										name: "Failed Transactions",
										value: "fail_trans",
									},
								],
								onChange: selected => {
									this.handleFilterChange("transactions", selected.value)
									this.setState({
										total: {
											...this.state.total,
											transactions: {
												current: Number.total(this.props.statistics.transactions.data.map(o => parseFloat(o[selected.value] || 0))),
												previous: Number.total(this.props.statistics.transactions.previous.map(o => parseFloat(o[selected.value] || 0))),
											}
										}
									})
								}
							}} />
						</div>

						<Comparision {...{
							current: this.state.total.transactions.current,
							previous: this.state.total.transactions.previous,
						}}/>
					</div>
				</Wrapper>

				<Wrapper resource={Permissions.REVENUES_STATS}>
					<div className="item">
						<div className={`selection-wrapper ${this.state.active === "revenue" ? "active" : ""}`}>
							{
								this.state.active !== "revenue"
								&&
								<div className="item-overlay" onClick={() => {
									this.handleClick("revenue")
								}}/>
							}
							
							<Selection {...{
								value: this.state.values.revenue,
								options: [
									{
										name: "Gross Revenue",
										value: "Gross_Revenue",
									},
									{
										name: "Net Revenue",
										value: "Net_Revenue",
									},
								],
								onChange: selected => {
									this.handleFilterChange("revenue", selected.value)
									this.setState({
										total: {
											...this.state.total,
											revenue: {
												current: Number.total(this.props.statistics.revenue.data.map(o => parseFloat(o[selected.value] || 0))),
												previous: Number.total(this.props.statistics.revenue.previous.map(o => parseFloat(o[selected.value] || 0))),
											}
										}
									})
								}
							}} />
						</div>

						<Comparision {...{
							current: this.state.total.revenue.current,
							previous: this.state.total.revenue.previous,
							prefix: "$"
						}}/>
					</div>
				</Wrapper>

				<div className="item">
					<div className={`selection-wrapper ${this.state.active === "marketing" ? "active" : ""}`}>
						{
							this.state.active !== "marketing"
							&&
							<div className="item-overlay" onClick={() => {
								this.handleClick("marketing")
							}}/>
						}
						
						<Selection {...{
							value: this.state.values.marketing,
							options: [
								{
									name: "Google Ads Cost",
									value: "cost",
								},
							],
						}} />
					</div>

					<Comparision {...{
						current: this.state.total.marketing.current,
						previous: this.state.total.marketing.previous,
						prefix: "$"
					}}/>
				</div>
			</div>
		)
	}
}

export default connect(
	state => ({
		permissions: state.permissions,
		statistics: state.statistics,
		app: state.app,
	}),
	{}
)(Filter);