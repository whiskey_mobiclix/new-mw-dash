import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { ColumnChart } from '../../../../../utility/modules/chart';
import Data from "../../../../../utility/helpers/data"
import Date from "../../../../../utility/helpers/date"
import "./style.scss"

class Revenue extends React.Component {
	render() {
		const dates = Date.days(this.props.gfilter.daterange.value);
		const statsData = Data.fill(dates, Data.groupByDate(this.props.revenue.data))

		return (
			<div className="product-general-revenue">
				<ColumnChart
					categories={dates}
					series={[
						{
							name: this.props.filter.value,
							data: statsData.map(o => parseFloat(o[this.props.filter.value] || 0))
						}
					]}
				/>
			</div>
		);
	}
}
export default connect(state => ({
	revenue: state.statistics.revenue,
	gfilter: state.filter,
}))(Revenue);
