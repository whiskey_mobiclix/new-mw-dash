import React from 'react';
import { connect } from 'react-redux';
import { Card, CardHeader, CardTitle, CardBody } from 'reactstrap';
import { ColumnChart } from '../../../../../utility/modules/chart';
import Data from '../../../../../utility/helpers/data';
import Date from "../../../../../utility/helpers/date"

import moment from 'moment';

//CSS
import './style.scss';

let $success = '#7367F0',
	$danger = '#ff80df';

class OverviewGeneralTransactions extends React.Component {
	colors = () => {
		const type = this.props.filter.value;

		if (type === 'total_trans') return [$success, $danger];
		if (type === 'success_trans') return [$success];
		return [$danger];
	};

	strokes = () => {
		const type = this.props.filter.value;

		if (type === 'total_trans') return ['#877df2', '#ff93e3'];
		if (type === 'success_trans') return ['#877df2'];
		return ['#ff93e3'];
	};

	series = () => {
		const dates = Date.days(this.props.gfilter.daterange.value);
		const type = this.props.filter.value;
		const statsData = Data.fill(dates, Data.groupByDate(this.props.transactions.data));

		if (type === 'total_trans')
			return [
				{
					name: 'Success',
					data: statsData.map(o => parseInt(o.success_trans || 0))
				},
				{
					name: 'Failed',
					data: statsData.map(o => parseInt(o.fail_trans || 0))
				}
			];

		return [
			{
				name: type,
				data: statsData.map(o => parseInt(o[type] || 0))
			}
		];
	};

	render() {
		const dates = Date.days(this.props.gfilter.daterange.value);

		return (
			<div className="product-general-transactions">
				<ColumnChart
					options={{
						legend: {
							onItemClick: {
						        toggleDataSeries: false
						    },
						},
						stroke: {
							show: true,
							colors: this.strokes()
						}
					}}
					categories={dates}
					series={this.series()}
					colors={this.colors()}
				/>
			</div>
		);
	}
}
export default connect(state => ({
	transactions: state.statistics.transactions,
	gfilter: state.filter,
}))(OverviewGeneralTransactions);
