import React from "react"
import { connect } from "react-redux"
import { Card, CardHeader, CardTitle, CardBody } from "reactstrap"
import { ColumnChart } from "../../../../../utility/modules/chart"
import moment from "moment"
import Data from "../../../../../utility/helpers/data"
import Date from "../../../../../utility/helpers/date"

//CSS
import "./style.scss"

class OverviewGeneralUsers extends React.Component {
  state = {
    refresh: false,
  }

  render() {
    const dates = Date.days(this.props.gfilter.daterange.value);
    const statsData = Data.fill(dates, Data.groupByDate(this.props.users.data));

    return (
      <div className={`product-general-users ${this.props.users.fetching ? "fetching" : ""}`}>
        {
          !this.props.users.fetching
          &&
          <ColumnChart
            categories={dates}
            series={[
              {
                name: this.props.filter.value,
                data: statsData.map(o => parseFloat(o[this.props.filter.value] || 0))
              }
            ]}
          />
        }
      </div>
    )
  }
}

export default connect(
  state => ({
    users: state.statistics.users,
    gfilter: state.filter,
  }),
  {}
)(OverviewGeneralUsers)