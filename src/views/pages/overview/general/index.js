import React from 'react';
import { Card, CardBody, CardHeader, CardTitle, Row, Col } from 'reactstrap';

import Permissions from "../../../../utility/helpers/permissions"
import Wrapper from "../../../../utility/modules/wrapper"

import Filter from './Filter';
import Users from './users';
import Transactions from './transactions';
import Marketing from './marketing';
import Revenue from './revenue';

class OverviewGeneral extends React.Component {
	state = {
		filter: {
			active: 'users',
			value: 'total_users'
		}
	};

	render() {
		return (
			<Row>
				<Col lg="12" md="12" sm="12">
					<Card>
						<CardHeader>
							<CardTitle>
								Overview Statistics
							</CardTitle>
						</CardHeader>
						
						<CardBody className="pt-0">
							<Filter
								{...{
									onChange: data => {
										this.setState({
											filter: data
										});
									}
								}}
							/>

							{this.state.filter.active === 'users' && (
								<Wrapper resource={Permissions.USERS_STATS}>
									<Users
										{...{
											filter: this.state.filter
										}}
									/>
								</Wrapper>
							)}

							{this.state.filter.active === 'transactions' && (
								<Wrapper resource={Permissions.TRANSACTIONS_STATS}>
									<Transactions
										{...{
											filter: this.state.filter
										}}
									/>
								</Wrapper>
							)}

							{this.state.filter.active === 'revenue' && (
								<Wrapper resource={Permissions.REVENUES_STATS}>
									<Revenue
										{...{
											filter: this.state.filter
										}}
									/>
								</Wrapper>
							)}

							{this.state.filter.active === 'marketing' && (
								<Marketing
									{...{
										filter: this.state.filter
									}}
								/>
							)}
						</CardBody>
					</Card>
				</Col>
			</Row>
		);
	}
}
export default OverviewGeneral;
