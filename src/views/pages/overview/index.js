import React from 'react'
import { connect } from 'react-redux'
import { Filter } from '../../../utility/modules'
import Title from '../../../utility/modules/title'
import { getStatisticsData } from '../../../redux/actions/statistics'
import Spinner from '../../../components/@vuexy/spinner/Loading-spinner';
import Date from "../../../utility/helpers/date"

//Layouts
import General from './general'
import Extra from './extra'

class Product extends React.Component {
	componentDidMount() {
		if(!this.props.users.data.length)
			this.props.getStatisticsData({
				product: this.props.filter.product.value,
				dates: Date.range(this.props.filter.daterange.value),
			})
	}

	render() {
		if(this.props.app.locked) return <Spinner />

		return (
			<div className="p-header">
				<Title text={'Dashboard Overview'} />
				<Filter />
				<General />
				<Extra />
			</div>
		);
	}
}

export default connect(
	state => ({
		users: state.statistics.users,
		filter: state.filter,
		app: state.app,
	}),
	{
		getStatisticsData
	}
)(Product);
