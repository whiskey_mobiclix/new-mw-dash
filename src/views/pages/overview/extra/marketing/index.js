import React from "react"
import { connect } from "react-redux"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle
} from "reactstrap"
import Number from "../../../../../utility/helpers/number"
import Data from "../../../../../utility/helpers/data"
import Country from "../../../../../utility/helpers/country"

import { PieChart } from "../../../../../utility/modules/chart";
import { Comparision } from "../../../../../utility/modules";

class Marketing extends React.Component {
  render() {
    const statsData = Data.groupByMarkets(this.props.marketing.data).filter(o => o.cost).sort((a, b) => {
      if(parseFloat(a.cost) < parseFloat(b.cost)) return 1
      return -1
    })

    return (
      <Card>
        <CardHeader>
          <CardTitle>
            <div style={{
              marginBottom: ".5rem"
            }}>
              <Comparision {...{
                current: Number.total(this.props.marketing.data.map(o => parseFloat(o.cost || 0) / 1000000)),
                previous: Number.total(this.props.marketing.previous.map(o => parseFloat(o.cost || 0) / 1000000)),
                prefix: "$"
              }}/>
            </div>
            Google Ads By Markets
          </CardTitle>
        </CardHeader>

        <CardBody className="pt-2">
          <PieChart {...{
            labels: statsData.map(o => Country.getCountryByCode(o.country)),
            series: statsData.map(o => parseFloat(o.cost || 0) / 1000000),
          }}/>
        </CardBody>
      </Card>
    )
  }
}
export default connect(
  state => ({
     marketing: state.statistics.marketing,
  })
)(Marketing)