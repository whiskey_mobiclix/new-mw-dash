import React from "react"
import { Card, CardHeader, CardTitle, CardBody } from "reactstrap"
import { ColumnChart } from "../../../../../utility/modules/chart";
import { Comparision } from "../../../../../utility/modules";

class TotalSessions extends React.Component {
  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>
            <div style={{
              marginBottom: ".5rem"
            }}>
              <Comparision />
            </div>
            Total session
          </CardTitle>
        </CardHeader>
        <CardBody>
          <ColumnChart {...{
            categories: [
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              "",
              ""
            ],
            series: [
              {
                name: "",
                data: [140, 180, 305, 240, 295, 150, 205, 160, 295]
              },
            ]
          }}/>
        </CardBody>
      </Card>
    )
  }
}
export default TotalSessions