import React from 'react';
import { Row, Col } from 'reactstrap';

//CSS
import './style.scss';

import Permissions from "../../../../utility/helpers/permissions"
import Wrapper from "../../../../utility/modules/wrapper"

//LAYOUTS
import Users from './users';
import Subscribers from './subscribers';
import Revenue from './revenue';
// import SessionDuration from './session-duration';
// import AvgTimeSpent from './avg-time-spent';
// import BounceRate from './bounce-rate';
// import TotalSessions from './total-sessions';
import SuccessTransactions from './success-transactions';
import TotalTransactions from "./total-transactions"
import Marketing from "./marketing"

class Extra extends React.Component {
	render() {
		return (
			<div className="product-extra">
				<Row>
					<Wrapper resource={Permissions.USERS_STATS}>
						<Col lg="4" md="6" sm="12">
							<Users />
						</Col>
					</Wrapper>

					<Wrapper resource={Permissions.USERS_STATS}>
						<Col lg="4" md="6" sm="12">
							<Subscribers />
						</Col>
					</Wrapper>

					{/* <Col lg="4" md="6" sm="12">
						<AvgTimeSpent />
					</Col> */}

					{/* <Col lg="4" md="6" sm="12">
						<TotalSessions />
					</Col> */}

					<Wrapper resource={Permissions.TRANSACTIONS_STATS}>
						<Col lg="4" md="6" sm="12">
							<SuccessTransactions />
						</Col>
					</Wrapper>

					<Wrapper resource={Permissions.TRANSACTIONS_STATS}>
						<Col lg="4" md="6" sm="12">
							<TotalTransactions />
						</Col>
					</Wrapper>

					<Wrapper resource={Permissions.REVENUES_STATS}>
						<Col lg="4" md="6" sm="12">
							<Revenue />
						</Col>
					</Wrapper>

					<Col lg="4" md="6" sm="12">
						<Marketing />
					</Col>

					{/* <Col lg="4" md="6" sm="12">
						<BounceRate />
					</Col>

					<Col lg="4" md="6" sm="12">
						<SessionDuration />
					</Col> */}
				</Row>
			</div>
		);
	}
}

export default Extra;
