import React from "react"
import { connect } from "react-redux"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle
} from "reactstrap"
import Number from "../../../../../utility/helpers/number"
import Data from "../../../../../utility/helpers/data"
import Country from "../../../../../utility/helpers/country"

import { PieChart } from "../../../../../utility/modules/chart";
import { Comparision } from "../../../../../utility/modules";

class TotalTransactions extends React.Component {
  render() {
    const statsData = Data.groupByMarkets(this.props.transactions.data).filter(o => parseFloat(o.total_trans) > 0).sort((a, b) => {
      if(parseFloat(a.total_trans) < parseFloat(b.total_trans)) return 1
      return -1
    })

    return (
      <Card>
        <CardHeader>
          <CardTitle>
            <div style={{
              marginBottom: ".5rem"
            }}>
              <Comparision {...{
                current: Number.total(this.props.transactions.data.map(o => parseFloat(o.total_trans || 0))),
                previous: Number.total(this.props.transactions.previous.map(o => parseFloat(o.total_trans || 0))),
              }}/>
            </div>
            Transactions By Markets
          </CardTitle>
        </CardHeader>

        <CardBody className="pt-2">
          <PieChart {...{
            labels: statsData.map(o => Country.getCountryByCode(o.country)),
            series: statsData.map(o => parseFloat(o.total_trans || 0)),
          }}/>
        </CardBody>
      </Card>
    )
  }
}
export default connect(
  state => ({
    transactions: state.statistics.transactions,
  })
)(TotalTransactions)