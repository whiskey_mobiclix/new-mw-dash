import React from 'react';
import { connect } from 'react-redux';
import moment from 'moment';
import { Card, CardHeader, CardTitle, CardBody } from 'reactstrap';

import Number from '../../../../../utility/helpers/number';
import Data from '../../../../../utility/helpers/data';
import Date from '../../../../../utility/helpers/date';
import { LineChart } from '../../../../../utility/modules/chart';
import { Comparision } from '../../../../../utility/modules';

class SuccessTransactions extends React.Component {
	render() {
		const dates = Date.days(this.props.gfilter.daterange.value);
		const statsData = Data.fill(dates, Data.groupByDate(this.props.transactions.data));

		return (
			<Card>
				<CardHeader>
					<CardTitle>
						<div
							style={{
								marginBottom: '.5rem'
							}}
						>
							<Comparision
								current={Number.total(
									this.props.transactions.data.map(o =>
										parseInt(o.success_trans || 0)
									)
								)}
								previous={Number.total(
									this.props.transactions.previous.map(o =>
										parseInt(o.success_trans || 0)
									)
								)}
							/>
						</div>
						Success Transactions
					</CardTitle>
				</CardHeader>
				<CardBody>
					<LineChart
						options={{
							xaxis: {
								labels: { show: false }
							}
						}}
						categories={dates}
						series={[
							{
								name: 'Success transactions',
								data: statsData.map(o => parseInt(o.success_trans || 0))
							}
						]}
					/>
				</CardBody>
			</Card>
		);
	}
}

export default connect(state => ({
	transactions: state.statistics.transactions,
	gfilter: state.filter,
}))(SuccessTransactions);
