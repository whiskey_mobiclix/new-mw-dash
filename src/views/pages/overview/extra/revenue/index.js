import React from "react"
import { connect } from "react-redux"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle
} from "reactstrap"
import Number from "../../../../../utility/helpers/number"
import Data from "../../../../../utility/helpers/data"
import Country from "../../../../../utility/helpers/country"

import { PieChart } from "../../../../../utility/modules/chart";
import { Comparision } from "../../../../../utility/modules";

class Revenue extends React.Component {
  render() {
    const statsData = Data.groupByMarkets(this.props.revenue.data).filter(o => o.Net_Revenue).sort((a, b) => {
      if(parseFloat(a.Net_Revenue) < parseFloat(b.Net_Revenue)) return 1
      return -1
    })

    return (
      <Card>
        <CardHeader>
          <CardTitle>
            <div style={{
              marginBottom: ".5rem"
            }}>
              <Comparision {...{
                current: Number.total(this.props.revenue.data.map(o => parseFloat(o.Net_Revenue || 0))),
                previous: Number.total(this.props.revenue.previous.map(o => parseFloat(o.Net_Revenue || 0))),
                prefix: "$"
              }}/>
            </div>
            Net Revenue By Markets
          </CardTitle>
        </CardHeader>

        <CardBody className="pt-2">
          <PieChart {...{
            labels: statsData.map(o => Country.getCountryByCode(o.country)),
            series: statsData.map(o => parseFloat(o.Net_Revenue || 0)),
          }}/>
        </CardBody>
      </Card>
    )
  }
}
export default connect(
  state => ({
     revenue: state.statistics.revenue,
  })
)(Revenue)