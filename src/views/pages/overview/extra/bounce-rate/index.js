import React from "react"
import { Card, CardHeader, CardTitle, CardBody } from "reactstrap"
import { LineChart } from "../../../../../utility/modules/chart";
import { Comparision } from "../../../../../utility/modules";

class BounceRate extends React.Component {
  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>
            <div style={{
              marginBottom: ".5rem"
            }}>
              <Comparision />
            </div>
            Bounce Rate
          </CardTitle>
        </CardHeader>
        <CardBody>
          <LineChart {...{
            categories: [
              "Jan",
              "Feb",
              "Mar",
              "Apr",
              "May",
              "Jun",
              "July",
              "Aug",
              "Sep",
              "Oct",
              "Nov",
              "Dec"
            ],
            series: [
              {
                name: "Sales",
                data: [140, 180, 305, 240, 295, 150, 205, 160, 295, 125, 255, 205]
              },
            ]
          }}/>
        </CardBody>
      </Card>
    )
  }
}
export default BounceRate