import axios from "axios";
import { Cookies } from "../helpers";

const instance = axios.create({
  timeout: 20000,
  headers: {
  	Authorization: `Bearer ${Cookies.get("access_token")}`
  }
});

export default {
	get: (url, token) => {
		if(token){
			return axios.get(`${process.env.REACT_APP_API_DOMAIN}${url}`, {
				timeout: 10000,
				headers: {
					Authorization: `Bearer ${token}`
				}
			})	
		}
		return instance.get(`${process.env.REACT_APP_API_DOMAIN}${url}`)
	},
	GET: (url, token) => {
		if(token){
			return axios.get(`${process.env.REACT_APP_REPORT_DOMAIN}${url}`, {
				timeout: 10000,
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
		}
		return instance.get(`${process.env.REACT_APP_REPORT_DOMAIN}${url}`)
	},
	post: (url, params) => {
		const token = Cookies.get("access_token");

		return axios.post(`${process.env.REACT_APP_API_DOMAIN}${url}`, params, {
			timeout: 10000,
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
	},
	POST: (url, params) => {
		const token = Cookies.get("access_token");

		return axios.post(`${process.env.REACT_APP_REPORT_DOMAIN}${url}`, params, {
			timeout: 10000,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`,
			}
		})
	},
	PUT: (url, params) => {
		const token = Cookies.get("access_token");

		return axios.put(`${process.env.REACT_APP_REPORT_DOMAIN}${url}`, params, {
			timeout: 10000,
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${token}`,
			}
		})
	}
}