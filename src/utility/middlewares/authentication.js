import React from "react";
import Login from "../../views/pages/authentication/login/Login";
import { Cookies } from "../helpers";

import Spinner from "../../components/@vuexy/spinner/Loading-spinner"

const LoginLayout = () => {
	return <div className="full-layout wrapper bg-full-screen-image blank-page dark-layout">
		<div className="app-content">
			<div className="content-wrapper">
				<div className="content-body">
					<div className="flexbox-container">
						<main className="main w-100">
							<Login />
						</main>
					</div>
				</div>
			</div>
		</div>
	</div>
}

const Authentication = (Comp, props) => {
	if(!props.user.data || !props.user.data.id){
		if(Cookies.get("access_token")) {
			props.auth().catch(() => {
				Cookies.delete("access_token");
				window.location.reload();
			});
			return <Spinner />
		}
		return <LoginLayout />
	}

	if(!!Object.keys(props.user.data).length)
		props.getConfig()

	return Comp
}

export default Authentication;