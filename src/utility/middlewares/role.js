import { history } from "../../history";

const RoleMiddleware = (Comp, props) => {
	try{
		if(props.user.data.group && props.user.data.group === "USER_GROUP_PARTNER"){
			history.push(`/product`)
		} else if (props.user.data.group && props.user.data.group === "USER_GROUP_FINANCE"){
			history.push(`/finance`)
		} else {
			history.push(`/admin`)
		}
	}catch(e){
		console.log(e);
	};

	return Comp
}

export default RoleMiddleware;