import React from "react";
import { connect } from "react-redux"
import Selection from "../../../components/selection";
import { setMonthRange } from "../../../redux/actions/filter"
import { getStatisticsData } from "../../../redux/actions/statistics"
import Date from "../../../utility/helpers/date"

//CSS
import "./style.scss";

class MonthFilter extends React.Component{
	render(){
		return (
			<Selection {...{
				value: this.props.filter.monthrange.value,
				options: this.props.filter.monthrange.options,
				onChange: selected => {
					this.props.setMonthRange(selected.value)
					// this.props.getStatisticsData({
					// 	dates: Date.range(selected.value),
					// 	product: this.props.filter.product.value,
					// })
				}
			}}/>
		)
	}
}

export default connect(
	state => ({
		filter: state.filter,
	}),
	{
		setMonthRange,
		getStatisticsData,
	}
)(MonthFilter);