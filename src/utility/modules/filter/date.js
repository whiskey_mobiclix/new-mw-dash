import React from "react";
import { connect } from "react-redux"
import Selection from "../../../components/selection";
import { setDateRange, setDateRangeOptions } from "../../../redux/actions/filter"
import { getStatisticsData } from "../../../redux/actions/statistics"
import Date from "../../../utility/helpers/date"
import moment from "moment"

//CSS
import "./style.scss";

class DateFilter extends React.Component{
	state = {
		modal: false,
	}

	toggleModal = () => {
		this.setState({
			modal: !this.state.modal
		})
	}

	render(){
		return (
			<>
				<Selection {...{
					type: "date",
					value: this.props.filter.daterange.value,
					options: this.props.filter.daterange.options,
					onChange: selected => {
						this.props.setDateRange(selected.value)
						this.props.getStatisticsData({
							dates: Date.range(selected.value),
							product: this.props.filter.product.value,
						})
					},
					onCustomDateRange: e => {
						const value = `${moment(e.from).format("DD MMM YYYY")} - ${moment(e.to).format("DD MMM YYYY")}`
						if(!this.props.filter.daterange.options.filter(o => o.value == value).length){
							this.props.setDateRangeOptions(this.props.filter.daterange.options.concat({
								name: value,
								value: value,
							}))
						}

						this.props.setDateRange(value)

						this.props.getStatisticsData({
							dates: Date.range(value),
							product: this.props.filter.product.value,
						})
					}
				}}/>
			</>
		)
	}
}

export default connect(
	state => ({
		filter: state.filter,
	}),
	{
		setDateRange,
		setDateRangeOptions,
		getStatisticsData,
	}
)(DateFilter);