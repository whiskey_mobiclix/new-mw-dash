import React from 'react';
import { connect } from 'react-redux';
import Selection from '../../../components/selection';
import { setDateRange, setProduct } from '../../../redux/actions/filter';
import DateFilter from './date';
import Config from '../../../utility/helpers/config';
import { setUsersStatisticsData } from '../../../redux/actions/statistics/users';
import { setTransactionsStatisticsData } from '../../../redux/actions/statistics/transactions';
import { setRevenueStatisticsData } from '../../../redux/actions/statistics/revenue';
import { setMarketingStatisticsData } from '../../../redux/actions/statistics/marketing';
import Wrapper from "../../../utility/modules/wrapper"
import Data from '../../../utility/helpers/data';
import { history } from "../../../history"

//CSS
import './style.scss';

class Filter extends React.Component {
	state = {
		country: null,
		mno: null,
		product: "",
	};

	componentWillUnmount() {
		this.props.setUsersStatisticsData({
			data: this.props.statistics.users.rawData,
			previous: this.props.statistics.users.rawPrevious
		});
		this.props.setTransactionsStatisticsData({
			data: this.props.statistics.transactions.rawData,
			previous: this.props.statistics.transactions.rawPrevious
		});
		this.props.setRevenueStatisticsData({
			data: this.props.statistics.revenue.rawData,
			previous: this.props.statistics.revenue.rawPrevious
		});
		this.props.setMarketingStatisticsData({
			data: this.props.statistics.marketing.rawData,
			previous: this.props.statistics.marketing.rawPrevious
		});
	}

	render() {
		return (
			<div className="mw-filter">
				<DateFilter />

				{
					!/^\/product/.test(history.location.pathname)
					&&
					<Wrapper group={["USER_GROUP_ADMIN"]}>
						<Selection
							{...{
								value: this.state.product,
								options: [
									{
										name: "All products",
										value: "",
									}
								].concat(Config.getAllProducts(this.props.app.config)),
								onChange: selected => {
									this.setState({
										product: selected.value,
									})

									this.props.setUsersStatisticsData({
										data: Data.filter(
											this.props.statistics.users.rawData,
											this.state.country,
											this.setState.mno,
											selected.value
										),
										previous: Data.filter(
											this.props.statistics.users.rawPrevious,
											this.state.country,
											this.setState.mno,
											selected.value
										)
									});
									this.props.setTransactionsStatisticsData({
										data: Data.filter(
											this.props.statistics.transactions.rawData,
											this.state.country,
											this.setState.mno,
											selected.value
										),
										previous: Data.filter(
											this.props.statistics.transactions.rawPrevious,
											this.state.country,
											this.setState.mno,
											selected.value
										)
									});
									this.props.setRevenueStatisticsData({
										data: Data.filter(
											this.props.statistics.revenue.rawData,
											this.state.country,
											this.setState.mno,
											selected.value
										),
										previous: Data.filter(
											this.props.statistics.revenue.rawPrevious,
											this.state.country,
											this.setState.mno,
											selected.value
										)
									});
									this.props.setMarketingStatisticsData({
										data: Data.filter(
											this.props.statistics.marketing.rawData,
											this.state.country,
											this.setState.mno,
											selected.value
										),
										previous: Data.filter(
											this.props.statistics.marketing.rawPrevious,
											this.state.country,
											this.setState.mno,
											selected.value
										)
									});
								}
							}}
						/>
					</Wrapper>
				}

				<Selection
					{...{
						value: null,
						options: [
							{
								name: 'All markets',
								value: null
							}
						].concat(
							Config.getMarketsByProduct(
								this.props.app.config,
								this.props.filter.product.value
							)
						),
						onChange: selected => {
							this.setState({
								country: selected.value
							});
							this.props.setUsersStatisticsData({
								data: Data.filter(
									this.props.statistics.users.rawData,
									selected.value,
									this.state.mno,
									this.state.product,
								),
								previous: Data.filter(
									this.props.statistics.users.rawPrevious,
									selected.value,
									this.state.mno,
									this.state.product,
								)
							});
							this.props.setTransactionsStatisticsData({
								data: Data.filter(
									this.props.statistics.transactions.rawData,
									selected.value,
									this.state.mno,
									this.state.product,
								),
								previous: Data.filter(
									this.props.statistics.transactions.rawPrevious,
									selected.value,
									this.state.mno,
									this.state.product,
								)
							});
							this.props.setRevenueStatisticsData({
								data: Data.filter(
									this.props.statistics.revenue.rawData,
									selected.value,
									this.state.mno,
									this.state.product,
								),
								previous: Data.filter(
									this.props.statistics.revenue.rawPrevious,
									selected.value,
									this.state.mno,
									this.state.product,
								)
							});
							this.props.setMarketingStatisticsData({
								data: Data.filter(
									this.props.statistics.marketing.rawData,
									selected.value,
									this.state.mno,
									this.state.product,
								),
								previous: Data.filter(
									this.props.statistics.marketing.rawPrevious,
									selected.value,
									this.state.mno,
									this.state.product,
								)
							});
						}
					}}
				/>

				{/*<Selection {...{
					value: null,
					options: [
						{
							name: "All payment methods",
							value: null,
						},
						{
							name: "Mobile",
							value: "mobile",
						},
						{
							name: "Credit card",
							value: "credit",
						},
					],
					onChange: selected => {
						console.log(selected);
					}
				}}/>*/}

				<Selection
					{...{
						value: null,
						options: [
							{
								name: 'All MNOs',
								value: null
							}
						].concat(
							Config.getNNOsByProduct(
								this.props.app.config,
								this.props.filter.product.value
							)
						),
						onChange: selected => {
							this.setState({
								mno: selected.value
							});
							this.props.setUsersStatisticsData({
								data: Data.filter(
									this.props.statistics.users.rawData,
									this.state.country,
									selected.value,
									this.state.product,
								),
								previous: Data.filter(
									this.props.statistics.users.rawPrevious,
									this.state.country,
									selected.value,
									this.state.product,
								)
							});
							this.props.setTransactionsStatisticsData({
								data: Data.filter(
									this.props.statistics.transactions.rawData,
									this.state.country,
									selected.value,
									this.state.product,
								),
								previous: Data.filter(
									this.props.statistics.transactions.rawPrevious,
									this.state.country,
									selected.value,
									this.state.product,
								)
							});
							this.props.setRevenueStatisticsData({
								data: Data.filter(
									this.props.statistics.revenue.rawData,
									this.state.country,
									selected.value,
									this.state.product,
								),
								previous: Data.filter(
									this.props.statistics.revenue.rawPrevious,
									this.state.country,
									selected.value,
									this.state.product,
								)
							});
							this.props.setMarketingStatisticsData({
								data: Data.filter(
									this.props.statistics.marketing.rawData,
									this.state.country,
									selected.value,
									this.state.product,
								),
								previous: Data.filter(
									this.props.statistics.marketing.rawPrevious,
									this.state.country,
									selected.value,
									this.state.product,
								)
							});
						}
					}}
				/>

				{/*<Selection {...{
					value: null,
					options: [
						{
							name: "All devices",
							value: null,
						},
						{
							name: "Mobile",
							value: "mobile",
						},
						{
							name: "Credit card",
							value: "credit",
						},
					],
					onChange: selected => {
						console.log(selected);
					}
				}}/>*/}
			</div>
		);
	}
}

export default connect(
	state => ({
		filter: state.filter,
		app: state.app,
		statistics: state.statistics
	}),
	{
		setDateRange,
		setUsersStatisticsData,
		setTransactionsStatisticsData,
		setRevenueStatisticsData,
		setProduct,
		setMarketingStatisticsData,
	}
)(Filter);
