import React from 'react';
import Chart from 'react-apexcharts';
import COLOR from '../../../../utility/helpers/color';
import moment from 'moment';
import merge from 'lodash/merge';

class ColumnCharts extends React.Component {
	state = {
		refresh: false,
		options: {
			chart: {
				stacked: true,
				toolbar: {
					show: false,
					tools: {
						download: false,
						selection: false,
						zoom: true,
						zoomin: false,
						zoomout: false,
						pan: false,
						reset: true
					},
					autoSelected: 'reset'
				}
			},
			plotOptions: {
				bar: {
					columnWidth: '30%',
					horizontal: false
				}
			},
			colors: this.props.colors || COLOR.base(),
			dataLabels: {
				enabled: false
			},
			grid: {
				borderColor: COLOR.$label_color,
				padding: {
					left: 0,
					right: 0
				}
			},
			legend: {
				show: true,
				position: 'top',
				horizontalAlign: 'left',
				offsetX: 0,
				fontSize: '14px',
				markers: {
					radius: 50,
					width: 10,
					height: 10
				}
			},
			xaxis: {
				// type: "datetime",
				labels: {
					rotate: -45,
					// rotateAlways: true,
					formatter: function(value, timestamp) {
						return new moment(value).format('DD MMM'); // The formatter function overrides format property
					},
					style: {
						colors: COLOR.$stroke_color
					}
				},
				axisTicks: {
					show: false
				},
				categories: this.props.categories,
				axisBorder: {
					show: false
				}
			},
			yaxis: {
				decimalsInFloat: false,
				tickAmount: 5,
				labels: {
					rotate: -45,
					rotateAlways: true,
					style: {
						color: COLOR.$stroke_color
					}
				}
			},
			tooltip: {
				x: {
					show: true
				},
				y: {
					formatter: function(
						value,
						{ series, seriesIndex, dataPointIndex, w }
					) {
						return Math.round(value);
					}
				}
			},
			stroke: {
				show: true,
				colors: ['#877df2'],
				width: 2
			}
		},
		series: this.props.series
	};

	componentDidUpdate(prevProps) {
		if (prevProps.series !== this.props.series) {
			this.refresh();
		}

		if (prevProps.colors !== this.props.colors) {
			this.setState({
				options: {
					...this.state.options,
					colors: this.props.colors
				}
			});
		}
	}

	refresh = () => {
		this.setState({
			options: {
				...this.state.options,
				categories: this.props.categories
			},
			series: this.props.series
		});
	};

	getColumnWidth = length => {
		if (length < 14) return '15%';

		if (length > 30) return '60%';

		return '30%';
	};

	render() {
		const options = {
			...this.state.options,
			xaxis: {
				...this.state.options.xaxis,
				type: this.props.categories.length > 30 ? 'datetime' : null,
				labels: {
					...this.state.options.xaxis.labels,
					rotateAlways: this.props.categories.length <= 30
				}
			},
			plotOptions: {
				...this.state.options.plotOptions,
				bar: {
					...this.state.options.plotOptions.bar,
					columnWidth: this.getColumnWidth(this.props.categories.length)
				}
			}
		};

		const mergedOptions = merge(options, this.props.options || {});

		return (
			<div className="product-general-transactions">
				<Chart
					options={mergedOptions}
					series={this.state.series}
					type="bar"
					height={350}
				/>
			</div>
		);
	}
}
export default ColumnCharts;
