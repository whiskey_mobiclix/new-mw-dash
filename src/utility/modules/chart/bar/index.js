import React from "react"
import { Progress } from "reactstrap"
import Number from "../../../../utility/helpers/number"

//CSS
import "./style.scss"

class BrowserStats extends React.Component {
  render() {
    const total = Number.total(this.props.series.map(o => o.value))

    return (
      <div className="bar-chart">
        {
          this.props.series.map((o, ix) =>
            <React.Fragment key={ix}>
            <div className="d-flex justify-content-between mb-25">
               <div className="browser-info">
                  <p className="mb-25">{o.name}</p>
                  <h4>{o.value}</h4>
                </div>
            </div>
            <Progress className="mb-2" value={o.value / total * 100} />
            </React.Fragment>
          )
        }
      </div>
    )
  }
}
export default BrowserStats