import React from 'react';
import COLOR from '../../../helpers/color';
import Chart from 'react-apexcharts';
import merge from 'lodash/merge';

class LineChart extends React.Component {
	state = {
		options: {
			chart: {
				dropShadow: {
					enabled: true,
					top: 5,
					left: 2,
					blur: 5,
					opacity: 0.15
				},
				toolbar: {
					show: false,
					tools: {
						download: false,
						selection: false,
						zoom: false,
						zoomin: false,
						zoomout: false,
						pan: false,
						reset: true
					},
					autoSelected: 'zoom'
				}
			},
			stroke: {
				curve: 'smooth',
				width: 4
			},
			grid: {
				borderColor: COLOR.$label_color
			},
			legend: {
				show: false
			},
			colors: [COLOR.$purple],
			fill: {
				type: 'gradient',
				gradient: {
					shade: 'dark',
					inverseColors: false,
					gradientToColors: [COLOR.$primary],
					shadeIntensity: 1,
					type: 'horizontal',
					opacityFrom: 1,
					opacityTo: 1,
					stops: [0, 100, 100, 100]
				}
			},
			markers: {
				size: 0,
				hover: {
					size: 5
				}
			},
			xaxis: {
				labels: {
					rotate: -45,
					rotateAlways: true,
					style: {
						colors: COLOR.$stroke_color
					}
				},
				axisTicks: {
					show: false
				},
				categories: this.props.categories,
				axisBorder: {
					show: false
				},
				tickPlacement: 'on'
			},
			yaxis: {
				tickAmount: 5,
				labels: {
					rotate: -45,
					rotateAlways: true,
					style: {
						color: COLOR.$stroke_color
					},
					formatter: val => {
						return val > 999 ? (val / 1000).toFixed(1) + 'k' : val;
					}
				}
			},
			tooltip: {
				x: { show: false }
			}
		},
		series: this.props.series
	};

	componentDidUpdate(prevProps){
		if(prevProps.series != this.props.series){
			this.setState({
				series: this.props.series,
				options: {
					...this.state.options,
					categories: this.props.categories,
				}
			})
		}
	}

	render() {
		const mergedOptions = merge(this.state.options, this.props.options || {});

		return (
			<div className="line-chart">
				<Chart
					options={mergedOptions}
					series={this.state.series}
					type="line"
					height={270}
				/>
			</div>
		);
	}
}

export default LineChart;
