import React from "react";
import COLOR from "../../../helpers/color";
import Chart from "react-apexcharts"
import { Circle } from "react-feather"
import Number from "../../../helpers/number"

//CSS
import "./style.scss";

class PieChart extends React.Component{
	state = {
	    options: {
	      chart: {
	        toolbar: {
	          show: false
	        }
	      },
	      dataLabels: {
	        enabled: false
	      },
	      legend: { show: false },
	      comparedResult: [2, -3, 8],
	      labels: this.props.labels,
	      stroke: { width: 0 },
	      colors: COLOR.base(),
	      fill: {
	        type: "gradient",
	        gradient: {
	          gradientToColors: COLOR.light(),
	        }
	      },
	      tooltip: {
	      	custom: function({series, seriesIndex, dataPointIndex, w}) {
			    return '<div class="arrow_box">' +
			      '<span style="font-size: 12px;padding: 10px;display: inline-block; color: #fff; font-weight: 600; background: '+COLOR.base()[seriesIndex]+'">' + w.config.labels[seriesIndex] + ': ' + series[seriesIndex] + '</span>' +
			      '</div>'
			},
	      },
	      noData: {
			  text: "No data found",
			  align: 'center',
			  verticalAlign: 'middle',
			  offsetX: 0,
			  offsetY: -20,
			  style: {
			    color: undefined,
			    fontSize: '14px',
			    fontFamily: "Montserrat",
			  }
			}
	    },
	    series: this.props.series,
	}

	componentDidUpdate(prevProps) {
		if (prevProps.series !== this.props.series) {
			this.refresh();
		}
	}

	refresh = () => {
		this.setState({
			options: {
				...this.state.options,
				labels: this.props.labels
			},
			series: this.props.series
		});
	}

	information(){
		let res = [];
		const total = this.total();

		this.state.options.labels.forEach((o, ix) => {

			res = res.concat({
				name: o,
				value: this.state.series[ix],
				color: this.state.options.colors[ix],
				percent: Number.rate(this.state.series[ix], total),
			});
		});

		return res;
	}

	total(){
		let res = 0;
		this.state.series.forEach(o => {
			res += o;
		});

		return res;
	}

	render(){
		const information = this.information();

		return (
			<div className="pie-chart">
				<Chart
		            options={this.state.options}
		            series={this.state.series}
		            type="donut"
		            height={300}
		        />

		        {
		        	information.map((o, ix) =>
		        		<div className="chart-info d-flex justify-content-between" key={ix}>
		            		<div className="series-info d-flex align-items-center">
			            	</div>
			            	<div className="series-result">
			            		<Circle size={12} color={o.color}/>
				            	<span className="text-bold-600 mx-50">{o.name}</span>
				              	<span className="align-middle">{o.percent}%</span>
			            	</div>
			          	</div>
		        	)
		        }
			</div>
		);
	}
}

export default PieChart;