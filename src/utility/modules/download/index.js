import React from "react"
import { Button } from "reactstrap"
import { CSVDownload } from "react-csv"

class Download extends React.Component{
	state = {
		start: false,
	}

	render(){
		return (
			<div className="mw-csv-download pb-2">
				<p>You can download raw data by clicking the button below</p>
				<Button.Ripple style={{
					padding: 0,
					color: "#7367f0",
					textDecoration: "underline"
				}} color="" onClick={() => {
					this.setState({
						start: true
					})

					setTimeout(() => {
						this.setState({
							start: false
						})
					}, 1000)
				}}>Download raw data</Button.Ripple>

				{
					this.state.start
					&&
					<CSVDownload data={this.props.data} target="_blank" />
				}
			</div>
		)
	}
}

export default Download