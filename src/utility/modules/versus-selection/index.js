import React from "react"
import Selection from "../../../components/selection"
import { Plus, Minus } from "react-feather"

//CSS
import "./style.scss"

class VersusSelection extends React.Component{
	state = {
		expand: false,
	}

	render(){
		return (
			<div className="versus-selection">
				<Selection {...{
					...this.props.items[0],
				}}/>

				{
					this.state.expand
					?
					<span className="vs">vs.</span>
					:
					<button className="add" onClick={() => {
						this.setState({
							expand: true,
						})
					}}>
						<Plus size={20}/>
					</button>
				}

				{
					this.state.expand
					&&
					<>
						<Selection {...{
							...this.props.items[1],
						}}/>
						<button className="add" onClick={() => {
						this.setState({
							expand: false,
						})
					}}>
							<Minus size={20}/>
						</button>
					</>
				}
			</div>
		)
	}
}

export default VersusSelection