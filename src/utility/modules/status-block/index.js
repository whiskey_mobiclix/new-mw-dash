import React from "react"
import "./style.scss"

class StatusBlock extends React.Component{
	render(){
		const data = this.props.data

		return (
			<div className="mw-block">
				<div className="parent">{data.name}</div>

				<div style={{
					display: "flex"
				}}>
					{
						data.children.map((o, ix) => 
							<div className={`child ${o.children ? "has-child" : ""}`} key={ix}>
								<div className="c-parent">
									{o.name}
								</div>

								<div style={{
									display: "flex"
								}}>
								{
									o.children
									&&
									o.children.map((oo, ixx) => 
										<div key={ixx} className="c-child">
											{oo.name}
										</div>
									)
								}
								</div>
							</div>
						)
					}
				</div>
			</div>
		)
	}
}

export default StatusBlock