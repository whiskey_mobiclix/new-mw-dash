import React from "react"
import "./style.scss"

class Title extends React.Component{
	render(){
		return (
			<div className="page-title">
				{this.props.text}
			</div>
		)
	}
}

export default Title