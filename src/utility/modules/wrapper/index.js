import React from "react"
import { connect } from "react-redux"

class Wrapper extends React.Component{
	available = () => {
		if(this.props.group)
			return !!this.props.group.filter(o => o == this.props.user.data.group).length


		if(this.props.resource)
			return !!this.props.permissions.data.filter(o => o.resource == this.props.resource).length

		return true
	}

	render(){
		if(!this.available()) return null

		return (
			<React.Fragment>
				{this.props.children}
			</React.Fragment>
		)
	}
}

export default connect(
	state => ({
		permissions: state.permissions,
		user: state.user,
	}),
	{}
)(Wrapper)