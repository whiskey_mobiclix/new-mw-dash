import React from 'react';
import { ArrowUp, ArrowDown } from 'react-feather';
import { UncontrolledTooltip } from 'reactstrap';
import Number from '../../helpers/number';

//CSS
import './style.scss';

class Comparision extends React.Component {
	render() {
		const current = this.props.current || 0;
		const previous = this.props.previous || 0;
		const dis = previous === 0 ? 0 : ((current - previous) / previous) * 100;

		return (
			<div className="mw-comparision">
				<span>
					{this.props.prefix && (
						<span style={{ marginRight: 5 }}>{this.props.prefix}</span>
					)}
					{Number.format(current)}
					{this.props.unit && (
						<span style={{ marginLeft: 5 }}>{this.props.unit}</span>
					)}
				</span>
				<span className={`mw-cmp-prev ${dis < 0 ? 'down' : 'up'}`}>
					{dis < 0 ? <ArrowDown /> : <ArrowUp />}
					{`${Number.format(Math.round(Math.abs(dis) * 10) / 10)}%`}
					<span className="mw-cmp-tooltip">
						Previous period: {Number.format(previous)}
					</span>
				</span>
			</div>
		);
	}
}

export default Comparision;
