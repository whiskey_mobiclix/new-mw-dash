import React from "react"
import {Button} from "reactstrap"

//CSS
import "./style.scss"

class Tab extends React.Component{
	render(){
		return (
			<div className="mw-tab">
				{
					this.props.items.map((o, ix) =>
						<Button key={ix} className={`square ${this.props.active === o.value ? "active" : ""}`} onClick={() => {
							this.props.onChange && this.props.onChange(o)
						}}>{o.name}</Button>
					)
				}
			</div>
		)
	}
}

export default Tab