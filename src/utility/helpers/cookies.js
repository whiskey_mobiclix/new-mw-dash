export default {
    set: (name,value,secconds) => {
        if(typeof document !== "undefined"){
            return new Promise(resolve => {
                var expires = "";
                if (secconds) {
                    var date = new Date();
                    date.setTime(date.getTime() + secconds);
                    expires = "; expires=" + date.toUTCString();
                }
                document.cookie = name + "=" + (value || "")  + expires + "; path=/";

                setTimeout(() => {
                    resolve();
                }, 500);
            })
        }
    },

    get: name => {
        if(typeof document !== "undefined"){
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');

            for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)===' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) === 0) {
                    return c.substring(nameEQ.length,c.length);
                }
            }
        }

        return null;
    },

    delete: name => {
        if(typeof document !== "undefined"){
            return new Promise(resolve => {
                document.cookie = name+'=; Max-Age=-99999999;';

                setTimeout(() => {
                    resolve();
                }, 500);
            })  
        }
    }
}