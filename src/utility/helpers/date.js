import moment from "moment"

export default {
	toUnix: value => {
		return Math.round(value / 1000)
	},
	days: value => {
		if(typeof value !== "string"){
			let range = []
			for(let i=0; i<value; i++){
				range = range.concat(moment().add({days: -1 * i}).format("DD MMM YYYY"))
			}

			return range.reverse()
		} else {
			const splited = value.split("-");

			let start = moment(splited[0])
			let end = moment(splited[1])

			let days = [];
			let day = start;

			while (day <= end) {
			    days.push(day.toDate());
			    day = day.clone().add(1, 'd');
			}

			return days.map(o => moment(o).format("DD MMM YYYY"))
		}

		return []
	},
	months: value => {
		let range = []
		for(let i=0; i<value; i++){
			range = range.concat(moment().add({months: -1 * i}).format("MMM YYYY"))
		}

		return range.reverse()
	},
	range: value => {
		if(typeof value !== "string"){
			return {
				current: {
					from: moment(moment().add(-1 * value + 1, "d").format("DD MMM YYYY 00:00")).valueOf(),
					to: moment(moment().format("DD MMM YYYY 23:59")).valueOf(),
				},
				previous: {
					from: moment(moment().add(-1 * value * 2 + 1, "d").format("DD MMM YYYY 00:00")).valueOf(),
					to: moment(moment().add(-1 * value, "d").format("DD MMM YYYY 23:59")).valueOf(),
				}
			}
		} else {
			const splited = value.split("-")
			const from = moment(splited[0]), to = moment(splited[1])
			var duration = moment.duration(to.diff(from));
			var days = duration.asDays();

			return {
				current: {
					from: moment(from.format("DD MMM YYYY 00:00")).valueOf(),
					to: moment(to.format("DD MMM YYYY 23:59")).valueOf(),
				},
				previous: {
					from: moment(moment(from).add(-1 * days - 1, "d").format("DD MMM YYYY 00:00")).valueOf(),
					to: moment(moment(from).add(-1, "d").format("DD MMM YYYY 23:59")).valueOf(),
				}
			}
		}
	},
	rangeByMonths: value => {
		return {
			current: {
				from: moment(moment().add(-1 * value * 30 + 1, "d").format("DD MMM YYYY 00:00")).valueOf(),
				to: moment(moment().format("DD MMM YYYY 23:59")).valueOf(),
			},
			previous: {
				from: moment(moment().add(-1 * value * 30 * 2 + 1, "d").format("DD MMM YYYY 00:00")).valueOf(),
				to: moment(moment().add(-1 * value * 30, "d").format("DD MMM YYYY 23:59")).valueOf(),
			}
		}
	},
}