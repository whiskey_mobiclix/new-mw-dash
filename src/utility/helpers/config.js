import Country from "./country"
import String from "./string"
import Array from "./array"

export default {
	getAllProducts: data => {
		try{
			return Object.keys(data).map(o => ({
				name: String.capitalize(o.toLowerCase()),
				value: o.toLowerCase(),
			}))
		}catch(e){
			return []
		}
	},
	getMarketsByProduct: (data, product) => {
		if(!product){
			try{
				let res = []
				const products = Object.keys(data)
				products.forEach(o => {
					let countries = data[o].countries
					let arr = Object.keys(countries)
					res = res.concat(arr)
				})

				return Array.unique(res).map(o => ({
					name: Country.getCountryByCode(o),
					value: o,
				}))
			} catch(e){
				return []
			}
		}

		try{
			let countries = data[product.toUpperCase()].countries
			return Object.keys(countries).map(o => ({
				name: Country.getCountryByCode(o),
				value: o,
			}))

		}catch(e){
			return []
		}
	},
	getNNOsByProduct: (data, product) => {
		if(!product){
			try{
				let res = []
				const products = Object.keys(data)
				products.forEach(prod => {
					let mnos = []
					let countries = data[prod].countries
					Object.keys(countries).forEach(o => {
						countries[o].data.forEach(c => {
							let mno = c.mno;
							if(res.indexOf(mno) < 0){
								res = res.concat(mno)
							}
						})
					})	
				})

				return Array.unique(res).map(o => ({
					name: String.capitalize(o.toLowerCase()),
					value: o,
				}))
			}catch(e){
				return []
			}
			
		}

		try{
			let res = []
			let countries = data[product.toUpperCase()].countries
			Object.keys(countries).forEach(o => {
				countries[o].data.forEach(c => {
					let mno = c.mno;
					if(res.indexOf(mno) < 0){
						res = res.concat(mno)
					}
				})
			})


			return res.map(o => ({
				name: String.capitalize(o.toLowerCase()),
				value: o,
			}))
		}catch(e){
			return []
		}
	},
	groupByMarkets: (data, product) => {
		try{
			let _data = data[product.toUpperCase()].countries
			let countries = Object.keys(_data);
			countries = countries.map(o => ({
				name: Country.getCountryByCode(o) || "Others",
				children: Array.unique(_data[o].data.map(item => item.mno)).map(item => ({
					name: String.capitalize(item.toLowerCase()),
					children: _data[o].data.filter(d => d.mno == item).map(d => ({
						name: String.capitalize(d.aggregator.toLowerCase()),
					})),
				})),
			}))

			return countries
		}catch(e){
			return []
		}
	},
	groupByRegions: (data, product) => {
		try{
			let _data = data[product.toUpperCase()].countries

			let countries = Object.keys(_data)
			let regions = Country.getRegions()
			let res = Object.keys(regions).map(o => ({
				name: regions[o].value.trim(),
				children: Object.keys(regions[o].countries).filter(d => !!_data[d]).map(c => ({
					name: Country.getCountryByCode(c),
					children: _data[c] && Array.unique(_data[c].data.map(item => item.mno)).map(m => ({
						name: String.capitalize(m.toLowerCase())
					})),
				})),
			}))

			res = res.filter(o => o.children.length)

			return res
		}catch(e){
			console.log(e)
			return []
		}
	}
}