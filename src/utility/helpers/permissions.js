export default {
	USERS_STATS: "stats/users",
	REVENUES_STATS: "stats/revenues",
	TRANSACTIONS_STATS: "stats/transactions",
}