export default {
	capitalize: s => {
		if (typeof s !== 'string') return ""
  		return s.charAt(0).toUpperCase() + s.slice(1)
	},

	random: (length = 8) => {
		var result           = '';
	    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@';
	    var charactersLength = characters.length;
	    for ( var i = 0; i < length; i++ ) {
	       result += characters.charAt(Math.floor(Math.random() * charactersLength));
	    }
	    return result;
	}
}