export default {
	format: (s, decimal) => {
		s += '';
		let x = s.split('.');
		let x1 = x[0];
		let x2 = x.length > 1 ? '.' + x[1] : '';
		let rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}

		if (decimal) x2 = x2.slice(0, decimal + 1);

		return x1 + x2;
	},

	total: array => {
		let res = 0;
		try {
			array.forEach(o => {
				res += o;
			});
		} catch (e) {
			console.log(e);
		}

		return Math.round(res * 100) / 100;
	},

	rate: (a, b) => {
		if(b === 0) return 0

		return Math.round(a / b * 10000) / 100
	}
};
