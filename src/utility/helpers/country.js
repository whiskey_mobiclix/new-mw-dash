export const CountryToDialCodes = {
    "AF": "93",
    "AL": "355",
    "DZ": "213",
    "AS": "684",
    "AD": "376",
    "AO": "244",
    "AI": "264",
    "AQ": "672",
    "AG": "268",
    "AR": "54",
    "AM": "374",
    "AW": "297",
    "AU": "61",
    "AT": "43",
    "AZ": "994",
    "BS": "242",
    "BH": "973",
    "BD": "880",
    "BB": "246",
    "BY": "375",
    "BE": "32",
    "BZ": "501",
    "BJ": "229",
    "BM": "441",
    "BT": "975",
    "BO": "591",
    "BA": "387",
    "BW": "267",
    "BR": "55",
    "IO": "246",
    "VG": "284",
    "BN": "673",
    "BG": "359",
    "BF": "226",
    "BI": "257",
    "KH": "855",
    "CM": "237",
    "CA": "1",
    "CV": "238",
    "KY": "345",
    "CF": "236",
    "TD": "235",
    "CL": "56",
    "CN": "86",
    "CX": "61",
    "CC": "61",
    "CO": "57",
    "KM": "269",
    "CK": "682",
    "CR": "506",
    "HR": "385",
    "CU": "53",
    "CW": "599",
    "CY": "357",
    "CZ": "420",
    "CD": "243",
    "DK": "45",
    "DJ": "253",
    "DM": "767",
    "DO": "849",
    "TL": "670",
    "EC": "593",
    "EG": "20",
    "SV": "503",
    "GQ": "240",
    "ER": "291",
    "EE": "372",
    "ET": "251",
    "FK": "500",
    "FO": "298",
    "FJ": "679",
    "FI": "358",
    "FR": "33",
    "PF": "689",
    "GA": "241",
    "GM": "220",
    "GE": "995",
    "DE": "49",
    "GH": "233",
    "GI": "350",
    "GR": "30",
    "GL": "299",
    "GD": "473",
    "GU": "671",
    "GT": "502",
    "GG": "1481",
    "GN": "224",
    "GW": "245",
    "GY": "592",
    "HT": "509",
    "HN": "504",
    "HK": "852",
    "HU": "36",
    "IS": "354",
    "IN": "91",
    "ID": "62",
    "IR": "98",
    "IQ": "964",
    "IE": "353",
    "IM": "1624",
    "IL": "972",
    "IT": "39",
    "CI": "225",
    "JM": "876",
    "JP": "81",
    "JE": "1534",
    "JO": "962",
    "KZ": "7",
    "KE": "254",
    "KI": "686",
    "XK": "383",
    "KW": "965",
    "KG": "996",
    "LA": "856",
    "LV": "371",
    "LB": "961",
    "LS": "266",
    "LR": "231",
    "LY": "218",
    "LI": "423",
    "LT": "370",
    "LU": "352",
    "MO": "853",
    "MK": "389",
    "MG": "261",
    "MW": "265",
    "MY": "60",
    "MV": "960",
    "ML": "223",
    "MT": "356",
    "MH": "692",
    "MR": "222",
    "MU": "230",
    "YT": "262",
    "MX": "52",
    "FM": "691",
    "MD": "373",
    "MC": "377",
    "MN": "976",
    "ME": "382",
    "MS": "664",
    "MA": "212",
    "MZ": "258",
    "MM": "95",
    "NA": "264",
    "NR": "674",
    "NP": "977",
    "NL": "31",
    "AN": "599",
    "NC": "687",
    "NZ": "64",
    "NI": "505",
    "NE": "227",
    "NG": "234",
    "NU": "683",
    "KP": "850",
    "MP": "670",
    "NO": "47",
    "OM": "968",
    "PK": "92",
    "PW": "680",
    "PS": "970",
    "PA": "507",
    "PG": "675",
    "PY": "595",
    "PE": "51",
    "PH": "63",
    "PN": "64",
    "PL": "48",
    "PT": "351",
    "PR": "939",
    "QA": "974",
    "CG": "242",
    "RE": "262",
    "RO": "40",
    "RU": "7",
    "RW": "250",
    "BL": "590",
    "SH": "290",
    "KN": "869",
    "LC": "758",
    "MF": "590",
    "PM": "508",
    "VC": "784",
    "WS": "685",
    "SM": "378",
    "ST": "239",
    "SA": "966",
    "SN": "221",
    "RS": "381",
    "SC": "248",
    "SL": "232",
    "SG": "65",
    "SX": "721",
    "SK": "421",
    "SI": "386",
    "SB": "677",
    "SO": "252",
    "ZA": "27",
    "KR": "82",
    "SS": "211",
    "ES": "34",
    "LK": "94",
    "SD": "249",
    "SR": "597",
    "SJ": "47",
    "SZ": "268",
    "SE": "46",
    "CH": "41",
    "SY": "963",
    "TW": "886",
    "TJ": "992",
    "TZ": "255",
    "TH": "66",
    "TG": "228",
    "TK": "690",
    "TO": "676",
    "TT": "868",
    "TN": "216",
    "TR": "90",
    "TM": "993",
    "TC": "649",
    "TV": "688",
    "VI": "340",
    "UG": "256",
    "UA": "380",
    "AE": "971",
    "GB": "44",
    "US": "1",
    "UY": "598",
    "UZ": "998",
    "VU": "678",
    "VA": "379",
    "VE": "58",
    "VN": "84",
    "WF": "681",
    "EH": "212",
    "YE": "967",
    "ZM": "260",
    "ZW": "263",
};

export const CountryCodes = {
    "AF": "Afghanistan",
    "AL": "Albania",
    "DZ": "Algeria",
    "AS": "American Samoa",
    "AD": "Andorra",
    "AO": "Angola",
    "AI": "Anguilla",
    "AG": "Antigua and Barbuda",
    "AR": "Argentina",
    "AM": "Armenia",
    "AW": "Aruba",
    "AU": "Australia",
    "AT": "Austria",
    "AZ": "Azerbaijan",
    "BS": "Bahamas",
    "BD": "Bangladesh",
    "BB": "Barbados",
    "BY": "Belarus",
    "BE": "Belgium",
    "BZ": "Belize",
    "BJ": "Benin",
    "BM": "Bermuda",
    "BT": "Bhutan",
    "BO": "Bolivia, Plurinational State of",
    "BA": "Bosnia and Herzegovina",
    "BW": "Botswana",
    "BR": "Brazil",
    "IO": "British Indian Ocean Territory",
    "BG": "Bulgaria",
    "BF": "Burkina Faso",
    "BI": "Burundi",
    "KH": "Cambodia",
    "CM": "Cameroon",
    "CA": "Canada",
    "CV": "Cape Verde",
    "KY": "Cayman Islands",
    "CF": "Central African Republic",
    "TD": "Chad",
    "CL": "Chile",
    "CN": "China",
    "CO": "Colombia",
    "KM": "Comoros",
    "CG": "Congo",
    "CD": "Democratic Republic of the Congo",
    "CK": "Cook Islands",
    "CR": "Costa Rica",
    "CI": "Côte d'Ivoire",
    "HR": "Croatia",
    "CU": "Cuba",
    "CW": "Curaçao",
    "CY": "Cyprus",
    "CZ": "Czech Republic",
    "DK": "Denmark",
    "DJ": "Djibouti",
    "DM": "Dominica",
    "DO": "Dominican Republic",
    "EC": "Ecuador",
    "EG": "Egypt",
    "SV": "El Salvador",
    "GQ": "Equatorial Guinea",
    "ER": "Eritrea",
    "EE": "Estonia",
    "ET": "Ethiopia",
    "FK": "Falkland Islands (Malvinas)",
    "FO": "Faroe Islands",
    "FJ": "Fiji",
    "FI": "Finland",
    "FR": "France",
    "PF": "French Polynesia",
    "GA": "Gabon",
    "GM": "Gambia",
    "GE": "Georgia",
    "DE": "Germany",
    "GH": "Ghana",
    "GI": "Gibraltar",
    "GR": "Greece",
    "GL": "Greenland",
    "GD": "Grenada",
    "GU": "Guam",
    "GT": "Guatemala",
    "GG": "Guernsey",
    "GN": "Guinea",
    "GW": "Guinea-Bissau",
    "HT": "Haiti",
    "HN": "Honduras",
    "HK": "Hong Kong",
    "HU": "Hungary",
    "IS": "Iceland",
    "IN": "India",
    "ID": "Indonesia",
    "IR": "Iran, Islamic Republic of",
    "IQ": "Iraq",
    "IE": "Ireland",
    "IM": "Isle of Man",
    "IL": "Israel",
    "IT": "Italy",
    "JM": "Jamaica",
    "JP": "Japan",
    "JE": "Jersey",
    "JO": "Jordan",
    "KZ": "Kazakhstan",
    "KE": "Kenya",
    "KI": "Kiribati",
    "KP": "North Korea",
    "KR": "South Korea",
    "KW": "Kuwait",
    "KG": "Kyrgyzstan",
    "LA": "Lao People's Democratic Republic",
    "LV": "Latvia",
    "LB": "Lebanon",
    "LS": "Lesotho",
    "LR": "Liberia",
    "LY": "Libya",
    "LI": "Liechtenstein",
    "LT": "Lithuania",
    "LU": "Luxembourg",
    "MO": "Macao",
    "MK": "Republic of Macedonia",
    "MG": "Madagascar",
    "MW": "Malawi",
    "MY": "Malaysia",
    "MV": "Maldives",
    "ML": "Mali",
    "MT": "Malta",
    "MH": "Marshall Islands",
    "MQ": "Martinique",
    "MR": "Mauritania",
    "MU": "Mauritius",
    "MX": "Mexico",
    "FM": "Micronesia, Federated States of",
    "MD": "Republic of Moldova",
    "MC": "Monaco",
    "MN": "Mongolia",
    "ME": "Montenegro",
    "MS": "Montserrat",
    "MA": "Morocco",
    "MZ": "Mozambique",
    "MM": "Myanmar",
    "NA": "Namibia",
    "NR": "Nauru",
    "NP": "Nepal",
    "NL": "Netherlands",
    "NZ": "New Zealand",
    "NI": "Nicaragua",
    "NE": "Niger",
    "NG": "Nigeria",
    "NU": "Niue",
    "NF": "Norfolk Island",
    "MP": "Northern Mariana Islands",
    "NO": "Norway",
    "OM": "Oman",
    "PK": "Pakistan",
    "PW": "Palau",
    "PS": "Palestinian Territory",
    "PA": "Panama",
    "PG": "Papua New Guinea",
    "PY": "Paraguay",
    "PE": "Peru",
    "PH": "Philippines",
    "PN": "Pitcairn",
    "PL": "Poland",
    "PT": "Portugal",
    "PR": "Puerto Rico",
    "QA": "Qatar",
    "RO": "Romania",
    "RU": "Russian",
    "RW": "Rwanda",
    "KN": "Saint Kitts and Nevis",
    "WS": "Samoa",
    "SM": "San Marino",
    "ST": "Sao Tome and Principe",
    "SA": "Saudi Arabia",
    "SN": "Senegal",
    "RS": "Serbia",
    "SC": "Seychelles",
    "SL": "Sierra Leone",
    "SG": "Singapore",
    "SX": "Sint Maarten",
    "SK": "Slovakia",
    "SI": "Slovenia",
    "SB": "Solomon Islands",
    "SO": "Somalia",
    "ZA": "South Africa",
    "SS": "South Sudan",
    "ES": "Spain",
    "LK": "Sri Lanka",
    "SD": "Sudan",
    "SR": "Suriname",
    "SZ": "Swaziland",
    "SE": "Sweden",
    "CH": "Switzerland",
    "SY": "Syria",
    "TW": "Taiwan, Province of China",
    "TJ": "Tajikistan",
    "TZ": "Tanzania",
    "TH": "Thailand",
    "TG": "Togo",
    "TK": "Tokelau",
    "TO": "Tonga",
    "TT": "Trinidad and Tobago",
    "TN": "Tunisia",
    "TR": "Turkey",
    "TM": "Turkmenistan",
    "TC": "Turks and Caicos Islands",
    "TV": "Tuvalu",
    "UG": "Uganda",
    "UA": "Ukraine",
    "AE": "United Arab Emirates",
    "GB": "United Kingdom",
    "US": "United States",
    "UY": "Uruguay",
    "UZ": "Uzbekistan",
    "VU": "Vanuatu",
    "VE": "Venezuela, Bolivarian Republic of",
    "VN": "Viet Nam",
    "VI": "Virgin Islands",
    "YE": "Yemen",
    "ZM": "Zambia",
    "ZW": "Zimbabwe",
    "UK": "United Kingdom"
};

export const  CountryToLanguage = {
    AD: "ca",
    AE: "ar",
    AF: "fa",
    AG: "en",
    AI: "en",
    AL: "sq",
    AM: "hy",
    AO: "pt",
    AQ: "en",
    AR: "es",
    AS: "en",
    AT: "de",
    AU: "en",
    AW: "nl",
    AX: "sv",
    AZ: "az",
    BA: "bs",
    BB: "en",
    BD: "bn",
    BE: "nl",
    BF: "fr",
    BG: "bg",
    BH: "ar",
    BI: "fr",
    BJ: "fr",
    BL: "fr",
    BM: "en",
    BN: "ms",
    BO: "es",
    BQ: "nl",
    BR: "pt",
    BS: "en",
    BT: "dz",
    BV: "no",
    BW: "en",
    BY: "be",
    BZ: "en",
    CA: "en",
    CC: "en",
    CD: "fr",
    CF: "fr",
    CG: "fr",
    CH: "de",
    CI: "fr",
    CK: "en",
    CL: "es",
    CM: "fr",
    CN: "zh",
    CO: "es",
    CR: "es",
    CU: "es",
    CV: "pt",
    CW: "nl",
    CX: "en",
    CY: "el",
    CZ: "cs",
    DE: "de",
    DJ: "fr",
    DK: "da",
    DM: "en",
    DO: "es",
    DZ: "ar",
    EC: "es",
    EE: "et",
    EG: "ar",
    EH: "ar",
    ER: "ti",
    ES: "es",
    ET: "am",
    FI: "fi",
    FJ: "en",
    FK: "en",
    FM: "en",
    FO: "fo",
    FR: "fr",
    GA: "fr",
    GB: "en",
    GD: "en",
    GE: "ka",
    GF: "fr",
    GG: "en",
    GH: "en",
    GI: "en",
    GL: "kl",
    GM: "en",
    GN: "fr",
    GP: "fr",
    GQ: "es",
    GR: "el",
    GS: "en",
    GT: "es",
    GU: "en",
    GW: "pt",
    GY: "en",
    HK: "zh",
    HM: "en",
    HN: "es",
    HR: "hr",
    HT: "fr",
    HU: "hu",
    ID: "id",
    IE: "en",
    IL: "he",
    IM: "en",
    IN: "hi",
    IO: "en",
    IQ: "ar",
    IR: "fa",
    IS: "is",
    IT: "it",
    JE: "en",
    JM: "en",
    JO: "ar",
    JP: "ja",
    KE: "sw",
    KG: "ky",
    KH: "km",
    KI: "en",
    KM: "ar",
    KN: "en",
    KP: "ko",
    KR: "ko",
    KW: "ar",
    KY: "en",
    KZ: "kk",
    LA: "lo",
    LB: "ar",
    LC: "en",
    LI: "de",
    LK: "si",
    LR: "en",
    LS: "en",
    LT: "lt",
    LU: "lb",
    LV: "lv",
    LY: "ar",
    MA: "fr",
    MC: "fr",
    MD: "ro",
    ME: "srp",
    MF: "fr",
    MG: "mg",
    MH: "en",
    MK: "mk",
    ML: "fr",
    MM: "my",
    MMU: "myu",
    MN: "mn",
    MO: "zh",
    MP: "en",
    MQ: "fr",
    MR: "ar",
    MS: "en",
    MT: "mt",
    MU: "mfe",
    MV: "dv",
    MW: "en",
    MX: "es",
    MY: "ms",
    MZ: "pt",
    NA: "en",
    NC: "fr",
    NE: "fr",
    NF: "en",
    NG: "en",
    NI: "es",
    NL: "nl",
    NO: "nb",
    NP: "ne",
    NR: "na",
    NU: "niu",
    NZ: "mi",
    OM: "ar",
    PA: "es",
    PE: "es",
    PF: "fr",
    PG: "en",
    PH: "en",
    PK: "en",
    PL: "pl",
    PM: "fr",
    PN: "en",
    PR: "es",
    PS: "ar",
    PT: "pt",
    PW: "en",
    PY: "es",
    QA: "ar",
    RE: "fr",
    RO: "ro",
    RS: "sr",
    RU: "ru",
    RW: "rw",
    SA: "ar",
    SB: "en",
    SC: "fr",
    SD: "ar",
    SE: "sv",
    SG: "zh",
    SH: "en",
    SI: "sl",
    SJ: "no",
    SK: "sk",
    SL: "en",
    SM: "it",
    SN: "fr",
    SO: "so",
    SR: "nl",
    ST: "pt",
    SS: "en",
    SV: "es",
    SX: "nl",
    SY: "ar",
    SZ: "en",
    TC: "en",
    TD: "fr",
    TF: "fr",
    TG: "fr",
    TH: "th",
    TJ: "tg",
    TK: "tkl",
    TL: "pt",
    TM: "tk",
    TN: "ar",
    TO: "en",
    TR: "tr",
    TT: "en",
    TV: "en",
    TW: "zh",
    TZ: "sw",
    UA: "uk",
    UG: "en",
    UM: "en",
    US: "en",
    UY: "es",
    UZ: "uz",
    VA: "it",
    VC: "en",
    VE: "es",
    VG: "en",
    VI: "en",
    VN: "vi",
    VU: "bi",
    WF: "fr",
    WS: "sm",
    YE: "ar",
    YT: "fr",
    ZA: "en",
    ZM: "en",
    ZW: "en",
};

export const Regions = {
  "ZJ": {
    "value": "Latin America & Caribbean ",
    "id": "LCN",
    "iso2code": "ZJ",
    "countries": {
      "AW": {
        "id": "ABW",
        "name": "Aruba",
        "iso2code": "AW"
      },
      "AR": {
        "id": "ARG",
        "name": "Argentina",
        "iso2code": "AR"
      },
      "AG": {
        "id": "ATG",
        "name": "Antigua and Barbuda",
        "iso2code": "AG"
      },
      "BS": {
        "id": "BHS",
        "name": "Bahamas, The",
        "iso2code": "BS"
      },
      "BZ": {
        "id": "BLZ",
        "name": "Belize",
        "iso2code": "BZ"
      },
      "BO": {
        "id": "BOL",
        "name": "Bolivia",
        "iso2code": "BO"
      },
      "BR": {
        "id": "BRA",
        "name": "Brazil",
        "iso2code": "BR"
      },
      "BB": {
        "id": "BRB",
        "name": "Barbados",
        "iso2code": "BB"
      },
      "CL": {
        "id": "CHL",
        "name": "Chile",
        "iso2code": "CL"
      },
      "CO": {
        "id": "COL",
        "name": "Colombia",
        "iso2code": "CO"
      },
      "CR": {
        "id": "CRI",
        "name": "Costa Rica",
        "iso2code": "CR"
      },
      "CU": {
        "id": "CUB",
        "name": "Cuba",
        "iso2code": "CU"
      },
      "CW": {
        "id": "CUW",
        "name": "Curacao",
        "iso2code": "CW"
      },
      "KY": {
        "id": "CYM",
        "name": "Cayman Islands",
        "iso2code": "KY"
      },
      "DM": {
        "id": "DMA",
        "name": "Dominica",
        "iso2code": "DM"
      },
      "DO": {
        "id": "DOM",
        "name": "Dominican Republic",
        "iso2code": "DO"
      },
      "EC": {
        "id": "ECU",
        "name": "Ecuador",
        "iso2code": "EC"
      },
      "KN": {
        "id": "KNA",
        "name": "St. Kitts and Nevis",
        "iso2code": "KN"
      },
      "LC": {
        "id": "LCA",
        "name": "St. Lucia",
        "iso2code": "LC"
      },
      "MF": {
        "id": "MAF",
        "name": "St. Martin (French part)",
        "iso2code": "MF"
      },
      "MX": {
        "id": "MEX",
        "name": "Mexico",
        "iso2code": "MX"
      },
      "GD": {
        "id": "GRD",
        "name": "Grenada",
        "iso2code": "GD"
      },
      "GT": {
        "id": "GTM",
        "name": "Guatemala",
        "iso2code": "GT"
      },
      "GY": {
        "id": "GUY",
        "name": "Guyana",
        "iso2code": "GY"
      },
      "HN": {
        "id": "HND",
        "name": "Honduras",
        "iso2code": "HN"
      },
      "HT": {
        "id": "HTI",
        "name": "Haiti",
        "iso2code": "HT"
      },
      "JM": {
        "id": "JAM",
        "name": "Jamaica",
        "iso2code": "JM"
      },
      "NI": {
        "id": "NIC",
        "name": "Nicaragua",
        "iso2code": "NI"
      },
      "PA": {
        "id": "PAN",
        "name": "Panama",
        "iso2code": "PA"
      },
      "PE": {
        "id": "PER",
        "name": "Peru",
        "iso2code": "PE"
      },
      "PR": {
        "id": "PRI",
        "name": "Puerto Rico",
        "iso2code": "PR"
      },
      "PY": {
        "id": "PRY",
        "name": "Paraguay",
        "iso2code": "PY"
      },
      "SV": {
        "id": "SLV",
        "name": "El Salvador",
        "iso2code": "SV"
      },
      "SR": {
        "id": "SUR",
        "name": "Suriname",
        "iso2code": "SR"
      },
      "SX": {
        "id": "SXM",
        "name": "Sint Maarten (Dutch part)",
        "iso2code": "SX"
      },
      "TC": {
        "id": "TCA",
        "name": "Turks and Caicos Islands",
        "iso2code": "TC"
      },
      "TT": {
        "id": "TTO",
        "name": "Trinidad and Tobago",
        "iso2code": "TT"
      },
      "UY": {
        "id": "URY",
        "name": "Uruguay",
        "iso2code": "UY"
      },
      "VC": {
        "id": "VCT",
        "name": "St. Vincent and the Grenadines",
        "iso2code": "VC"
      },
      "VE": {
        "id": "VEN",
        "name": "Venezuela, RB",
        "iso2code": "VE"
      },
      "VG": {
        "id": "VGB",
        "name": "British Virgin Islands",
        "iso2code": "VG"
      },
      "VI": {
        "id": "VIR",
        "name": "Virgin Islands (U.S.)",
        "iso2code": "VI"
      }
    }
  },
  "8S": {
    "value": "South Asia",
    "id": "SAS",
    "iso2code": "8S",
    "countries": {
      "AF": {
        "id": "AFG",
        "name": "Afghanistan",
        "iso2code": "AF"
      },
      "BD": {
        "id": "BGD",
        "name": "Bangladesh",
        "iso2code": "BD"
      },
      "BT": {
        "id": "BTN",
        "name": "Bhutan",
        "iso2code": "BT"
      },
      "LK": {
        "id": "LKA",
        "name": "Sri Lanka",
        "iso2code": "LK"
      },
      "MV": {
        "id": "MDV",
        "name": "Maldives",
        "iso2code": "MV"
      },
      "IN": {
        "id": "IND",
        "name": "India",
        "iso2code": "IN"
      },
      "NP": {
        "id": "NPL",
        "name": "Nepal",
        "iso2code": "NP"
      },
      "PK": {
        "id": "PAK",
        "name": "Pakistan",
        "iso2code": "PK"
      }
    }
  },
  "NA": {
    "value": "Aggregates",
    "id": "NA",
    "iso2code": "NA",
    "countries": {
      "A9": {
        "id": "AFR",
        "name": "Africa",
        "iso2code": "A9"
      },
      "L5": {
        "id": "ANR",
        "name": "Andean Region",
        "iso2code": "L5"
      },
      "1A": {
        "id": "ARB",
        "name": "Arab World",
        "iso2code": "1A"
      },
      "B4": {
        "id": "BEA",
        "name": "East Asia & Pacific (IBRD-only countries)",
        "iso2code": "B4"
      },
      "B7": {
        "id": "BEC",
        "name": "Europe & Central Asia (IBRD-only countries)",
        "iso2code": "B7"
      },
      "B1": {
        "id": "BHI",
        "name": "IBRD countries classified as high income",
        "iso2code": "B1"
      },
      "B2": {
        "id": "BLA",
        "name": "Latin America & the Caribbean (IBRD-only countries)",
        "iso2code": "B2"
      },
      "B3": {
        "id": "BMN",
        "name": "Middle East & North Africa (IBRD-only countries)",
        "iso2code": "B3"
      },
      "B6": {
        "id": "BSS",
        "name": "Sub-Saharan Africa (IBRD-only countries)",
        "iso2code": "B6"
      },
      "C9": {
        "id": "CAA",
        "name": "Sub-Saharan Africa (IFC classification)",
        "iso2code": "C9"
      },
      "C4": {
        "id": "CEA",
        "name": "East Asia and the Pacific (IFC classification)",
        "iso2code": "C4"
      },
      "B8": {
        "id": "CEB",
        "name": "Central Europe and the Baltics",
        "iso2code": "B8"
      },
      "C5": {
        "id": "CEU",
        "name": "Europe and Central Asia (IFC classification)",
        "iso2code": "C5"
      },
      "C6": {
        "id": "CLA",
        "name": "Latin America and the Caribbean (IFC classification)",
        "iso2code": "C6"
      },
      "C7": {
        "id": "CME",
        "name": "Middle East and North Africa (IFC classification)",
        "iso2code": "C7"
      },
      "C8": {
        "id": "CSA",
        "name": "South Asia (IFC classification)",
        "iso2code": "C8"
      },
      "S3": {
        "id": "CSS",
        "name": "Caribbean small states",
        "iso2code": "S3"
      },
      "D4": {
        "id": "DEA",
        "name": "East Asia & Pacific (IDA-eligible countries)",
        "iso2code": "D4"
      },
      "D7": {
        "id": "DEC",
        "name": "Europe & Central Asia (IDA-eligible countries)",
        "iso2code": "D7"
      },
      "D8": {
        "id": "DFS",
        "name": "IDA countries classified as Fragile Situations",
        "iso2code": "D8"
      },
      "D2": {
        "id": "DLA",
        "name": "Latin America & the Caribbean (IDA-eligible countries)",
        "iso2code": "D2"
      },
      "D3": {
        "id": "DMN",
        "name": "Middle East & North Africa (IDA-eligible countries)",
        "iso2code": "D3"
      },
      "D9": {
        "id": "DNF",
        "name": "IDA countries not classified as Fragile Situations",
        "iso2code": "D9"
      },
      "N6": {
        "id": "DNS",
        "name": "IDA countries in Sub-Saharan Africa not classified as fragile situations ",
        "iso2code": "N6"
      },
      "D5": {
        "id": "DSA",
        "name": "South Asia (IDA-eligible countries)",
        "iso2code": "D5"
      },
      "F6": {
        "id": "DSF",
        "name": "IDA countries in Sub-Saharan Africa classified as fragile situations ",
        "iso2code": "F6"
      },
      "D6": {
        "id": "DSS",
        "name": "Sub-Saharan Africa (IDA-eligible countries)",
        "iso2code": "D6"
      },
      "6D": {
        "id": "DXS",
        "name": "IDA total, excluding Sub-Saharan Africa",
        "iso2code": "6D"
      },
      "4E": {
        "id": "EAP",
        "name": "East Asia & Pacific (excluding high income)",
        "iso2code": "4E"
      },
      "V2": {
        "id": "EAR",
        "name": "Early-demographic dividend",
        "iso2code": "V2"
      },
      "Z4": {
        "id": "EAS",
        "name": "East Asia & Pacific",
        "iso2code": "Z4"
      },
      "7E": {
        "id": "ECA",
        "name": "Europe & Central Asia (excluding high income)",
        "iso2code": "7E"
      },
      "Z7": {
        "id": "ECS",
        "name": "Europe & Central Asia",
        "iso2code": "Z7"
      },
      "XC": {
        "id": "EMU",
        "name": "Euro area",
        "iso2code": "XC"
      },
      "EU": {
        "id": "EUU",
        "name": "European Union",
        "iso2code": "EU"
      },
      "F1": {
        "id": "FCS",
        "name": "Fragile and conflict affected situations",
        "iso2code": "F1"
      },
      "XJ": {
        "id": "LAC",
        "name": "Latin America & Caribbean (excluding high income)",
        "iso2code": "XJ"
      },
      "ZJ": {
        "id": "LCN",
        "name": "Latin America & Caribbean ",
        "iso2code": "ZJ"
      },
      "L4": {
        "id": "LCR",
        "name": "Latin America and the Caribbean",
        "iso2code": "L4"
      },
      "XL": {
        "id": "LDC",
        "name": "Least developed countries: UN classification",
        "iso2code": "XL"
      },
      "XM": {
        "id": "LIC",
        "name": "Low income",
        "iso2code": "XM"
      },
      "XN": {
        "id": "LMC",
        "name": "Lower middle income",
        "iso2code": "XN"
      },
      "XO": {
        "id": "LMY",
        "name": "Low & middle income",
        "iso2code": "XO"
      },
      "V3": {
        "id": "LTE",
        "name": "Late-demographic dividend",
        "iso2code": "V3"
      },
      "L6": {
        "id": "MCA",
        "name": "Central America",
        "iso2code": "L6"
      },
      "M1": {
        "id": "MDE",
        "name": "Middle East (developing only)",
        "iso2code": "M1"
      },
      "ZQ": {
        "id": "MEA",
        "name": "Middle East & North Africa",
        "iso2code": "ZQ"
      },
      "XP": {
        "id": "MIC",
        "name": "Middle income",
        "iso2code": "XP"
      },
      "XQ": {
        "id": "MNA",
        "name": "Middle East & North Africa (excluding high income)",
        "iso2code": "XQ"
      },
      "XU": {
        "id": "NAC",
        "name": "North America",
        "iso2code": "XU"
      },
      "M2": {
        "id": "NAF",
        "name": "North Africa",
        "iso2code": "M2"
      },
      "6F": {
        "id": "FXS",
        "name": "IDA countries classified as fragile situations, excluding Sub-Saharan Africa",
        "iso2code": "6F"
      },
      "XD": {
        "id": "HIC",
        "name": "High income",
        "iso2code": "XD"
      },
      "XE": {
        "id": "HPC",
        "name": "Heavily indebted poor countries (HIPC)",
        "iso2code": "XE"
      },
      "ZB": {
        "id": "IBB",
        "name": "IBRD, including blend",
        "iso2code": "ZB"
      },
      "XF": {
        "id": "IBD",
        "name": "IBRD only",
        "iso2code": "XF"
      },
      "ZT": {
        "id": "IBT",
        "name": "IDA & IBRD total",
        "iso2code": "ZT"
      },
      "XG": {
        "id": "IDA",
        "name": "IDA total",
        "iso2code": "XG"
      },
      "XH": {
        "id": "IDB",
        "name": "IDA blend",
        "iso2code": "XH"
      },
      "XI": {
        "id": "IDX",
        "name": "IDA only",
        "iso2code": "XI"
      },
      "XY": {
        "id": "INX",
        "name": "Not classified",
        "iso2code": "XY"
      },
      "6L": {
        "id": "NLS",
        "name": "Non-resource rich Sub-Saharan Africa countries, of which landlocked",
        "iso2code": "6L"
      },
      "6X": {
        "id": "NRS",
        "name": "Non-resource rich Sub-Saharan Africa countries",
        "iso2code": "6X"
      },
      "6N": {
        "id": "NXS",
        "name": "IDA countries not classified as fragile situations, excluding Sub-Saharan Africa",
        "iso2code": "6N"
      },
      "OE": {
        "id": "OED",
        "name": "OECD members",
        "iso2code": "OE"
      },
      "S4": {
        "id": "OSS",
        "name": "Other small states",
        "iso2code": "S4"
      },
      "V1": {
        "id": "PRE",
        "name": "Pre-demographic dividend",
        "iso2code": "V1"
      },
      "S2": {
        "id": "PSS",
        "name": "Pacific island small states",
        "iso2code": "S2"
      },
      "V4": {
        "id": "PST",
        "name": "Post-demographic dividend",
        "iso2code": "V4"
      },
      "R6": {
        "id": "RRS",
        "name": "Resource rich Sub-Saharan Africa countries",
        "iso2code": "R6"
      },
      "O6": {
        "id": "RSO",
        "name": "Resource rich Sub-Saharan Africa countries, of which oil exporters",
        "iso2code": "O6"
      },
      "8S": {
        "id": "SAS",
        "name": "South Asia",
        "iso2code": "8S"
      },
      "L7": {
        "id": "SCE",
        "name": "Southern Cone",
        "iso2code": "L7"
      },
      "ZF": {
        "id": "SSA",
        "name": "Sub-Saharan Africa (excluding high income)",
        "iso2code": "ZF"
      },
      "ZG": {
        "id": "SSF",
        "name": "Sub-Saharan Africa ",
        "iso2code": "ZG"
      },
      "S1": {
        "id": "SST",
        "name": "Small states",
        "iso2code": "S1"
      },
      "A4": {
        "id": "SXZ",
        "name": "Sub-Saharan Africa excluding South Africa",
        "iso2code": "A4"
      },
      "T4": {
        "id": "TEA",
        "name": "East Asia & Pacific (IDA & IBRD countries)",
        "iso2code": "T4"
      },
      "T7": {
        "id": "TEC",
        "name": "Europe & Central Asia (IDA & IBRD countries)",
        "iso2code": "T7"
      },
      "T2": {
        "id": "TLA",
        "name": "Latin America & the Caribbean (IDA & IBRD countries)",
        "iso2code": "T2"
      },
      "T3": {
        "id": "TMN",
        "name": "Middle East & North Africa (IDA & IBRD countries)",
        "iso2code": "T3"
      },
      "T5": {
        "id": "TSA",
        "name": "South Asia (IDA & IBRD)",
        "iso2code": "T5"
      },
      "T6": {
        "id": "TSS",
        "name": "Sub-Saharan Africa (IDA & IBRD countries)",
        "iso2code": "T6"
      },
      "XT": {
        "id": "UMC",
        "name": "Upper middle income",
        "iso2code": "XT"
      },
      "1W": {
        "id": "WLD",
        "name": "World",
        "iso2code": "1W"
      },
      "A5": {
        "id": "XZN",
        "name": "Sub-Saharan Africa excluding South Africa and Nigeria",
        "iso2code": "A5"
      }
    }
  },
  "ZG": {
    "value": "Sub-Saharan Africa ",
    "id": "SSF",
    "iso2code": "ZG",
    "countries": {
      "AO": {
        "id": "AGO",
        "name": "Angola",
        "iso2code": "AO"
      },
      "BI": {
        "id": "BDI",
        "name": "Burundi",
        "iso2code": "BI"
      },
      "BJ": {
        "id": "BEN",
        "name": "Benin",
        "iso2code": "BJ"
      },
      "BF": {
        "id": "BFA",
        "name": "Burkina Faso",
        "iso2code": "BF"
      },
      "BW": {
        "id": "BWA",
        "name": "Botswana",
        "iso2code": "BW"
      },
      "CF": {
        "id": "CAF",
        "name": "Central African Republic",
        "iso2code": "CF"
      },
      "CI": {
        "id": "CIV",
        "name": "Cote d'Ivoire",
        "iso2code": "CI"
      },
      "CM": {
        "id": "CMR",
        "name": "Cameroon",
        "iso2code": "CM"
      },
      "CD": {
        "id": "COD",
        "name": "Congo, Dem. Rep.",
        "iso2code": "CD"
      },
      "CG": {
        "id": "COG",
        "name": "Congo, Rep.",
        "iso2code": "CG"
      },
      "KM": {
        "id": "COM",
        "name": "Comoros",
        "iso2code": "KM"
      },
      "CV": {
        "id": "CPV",
        "name": "Cabo Verde",
        "iso2code": "CV"
      },
      "ER": {
        "id": "ERI",
        "name": "Eritrea",
        "iso2code": "ER"
      },
      "ET": {
        "id": "ETH",
        "name": "Ethiopia",
        "iso2code": "ET"
      },
      "LR": {
        "id": "LBR",
        "name": "Liberia",
        "iso2code": "LR"
      },
      "LS": {
        "id": "LSO",
        "name": "Lesotho",
        "iso2code": "LS"
      },
      "MG": {
        "id": "MDG",
        "name": "Madagascar",
        "iso2code": "MG"
      },
      "ML": {
        "id": "MLI",
        "name": "Mali",
        "iso2code": "ML"
      },
      "MZ": {
        "id": "MOZ",
        "name": "Mozambique",
        "iso2code": "MZ"
      },
      "MR": {
        "id": "MRT",
        "name": "Mauritania",
        "iso2code": "MR"
      },
      "MU": {
        "id": "MUS",
        "name": "Mauritius",
        "iso2code": "MU"
      },
      "MW": {
        "id": "MWI",
        "name": "Malawi",
        "iso2code": "MW"
      },
      "GA": {
        "id": "GAB",
        "name": "Gabon",
        "iso2code": "GA"
      },
      "GH": {
        "id": "GHA",
        "name": "Ghana",
        "iso2code": "GH"
      },
      "GN": {
        "id": "GIN",
        "name": "Guinea",
        "iso2code": "GN"
      },
      "GM": {
        "id": "GMB",
        "name": "Gambia, The",
        "iso2code": "GM"
      },
      "GW": {
        "id": "GNB",
        "name": "Guinea-Bissau",
        "iso2code": "GW"
      },
      "GQ": {
        "id": "GNQ",
        "name": "Equatorial Guinea",
        "iso2code": "GQ"
      },
      "KE": {
        "id": "KEN",
        "name": "Kenya",
        "iso2code": "KE"
      },
      "NA": {
        "id": "NAM",
        "name": "Namibia",
        "iso2code": "NA"
      },
      "NE": {
        "id": "NER",
        "name": "Niger",
        "iso2code": "NE"
      },
      "NG": {
        "id": "NGA",
        "name": "Nigeria",
        "iso2code": "NG"
      },
      "RW": {
        "id": "RWA",
        "name": "Rwanda",
        "iso2code": "RW"
      },
      "SD": {
        "id": "SDN",
        "name": "Sudan",
        "iso2code": "SD"
      },
      "SN": {
        "id": "SEN",
        "name": "Senegal",
        "iso2code": "SN"
      },
      "SL": {
        "id": "SLE",
        "name": "Sierra Leone",
        "iso2code": "SL"
      },
      "SO": {
        "id": "SOM",
        "name": "Somalia",
        "iso2code": "SO"
      },
      "SS": {
        "id": "SSD",
        "name": "South Sudan",
        "iso2code": "SS"
      },
      "ST": {
        "id": "STP",
        "name": "Sao Tome and Principe",
        "iso2code": "ST"
      },
      "SZ": {
        "id": "SWZ",
        "name": "Eswatini",
        "iso2code": "SZ"
      },
      "SC": {
        "id": "SYC",
        "name": "Seychelles",
        "iso2code": "SC"
      },
      "TD": {
        "id": "TCD",
        "name": "Chad",
        "iso2code": "TD"
      },
      "TG": {
        "id": "TGO",
        "name": "Togo",
        "iso2code": "TG"
      },
      "TZ": {
        "id": "TZA",
        "name": "Tanzania",
        "iso2code": "TZ"
      },
      "UG": {
        "id": "UGA",
        "name": "Uganda",
        "iso2code": "UG"
      },
      "ZA": {
        "id": "ZAF",
        "name": "South Africa",
        "iso2code": "ZA"
      },
      "ZM": {
        "id": "ZMB",
        "name": "Zambia",
        "iso2code": "ZM"
      },
      "ZW": {
        "id": "ZWE",
        "name": "Zimbabwe",
        "iso2code": "ZW"
      }
    }
  },
  "Z7": {
    "value": "Europe & Central Asia",
    "id": "ECS",
    "iso2code": "Z7",
    "countries": {
      "AL": {
        "id": "ALB",
        "name": "Albania",
        "iso2code": "AL"
      },
      "AD": {
        "id": "AND",
        "name": "Andorra",
        "iso2code": "AD"
      },
      "AM": {
        "id": "ARM",
        "name": "Armenia",
        "iso2code": "AM"
      },
      "AT": {
        "id": "AUT",
        "name": "Austria",
        "iso2code": "AT"
      },
      "AZ": {
        "id": "AZE",
        "name": "Azerbaijan",
        "iso2code": "AZ"
      },
      "BE": {
        "id": "BEL",
        "name": "Belgium",
        "iso2code": "BE"
      },
      "BG": {
        "id": "BGR",
        "name": "Bulgaria",
        "iso2code": "BG"
      },
      "BA": {
        "id": "BIH",
        "name": "Bosnia and Herzegovina",
        "iso2code": "BA"
      },
      "BY": {
        "id": "BLR",
        "name": "Belarus",
        "iso2code": "BY"
      },
      "CH": {
        "id": "CHE",
        "name": "Switzerland",
        "iso2code": "CH"
      },
      "JG": {
        "id": "CHI",
        "name": "Channel Islands",
        "iso2code": "JG"
      },
      "CY": {
        "id": "CYP",
        "name": "Cyprus",
        "iso2code": "CY"
      },
      "CZ": {
        "id": "CZE",
        "name": "Czech Republic",
        "iso2code": "CZ"
      },
      "DE": {
        "id": "DEU",
        "name": "Germany",
        "iso2code": "DE"
      },
      "DK": {
        "id": "DNK",
        "name": "Denmark",
        "iso2code": "DK"
      },
      "ES": {
        "id": "ESP",
        "name": "Spain",
        "iso2code": "ES"
      },
      "EE": {
        "id": "EST",
        "name": "Estonia",
        "iso2code": "EE"
      },
      "FI": {
        "id": "FIN",
        "name": "Finland",
        "iso2code": "FI"
      },
      "LI": {
        "id": "LIE",
        "name": "Liechtenstein",
        "iso2code": "LI"
      },
      "LT": {
        "id": "LTU",
        "name": "Lithuania",
        "iso2code": "LT"
      },
      "LU": {
        "id": "LUX",
        "name": "Luxembourg",
        "iso2code": "LU"
      },
      "LV": {
        "id": "LVA",
        "name": "Latvia",
        "iso2code": "LV"
      },
      "MC": {
        "id": "MCO",
        "name": "Monaco",
        "iso2code": "MC"
      },
      "MD": {
        "id": "MDA",
        "name": "Moldova",
        "iso2code": "MD"
      },
      "MK": {
        "id": "MKD",
        "name": "North Macedonia",
        "iso2code": "MK"
      },
      "ME": {
        "id": "MNE",
        "name": "Montenegro",
        "iso2code": "ME"
      },
      "FR": {
        "id": "FRA",
        "name": "France",
        "iso2code": "FR"
      },
      "FO": {
        "id": "FRO",
        "name": "Faroe Islands",
        "iso2code": "FO"
      },
      "GB": {
        "id": "GBR",
        "name": "United Kingdom",
        "iso2code": "GB"
      },
      "GE": {
        "id": "GEO",
        "name": "Georgia",
        "iso2code": "GE"
      },
      "GI": {
        "id": "GIB",
        "name": "Gibraltar",
        "iso2code": "GI"
      },
      "GR": {
        "id": "GRC",
        "name": "Greece",
        "iso2code": "GR"
      },
      "GL": {
        "id": "GRL",
        "name": "Greenland",
        "iso2code": "GL"
      },
      "HR": {
        "id": "HRV",
        "name": "Croatia",
        "iso2code": "HR"
      },
      "HU": {
        "id": "HUN",
        "name": "Hungary",
        "iso2code": "HU"
      },
      "IM": {
        "id": "IMN",
        "name": "Isle of Man",
        "iso2code": "IM"
      },
      "IE": {
        "id": "IRL",
        "name": "Ireland",
        "iso2code": "IE"
      },
      "IS": {
        "id": "ISL",
        "name": "Iceland",
        "iso2code": "IS"
      },
      "IT": {
        "id": "ITA",
        "name": "Italy",
        "iso2code": "IT"
      },
      "KZ": {
        "id": "KAZ",
        "name": "Kazakhstan",
        "iso2code": "KZ"
      },
      "KG": {
        "id": "KGZ",
        "name": "Kyrgyz Republic",
        "iso2code": "KG"
      },
      "NL": {
        "id": "NLD",
        "name": "Netherlands",
        "iso2code": "NL"
      },
      "NO": {
        "id": "NOR",
        "name": "Norway",
        "iso2code": "NO"
      },
      "PL": {
        "id": "POL",
        "name": "Poland",
        "iso2code": "PL"
      },
      "PT": {
        "id": "PRT",
        "name": "Portugal",
        "iso2code": "PT"
      },
      "RO": {
        "id": "ROU",
        "name": "Romania",
        "iso2code": "RO"
      },
      "RU": {
        "id": "RUS",
        "name": "Russian Federation",
        "iso2code": "RU"
      },
      "SM": {
        "id": "SMR",
        "name": "San Marino",
        "iso2code": "SM"
      },
      "RS": {
        "id": "SRB",
        "name": "Serbia",
        "iso2code": "RS"
      },
      "SK": {
        "id": "SVK",
        "name": "Slovak Republic",
        "iso2code": "SK"
      },
      "SI": {
        "id": "SVN",
        "name": "Slovenia",
        "iso2code": "SI"
      },
      "SE": {
        "id": "SWE",
        "name": "Sweden",
        "iso2code": "SE"
      },
      "TJ": {
        "id": "TJK",
        "name": "Tajikistan",
        "iso2code": "TJ"
      },
      "TM": {
        "id": "TKM",
        "name": "Turkmenistan",
        "iso2code": "TM"
      },
      "TR": {
        "id": "TUR",
        "name": "Turkey",
        "iso2code": "TR"
      },
      "UA": {
        "id": "UKR",
        "name": "Ukraine",
        "iso2code": "UA"
      },
      "UZ": {
        "id": "UZB",
        "name": "Uzbekistan",
        "iso2code": "UZ"
      },
      "XK": {
        "id": "XKX",
        "name": "Kosovo",
        "iso2code": "XK"
      }
    }
  },
  "ZQ": {
    "value": "Middle East & North Africa",
    "id": "MEA",
    "iso2code": "ZQ",
    "countries": {
      "AE": {
        "id": "ARE",
        "name": "United Arab Emirates",
        "iso2code": "AE"
      },
      "BH": {
        "id": "BHR",
        "name": "Bahrain",
        "iso2code": "BH"
      },
      "DJ": {
        "id": "DJI",
        "name": "Djibouti",
        "iso2code": "DJ"
      },
      "DZ": {
        "id": "DZA",
        "name": "Algeria",
        "iso2code": "DZ"
      },
      "EG": {
        "id": "EGY",
        "name": "Egypt, Arab Rep.",
        "iso2code": "EG"
      },
      "KW": {
        "id": "KWT",
        "name": "Kuwait",
        "iso2code": "KW"
      },
      "LB": {
        "id": "LBN",
        "name": "Lebanon",
        "iso2code": "LB"
      },
      "LY": {
        "id": "LBY",
        "name": "Libya",
        "iso2code": "LY"
      },
      "MA": {
        "id": "MAR",
        "name": "Morocco",
        "iso2code": "MA"
      },
      "MT": {
        "id": "MLT",
        "name": "Malta",
        "iso2code": "MT"
      },
      "IR": {
        "id": "IRN",
        "name": "Iran, Islamic Rep.",
        "iso2code": "IR"
      },
      "IQ": {
        "id": "IRQ",
        "name": "Iraq",
        "iso2code": "IQ"
      },
      "IL": {
        "id": "ISR",
        "name": "Israel",
        "iso2code": "IL"
      },
      "JO": {
        "id": "JOR",
        "name": "Jordan",
        "iso2code": "JO"
      },
      "OM": {
        "id": "OMN",
        "name": "Oman",
        "iso2code": "OM"
      },
      "PS": {
        "id": "PSE",
        "name": "West Bank and Gaza",
        "iso2code": "PS"
      },
      "QA": {
        "id": "QAT",
        "name": "Qatar",
        "iso2code": "QA"
      },
      "SA": {
        "id": "SAU",
        "name": "Saudi Arabia",
        "iso2code": "SA"
      },
      "SY": {
        "id": "SYR",
        "name": "Syrian Arab Republic",
        "iso2code": "SY"
      },
      "TN": {
        "id": "TUN",
        "name": "Tunisia",
        "iso2code": "TN"
      },
      "YE": {
        "id": "YEM",
        "name": "Yemen, Rep.",
        "iso2code": "YE"
      }
    }
  },
  "Z4": {
    "value": "East Asia & Pacific",
    "id": "EAS",
    "iso2code": "Z4",
    "countries": {
      "AS": {
        "id": "ASM",
        "name": "American Samoa",
        "iso2code": "AS"
      },
      "AU": {
        "id": "AUS",
        "name": "Australia",
        "iso2code": "AU"
      },
      "BN": {
        "id": "BRN",
        "name": "Brunei Darussalam",
        "iso2code": "BN"
      },
      "CN": {
        "id": "CHN",
        "name": "China",
        "iso2code": "CN"
      },
      "FJ": {
        "id": "FJI",
        "name": "Fiji",
        "iso2code": "FJ"
      },
      "KR": {
        "id": "KOR",
        "name": "Korea, Rep.",
        "iso2code": "KR"
      },
      "LA": {
        "id": "LAO",
        "name": "Lao PDR",
        "iso2code": "LA"
      },
      "MO": {
        "id": "MAC",
        "name": "Macao SAR, China",
        "iso2code": "MO"
      },
      "MH": {
        "id": "MHL",
        "name": "Marshall Islands",
        "iso2code": "MH"
      },
      "MM": {
        "id": "MMR",
        "name": "Myanmar",
        "iso2code": "MM"
      },
      "MN": {
        "id": "MNG",
        "name": "Mongolia",
        "iso2code": "MN"
      },
      "MP": {
        "id": "MNP",
        "name": "Northern Mariana Islands",
        "iso2code": "MP"
      },
      "MY": {
        "id": "MYS",
        "name": "Malaysia",
        "iso2code": "MY"
      },
      "FM": {
        "id": "FSM",
        "name": "Micronesia, Fed. Sts.",
        "iso2code": "FM"
      },
      "GU": {
        "id": "GUM",
        "name": "Guam",
        "iso2code": "GU"
      },
      "HK": {
        "id": "HKG",
        "name": "Hong Kong SAR, China",
        "iso2code": "HK"
      },
      "ID": {
        "id": "IDN",
        "name": "Indonesia",
        "iso2code": "ID"
      },
      "JP": {
        "id": "JPN",
        "name": "Japan",
        "iso2code": "JP"
      },
      "KH": {
        "id": "KHM",
        "name": "Cambodia",
        "iso2code": "KH"
      },
      "KI": {
        "id": "KIR",
        "name": "Kiribati",
        "iso2code": "KI"
      },
      "NC": {
        "id": "NCL",
        "name": "New Caledonia",
        "iso2code": "NC"
      },
      "NR": {
        "id": "NRU",
        "name": "Nauru",
        "iso2code": "NR"
      },
      "NZ": {
        "id": "NZL",
        "name": "New Zealand",
        "iso2code": "NZ"
      },
      "PH": {
        "id": "PHL",
        "name": "Philippines",
        "iso2code": "PH"
      },
      "PW": {
        "id": "PLW",
        "name": "Palau",
        "iso2code": "PW"
      },
      "PG": {
        "id": "PNG",
        "name": "Papua New Guinea",
        "iso2code": "PG"
      },
      "KP": {
        "id": "PRK",
        "name": "Korea, Dem. People’s Rep.",
        "iso2code": "KP"
      },
      "PF": {
        "id": "PYF",
        "name": "French Polynesia",
        "iso2code": "PF"
      },
      "SG": {
        "id": "SGP",
        "name": "Singapore",
        "iso2code": "SG"
      },
      "SB": {
        "id": "SLB",
        "name": "Solomon Islands",
        "iso2code": "SB"
      },
      "TH": {
        "id": "THA",
        "name": "Thailand",
        "iso2code": "TH"
      },
      "TL": {
        "id": "TLS",
        "name": "Timor-Leste",
        "iso2code": "TL"
      },
      "TO": {
        "id": "TON",
        "name": "Tonga",
        "iso2code": "TO"
      },
      "TV": {
        "id": "TUV",
        "name": "Tuvalu",
        "iso2code": "TV"
      },
      "TW": {
        "id": "TWN",
        "name": "Taiwan, China",
        "iso2code": "TW"
      },
      "VN": {
        "id": "VNM",
        "name": "Vietnam",
        "iso2code": "VN"
      },
      "VU": {
        "id": "VUT",
        "name": "Vanuatu",
        "iso2code": "VU"
      },
      "WS": {
        "id": "WSM",
        "name": "Samoa",
        "iso2code": "WS"
      }
    }
  },
  "XU": {
    "value": "North America",
    "id": "NAC",
    "iso2code": "XU",
    "countries": {
      "BM": {
        "id": "BMU",
        "name": "Bermuda",
        "iso2code": "BM"
      },
      "CA": {
        "id": "CAN",
        "name": "Canada",
        "iso2code": "CA"
      },
      "US": {
        "id": "USA",
        "name": "United States",
        "iso2code": "US"
      }
    }
  }
}

export default {
	getCountryByCode: code => {
		return CountryCodes[code]
	},
    getRegions: () => {
        return Regions
    }
}