export default {
	findByAttribute: (array, attr, value) => {
		let res = {};
		array.forEach(o => {
			if(o[attr] == value){
				res = o
			}
		})

		return res;
	},
	unique: array => {
		let res = []
		array.forEach(o => {
			if(!res.filter(item => item == o).length) res = res.concat(o)
		})

		return res
	}
}