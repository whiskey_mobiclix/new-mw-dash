import React from 'react';
import * as Icon from 'react-feather';

export default {
	header: (pathname, group, product) => {
		let res = {
			logo: '',
			name: '',
			url: '/'
		};

		if (group === 'USER_GROUP_PARTNER') res.url = '/product';
		else if (group === 'USER_GROUP_FINANCE') {
			res.url = '/finance';
			res.name = 'FINANCE BOARD';
		} else {
			res.url = '/admin';
			res.name = 'ADMIN BOARD';
		}

		if (/^\/product(\/.+)?/.test(pathname)) res.name = product;

		return res;
	},

	list: (pathname, me) => {
		if (/^\/product/.test(pathname)) {
			let res = [
				{
					id: 'product-overview',
					title: 'Product Overview',
					type: 'collapse',
					icon: <Icon.Eye size={20} />,
					children: [
						{
							id: 'product-info',
							title: 'Product Information',
							type: 'item',
							icon: <Icon.Circle size={12} />,
							navLink: '/product/overview'
						},
						{
							id: 'product-status',
							title: 'Product Status',
							type: 'item',
							icon: <Icon.Circle size={12} />,
							navLink: '/product/status'
						}
					]
				},
				{
					type: 'groupHeader',
					groupTitle: 'STATISTICS'
				},
				{
					id: 'product-dashboard',
					title: 'Product Dashboard',
					type: 'collapse',
					icon: <Icon.Activity size={20} />,
					children: [
						{
							id: 'dashboard-overview',
							title: 'Dashboard Overview',
							type: 'item',
							icon: <Icon.Circle size={12} />,
							navLink: '/product',
							permissions: ["stats/transactions", "stats/users", "stats/revenues"],
						},
						{
							id: 'statistics',
							title: 'Statistics',
							type: 'item',
							icon: <Icon.Circle size={12} />,
							navLink: '/product/statistics',
							permissions: ["stats/transactions", "stats/users", "stats/revenues"],
						},
						{
							id: 'payment',
							title: 'Transactions & Receivable',
							type: 'item',
							icon: <Icon.Circle size={12} />,
							navLink: '/product/transactions',
							permissions: ["stats/transactions"]
						},
						{
							id: 'markets-comparision',
							title: 'Markets Comparision',
							type: 'item',
							icon: <Icon.Circle size={12} />,
							navLink: '/product/markets-comparision',
							permissions: ["stats/transactions", "stats/users", "stats/revenues"],
						},
					]
				},
				// {
				// 	id: 'accquisition',
				// 	title: 'Accquisition',
				// 	type: 'item',
				// 	icon: <Icon.Pause size={20} />,
				// 	navLink: '/product/accquisition'
				// },
				{
					id: 'monthly-statement',
					title: 'Monthly Statement',
					type: 'item',
					icon: <Icon.AlignLeft size={20} />,
					navLink: '/product/monthly-statement',
					permissions: ["stats/transactions", "stats/users", "stats/revenues"],
				},
				{
					id: 'product-test',
					title: 'Test Components',
					type: 'item',
					icon: <Icon.Check size={20} />,
					navLink: '/product/test',
				},
			];

			if(!me.managed_by){
				res = res.concat([
					{
						type: 'groupHeader',
						groupTitle: 'SETTINGS'
					},
					// {
					// 	id: 'content-management',
					// 	title: 'Content Management',
					// 	type: 'item',
					// 	icon: <Icon.FileText size={20} />,
					// 	navLink: '/product/contents'
					// },
					{
						id: 'administrator',
						title: 'Administrator',
						type: 'collapse',
						icon: <Icon.Settings size={20} />,
						children: [
							{
								id: 'product-administrator-list',
								title: 'Staffs',
								type: 'item',
								icon: <Icon.Circle size={12} />,
								navLink: '/product/partner-users'
							},
							{
								id: 'product-administrator-create',
								title: 'Create new staff',
								type: 'item',
								icon: <Icon.Circle size={12} />,
								navLink: '/product/create-user'
							}
						]
					},
				])
			}

			return res;
		}

		if (/^\/finance/.test(pathname)) {
			return [
				{
					type: 'groupHeader',
					groupTitle: 'STATISTICS'
				},
				{
					id: 'finance-dashboard',
					title: 'Dashboard',
					type: 'collapse',
					icon: <Icon.BarChart2 size={20} />,
					children: [
						{
							id: 'finance-dashboard-overview',
							title: 'Overview',
							type: 'item',
							icon: <Icon.Circle size={12} />,
							navLink: '/finance'
						},
						{
							id: 'finance-dashboard-transactions',
							title: 'Transactions',
							type: 'item',
							icon: <Icon.Circle size={12} />,
							navLink: '/finance/transactions'
						}
					]
				},
				{
					type: 'groupHeader',
					groupTitle: 'TOOLS'
				},
				{
					id: 'finance-discrepency',
					title: 'Discrepency Check',
					type: 'item',
					icon: <Icon.Activity size={20} />,
					navLink: "/finance/discrepency"
				},
				{
					type: 'groupHeader',
					groupTitle: 'SETTINGS'
				},
				{
					id: 'finance-configs',
					title: 'Configs',
					type: 'item',
					icon: <Icon.Settings size={20} />,
					navLink: '/finance/configs'
				}
			];
		}

		return [
			{
				type: 'groupHeader',
				groupTitle: 'STATISTICS'
			},
			{
				id: 'admin-dashboard',
				title: 'Dashboard',
				type: 'collapse',
				icon: <Icon.BarChart2 size={20} />,
				children: [
					{
						id: 'admin-dashboard-overview',
						title: 'Overview',
						type: 'item',
						icon: <Icon.Circle size={12} />,
						navLink: '/admin'
					},
					{
						id: 'admin-dashboard-comparision-in-depth',
						title: 'Comparision In-Depth',
						type: 'item',
						icon: <Icon.Circle size={12} />,
						navLink: '/admin/comparision-in-depth'
					}
				]
			},
			// {
			// 	id: 'admin-accquisition',
			// 	title: 'Accquisition',
			// 	type: 'item',
			// 	icon: <Icon.Pause size={20} />,
			// 	navLink: '/admin/accquisition'
			// },
			{
				type: 'groupHeader',
				groupTitle: 'SETTINGS'
			},
			{
				id: 'admin-settings',
				title: 'Administrator',
				type: 'collapse',
				icon: <Icon.Users size={20} />,
				children: [
					{
						id: 'admin-settings-users',
						title: 'Internal users',
						type: 'item',
						icon: <Icon.Circle size={12} />,
						navLink: '/admin/internal-users'
					},
					{
						id: 'admin-settings-partners',
						title: 'Partner users',
						type: 'item',
						icon: <Icon.Circle size={12} />,
						navLink: '/admin/partner-users'
					},
					{
						id: 'admin-settings-create',
						title: 'Create new user',
						type: 'item',
						icon: <Icon.Circle size={12} />,
						navLink: '/admin/create-user'
					}
				]
			},
			{
				type: 'groupHeader',
				groupTitle: 'MANAGEMENTS'
			},
			{
				id: 'admin-products',
				title: 'Products',
				type: 'collapse',
				icon: <Icon.Grid size={20} />,
				children: [
					{
						id: 'admin-products-list',
						title: 'All products',
						type: 'item',
						icon: <Icon.Circle size={12} />,
						navLink: '/admin/products'
					}
				]
			}
		];
	}
};
