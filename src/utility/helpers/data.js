import moment from 'moment';

const merge = (a, b) => {
	let keysA = Object.keys(a),
		keysB = Object.keys(b),
		res = {};
	let keys = keysA;
	if (keysA.length < keysB.length) keys = keysB;

	keys.forEach(o => {
		if (/^\d*\.?\d*$/.test(a[o]) || /^\d*\.?\d*$/.test(b[o]))
			res[o] = parseFloat(a[o] || 0) + parseFloat(b[o] || 0);
	});

	return res;
};

const groupByDate = data => {
	try {
		let res = [];
		data.forEach(o => {
			let dt = moment(parseInt(o.time) * 1000).format('DD MMM YYYY'),
				index = 0;
			if (
				res.filter((item, ix) => {
					if (item.time === dt) {
						index = ix;
						return true;
					}
					return false;
				}).length
			) {
				res[index] = {
					...merge(res[index], o),
					time: dt
				};
			} else {
				res = res.concat({
					...o,
					time: dt
				});
			}
		});

		return res.sort((a, b) => {
			if (moment(a.time) < moment(b.time)) return -1;
			return 1;
		});
	} catch (e) {
		return [];
	}
};

const groupByMonths = data => {
	try {
		let res = [];
		data.forEach(o => {
			let dt = moment(parseInt(o.time) * 1000).format('MMM YYYY'),
				index = 0;
			if (
				res.filter((item, ix) => {
					if (item.time === dt) {
						index = ix;
						return true;
					}
					return false;
				}).length
			) {
				res[index] = {
					...merge(res[index], o),
					time: dt
				};
			} else {
				res = res.concat({
					...o,
					time: dt
				});
			}
		});

		return res.sort((a, b) => {
			if (moment(a.time) < moment(b.time)) return -1;
			return 1;
		});
	} catch (e) {
		return [];
	}
};

export default {
	fill: (dates, data) => {
		let res = [];
		dates.forEach(o => {
			res = res.concat(
				data.filter(d => d.time == o)[0] || {
					time: o
				}
			);
		});

		return res;
	},
	groupByMonths: data => {
		return groupByMonths(data);
	},
	groupByDate: data => {
		return groupByDate(data);
	},
	groupByMarkets: data => {
		let res = [];
		try {
			data.forEach(o => {
				let index = 0;
				if (
					res.filter((item, ix) => {
						if (item.country === o.groupBy.country) {
							index = ix;
							return true;
						}
						return false;
					}).length
				) {
					res[index] = {
						...merge(res[index], o),
						country: o.groupBy.country
					};
				} else {
					res = res.concat({
						...o,
						country: o.groupBy.country
					});
				}
			});

			return res;
		} catch (e) {
			console.log(e);
			return [];
		}
	},
	groupByMNOs: data => {
		let res = [];
		try {
			data.forEach(o => {
				let index = 0;
				if (
					res.filter((item, ix) => {
						if (item.mno === o.groupBy.mno) {
							index = ix;
							return true;
						}
						return false;
					}).length
				) {
					res[index] = {
						...merge(res[index], o),
						mno: o.groupBy.mno
					};
				} else {
					res = res.concat({
						...o,
						mno: o.groupBy.mno
					});
				}
			});

			return res;
		} catch (e) {
			console.log(e);
			return [];
		}
	},
	groupByAggregators: data => {
		let res = [];
		
		try {
			data.forEach(o => {
				let index = 0;
				if (
					res.filter((item, ix) => {
						if (item.aggregator === o.groupBy.aggregator) {
							index = ix;
							return true;
						}
						return false;
					}).length
				) {
					res[index] = {
						...merge(res[index], o),
						aggregator: o.groupBy.aggregator
					};
				} else {
					if (o.groupBy.aggregator) {
						res = res.concat({
							...o,
							aggregator: o.groupBy.aggregator
						});
					}
				}
			});

			return res;
		} catch (e) {
			console.log(e);
			return [];
		}
	},
	groupByDateAndMarkets: data => {
		try {
			let res = [],
				countries = [];
			data.forEach(o => {
				if (
					o.groupBy &&
					o.groupBy.country &&
					countries.indexOf(o.groupBy.country) < 0
				) {
					countries = countries.concat(o.groupBy.country);
				}
			});

			return countries.map(c => ({
				country: c,
				data: groupByMonths(
					data.filter(o => o.groupBy && o.groupBy.country == c)
				)
			}));
		} catch (e) {
			return [];
		}
	},
	filter: (data, country, mno, product) => {
		let res = data
		if(country) res = data.filter(o => o.groupBy && o.groupBy.country === country)
		if(mno) res = res.filter(o => o.groupBy && o.groupBy.mno === mno)
		if(product) res = res.filter(o => o.groupBy && (o.groupBy.product === product.toUpperCase() || o.groupBy.product === product))

		return res;
	}
};
