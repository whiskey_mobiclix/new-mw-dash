export default [
	{
		code: "gameon",
		name: "GameOn",
		vertical: "sport",
		address: "https://game-on.live",
		methods: "Credit card",
		status: "Running in 5 countries",
		activity: "Whiskey updated new contents",
	},
	{
		code: "vanguard",
		name: "Vanguard Cardfight",
		vertical: "svods",
		address: "https://vanguard-cardfight.com",
		methods: "Credit card, Mobile payment",
		status: "Running in 5 countries",
		activity: "Whiskey updated new contents",
	},
	{
		code: "wwe",
		name: "WWE Sea",
		vertical: "sport",
		address: "https://wwe-sea.com",
		methods: "Credit card, Mobile payment",
		status: "Running in 5 countries",
		activity: "Whiskey updated new contents",
	},
	{
		code: "babystep",
		name: "Babystep Mobile",
		vertical: "sport",
		address: "https://babystep-mobile.com",
		methods: "Mobile payment",
		status: "Running in 5 countries",
		activity: "Whiskey updated new contents",
	},
	{
		code: "mocoplay",
		name: "Mocoplay",
		vertical: "game",
		address: "https://mocoplay.com",
		methods: "Credit card",
		status: "Running in 5 countries",
		activity: "Whiskey updated new contents",
	},
	{
		code: "pulse",
		name: "Pulse",
		vertical: "svods",
		address: "https://pulse-web.com",
		methods: "Credit card",
		status: "Running in 5 countries",
		activity: "Whiskey updated new contents",
	},
	{
		code: "mocovids",
		name: "Mocovids",
		vertical: "svods",
		address: "https://mocovids.com",
		methods: "Credit card",
		status: "Running in 5 countries",
		activity: "Whiskey updated new contents",
	}
]