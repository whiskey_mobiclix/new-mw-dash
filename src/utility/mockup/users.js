export default {
  "users": [
    {
      "id": "1",
      "name": "Whiskey",
      "email": "whiskey@example.com",
      "status": "USER_STATUS_ACTIVE",
      "group": "USER_GROUP_PARTNER",
      "lastLoginAt": "1588575209393",
      "role": "Mid-Level"
    },
    {
      "id": "2",
      "email": "johny@example.com",
      "name": "Johny Tri Nguyen",
      "status": "USER_STATUS_ACTIVE",
      "group": "USER_GROUP_PARTNER",
      "lastLoginAt": "1588575209393",
      "role": "Junior-Level"
    },
    {
      "id": "3",
      "email": "tommy@example.com",
      "name": "Red Lolipop",
      "status": "USER_STATUS_INACTIVE",
      "group": "USER_GROUP_PARTNER",
      "lastLoginAt": "1588575209393",
      "role": "Mid-Level"
    },
    {
      "id": "4",
      "name": "Whiskey",
      "email": "whiskey@example.com",
      "status": "USER_STATUS_ACTIVE",
      "group": "USER_GROUP_PARTNER",
      "lastLoginAt": "1588575209393",
      "role": "Mid-Level"
    },
    {
      "id": "5",
      "email": "johny@example.com",
      "name": "Johny Tri Nguyen",
      "status": "USER_STATUS_ACTIVE",
      "group": "USER_GROUP_PARTNER",
      "lastLoginAt": "1588575209393",
      "role": "Junior-Level"
    },
    {
      "id": "6",
      "email": "tommy@example.com",
      "name": "Red Lolipop",
      "status": "USER_STATUS_INACTIVE",
      "group": "USER_GROUP_PARTNER",
      "lastLoginAt": "1588575209393",
      "role": "Mid-Level"
    }
  ]
}