import moment from 'moment';

let count = 0;

export default () => ({
    "data": [
        {
            "Gross_Revenue": 4921.605000001181,
            "Net_Revenue": 2001.6167535004802,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581120000
        },
        {
            "Gross_Revenue": 88.67200000000001,
            "Net_Revenue": 37.90728000000001,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581379200
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 91.28000000000002,
            "Net_Revenue": 39.022200000000005,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 688.9679999999804,
            "Net_Revenue": 225.45788831999357,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 0.96,
            "Net_Revenue": 0.37330329600000006,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1581465600
        },
        {
            "Gross_Revenue": 549,
            "Net_Revenue": 311.283,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 548.3999999999979,
            "Net_Revenue": 289.4729399999989,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 9.05280000000001,
            "Net_Revenue": 3.655068000000004,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1581984000
        },
        {
            "Gross_Revenue": 0.384,
            "Net_Revenue": 0.14932131839999999,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1580947200
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581120000
        },
        {
            "Gross_Revenue": 616.1399999999941,
            "Net_Revenue": 201.62565359999806,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1580860800
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "DEBITEL",
                "aggregator": "ONEBIP"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 2.445,
            "Net_Revenue": 1.2225,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "COMVIVA"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 0.192,
            "Net_Revenue": 0.07466065919999999,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 435.5999999999973,
            "Net_Revenue": 229.93145999999857,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 474.18000000001643,
            "Net_Revenue": 362.74770000001257,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 88.67200000000001,
            "Net_Revenue": 37.90728000000001,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 520.7999999999969,
            "Net_Revenue": 274.9042799999983,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 93.88800000000002,
            "Net_Revenue": 40.13712000000001,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 4415.060000000932,
            "Net_Revenue": 1795.6049020003788,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 145.90799999999976,
            "Net_Revenue": 47.74693391999992,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581811200
        },
        {
            "Gross_Revenue": 16.740000000000027,
            "Net_Revenue": 12.80610000000002,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581984000
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 411.0599999999892,
            "Net_Revenue": 167.1781019999956,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582156800
        },
        {
            "Gross_Revenue": 108.00000000000013,
            "Net_Revenue": 62.596800000000066,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581897600
        },
        {
            "Gross_Revenue": 83.456,
            "Net_Revenue": 35.67744,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1580860800
        },
        {
            "Gross_Revenue": 4.89,
            "Net_Revenue": 2.445,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "COMVIVA"
            },
            "time": 1581984000
        },
        {
            "Gross_Revenue": 263.25,
            "Net_Revenue": 149.26275,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 17.700000000000006,
            "Net_Revenue": 13.540500000000005,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581897600
        },
        {
            "Gross_Revenue": 6.955200000000005,
            "Net_Revenue": 2.8081620000000016,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1581897600
        },
        {
            "Gross_Revenue": 204.75,
            "Net_Revenue": 116.09325000000001,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 6449.499000001427,
            "Net_Revenue": 2656.0649256755873,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581465600
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "DEBITEL",
                "aggregator": "ONEBIP"
            },
            "time": 1582675200
        },
        {
            "Gross_Revenue": 91.28000000000002,
            "Net_Revenue": 39.022200000000005,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581033600
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581897600
        },
        {
            "Gross_Revenue": 2174.4659999998817,
            "Net_Revenue": 895.4994604499511,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 435.2399999999881,
            "Net_Revenue": 177.01210799999512,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582070400
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.6277372,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1580947200
        },
        {
            "Gross_Revenue": 1377.6399999999987,
            "Net_Revenue": 627.6872249999996,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.6277372,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1581552000
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 6449.499000001427,
            "Net_Revenue": 2656.0649256755873,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581120000
        },
        {
            "Gross_Revenue": 7089.957000001658,
            "Net_Revenue": 2919.8215415256827,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 7204.600000002819,
            "Net_Revenue": 2930.1108200011463,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 46.94399999999999,
            "Net_Revenue": 20.068559999999994,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581638400
        },
        {
            "Gross_Revenue": 614.25,
            "Net_Revenue": 348.27975,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581552000
        },
        {
            "Gross_Revenue": 1008.2325000000022,
            "Net_Revenue": 459.3759328125011,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1582243200
        },
        {
            "Gross_Revenue": 2083.1249999999854,
            "Net_Revenue": 949.1238281249933,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1582070400
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581984000
        },
        {
            "Gross_Revenue": 592.7039999999985,
            "Net_Revenue": 193.95645695999949,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581465600
        },
        {
            "Gross_Revenue": 5715.580000001746,
            "Net_Revenue": 2324.52638600071,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581552000
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581638400
        },
        {
            "Gross_Revenue": 65.26800000000023,
            "Net_Revenue": 21.358300320000073,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1582502400
        },
        {
            "Gross_Revenue": 1581.1900000001658,
            "Net_Revenue": 643.0699730000674,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581897600
        },
        {
            "Gross_Revenue": 2.640000000000002,
            "Net_Revenue": 2.0196000000000014,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1582675200
        },
        {
            "Gross_Revenue": 461.40000000001595,
            "Net_Revenue": 352.97100000001217,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1580947200
        },
        {
            "Gross_Revenue": 141.11999999999958,
            "Net_Revenue": 46.180108799999864,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581897600
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581811200
        },
        {
            "Gross_Revenue": 4188.730000000799,
            "Net_Revenue": 1703.5564910003247,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 140.40000000000015,
            "Net_Revenue": 81.37584000000008,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581465600
        },
        {
            "Gross_Revenue": 413.9999999999975,
            "Net_Revenue": 218.52989999999866,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1582502400
        },
        {
            "Gross_Revenue": 306.4320000000058,
            "Net_Revenue": 100.27680768000191,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581638400
        },
        {
            "Gross_Revenue": 132.00000000000017,
            "Net_Revenue": 76.5072000000001,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 20150.23500000069,
            "Net_Revenue": 9181.087509375317,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581120000
        },
        {
            "Gross_Revenue": 2633.7990000000477,
            "Net_Revenue": 1084.6642731750196,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582156800
        },
        {
            "Gross_Revenue": 775.2000000000065,
            "Net_Revenue": 409.1893200000034,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1582156800
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1582070400
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "DEBITEL",
                "aggregator": "ONEBIP"
            },
            "time": 1582502400
        },
        {
            "Gross_Revenue": 2.608,
            "Net_Revenue": 1.11492,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1582070400
        },
        {
            "Gross_Revenue": 5217.1600000014105,
            "Net_Revenue": 2121.8189720005735,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581033600
        },
        {
            "Gross_Revenue": 532.7999999999973,
            "Net_Revenue": 281.23847999999856,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581120000
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 144,
            "Net_Revenue": 81.64800000000001,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581465600
        },
        {
            "Gross_Revenue": 2288.936999999923,
            "Net_Revenue": 942.6414800249682,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581811200
        },
        {
            "Gross_Revenue": 213.75,
            "Net_Revenue": 121.19625,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1582243200
        },
        {
            "Gross_Revenue": 514.7999999999967,
            "Net_Revenue": 271.7371799999982,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581638400
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581033600
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.6277372,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1580860800
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 312.75,
            "Net_Revenue": 177.32925,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581984000
        },
        {
            "Gross_Revenue": 103.20000000000013,
            "Net_Revenue": 59.81472000000008,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581984000
        },
        {
            "Gross_Revenue": 14.700000000000033,
            "Net_Revenue": 11.245500000000025,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1582156800
        },
        {
            "Gross_Revenue": 252,
            "Net_Revenue": 142.884,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1582329600
        },
        {
            "Gross_Revenue": 477.8400000000166,
            "Net_Revenue": 365.54760000001266,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1580860800
        },
        {
            "Gross_Revenue": 677.3759999999826,
            "Net_Revenue": 221.6645222399943,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 663.6000000000023,
            "Net_Revenue": 350.28126000000117,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581811200
        },
        {
            "Gross_Revenue": 8.280000000000008,
            "Net_Revenue": 3.343050000000003,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1581811200
        },
        {
            "Gross_Revenue": 0.384,
            "Net_Revenue": 0.14932131839999999,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1581033600
        },
        {
            "Gross_Revenue": 0.384,
            "Net_Revenue": 0.14932131839999999,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 243,
            "Net_Revenue": 137.781,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581120000
        },
        {
            "Gross_Revenue": 420.75,
            "Net_Revenue": 238.56525,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 308.25,
            "Net_Revenue": 174.77775,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 448.23999999998784,
            "Net_Revenue": 182.29920799999505,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582243200
        },
        {
            "Gross_Revenue": 6793.395000001551,
            "Net_Revenue": 2797.6898958756383,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 85.2000000000001,
            "Net_Revenue": 49.38192000000006,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1582156800
        },
        {
            "Gross_Revenue": 6752.10250000014,
            "Net_Revenue": 3076.4267015625646,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581638400
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581465600
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.6277372,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 669.0599999999841,
            "Net_Revenue": 218.9431943999948,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1580947200
        },
        {
            "Gross_Revenue": 671.8319999999836,
            "Net_Revenue": 219.85030367999462,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581120000
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 486.0600000000169,
            "Net_Revenue": 371.8359000000129,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581552000
        },
        {
            "Gross_Revenue": 4.5,
            "Net_Revenue": 2.5515000000000003,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1582675200
        },
        {
            "Gross_Revenue": 18697.602500000616,
            "Net_Revenue": 8519.23182656278,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1580947200
        },
        {
            "Gross_Revenue": 5.216,
            "Net_Revenue": 2.22984,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1582502400
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1582502400
        },
        {
            "Gross_Revenue": 100.80000000000014,
            "Net_Revenue": 58.42368000000008,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1580947200
        },
        {
            "Gross_Revenue": 316.41999999999257,
            "Net_Revenue": 128.68801399999697,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582502400
        },
        {
            "Gross_Revenue": 3164.6160000002396,
            "Net_Revenue": 1303.2679842000987,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 513.9000000000161,
            "Net_Revenue": 393.13350000001236,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581120000
        },
        {
            "Gross_Revenue": 4151.385000000596,
            "Net_Revenue": 1709.6441276252453,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1580860800
        },
        {
            "Gross_Revenue": 15.240000000000038,
            "Net_Revenue": 11.658600000000028,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1582070400
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.6277372,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 758.4000000000059,
            "Net_Revenue": 400.32144000000307,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581465600
        },
        {
            "Gross_Revenue": 150.0000000000002,
            "Net_Revenue": 86.94000000000011,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 6563.004000001468,
            "Net_Revenue": 2702.8091223006045,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 88.67200000000001,
            "Net_Revenue": 37.90728000000001,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581465600
        },
        {
            "Gross_Revenue": 571.1999999999988,
            "Net_Revenue": 301.5079199999994,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1580860800
        },
        {
            "Gross_Revenue": 193.1999999999996,
            "Net_Revenue": 101.98061999999979,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1582675200
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.6277372,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 414.4140000000028,
            "Net_Revenue": 170.66604555000114,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582329600
        },
        {
            "Gross_Revenue": 88.67200000000001,
            "Net_Revenue": 37.90728000000001,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 1032.0000000000161,
            "Net_Revenue": 544.7412000000085,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1582329600
        },
        {
            "Gross_Revenue": 116.92799999999927,
            "Net_Revenue": 38.26351871999976,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1582070400
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.6277372,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1581033600
        },
        {
            "Gross_Revenue": 411.1899999999894,
            "Net_Revenue": 167.23097299999569,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 471.48000000001633,
            "Net_Revenue": 360.6822000000125,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 110.87999999999938,
            "Net_Revenue": 36.284371199999796,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1582156800
        },
        {
            "Gross_Revenue": 0.384,
            "Net_Revenue": 0.14932131839999999,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 469.81999999998715,
            "Net_Revenue": 191.07579399999477,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582416000
        },
        {
            "Gross_Revenue": 1733.1599999999937,
            "Net_Revenue": 789.6710249999971,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 45.60000000000001,
            "Net_Revenue": 26.429760000000005,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581811200
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1582243200
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.6277372,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1581465600
        },
        {
            "Gross_Revenue": 0.192,
            "Net_Revenue": 0.07466065919999999,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1581120000
        },
        {
            "Gross_Revenue": 808.8000000000078,
            "Net_Revenue": 426.92508000000413,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 2350.985000000393,
            "Net_Revenue": 956.1455995001598,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581638400
        },
        {
            "Gross_Revenue": 96.49600000000002,
            "Net_Revenue": 41.25204000000001,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1580947200
        },
        {
            "Gross_Revenue": 2.608,
            "Net_Revenue": 1.11492,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1582243200
        },
        {
            "Gross_Revenue": 7.838400000000007,
            "Net_Revenue": 3.1647540000000025,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1582243200
        },
        {
            "Gross_Revenue": 997.1224999999991,
            "Net_Revenue": 454.31393906249974,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1582502400
        },
        {
            "Gross_Revenue": 2749.7249999999776,
            "Net_Revenue": 1252.8434531249902,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581897600
        },
        {
            "Gross_Revenue": 20541.862500000716,
            "Net_Revenue": 9359.522789062828,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.6277372,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1581120000
        },
        {
            "Gross_Revenue": 3389.694000000321,
            "Net_Revenue": 1395.960731550132,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581984000
        },
        {
            "Gross_Revenue": 7.334999999999999,
            "Net_Revenue": 3.6674999999999995,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "COMVIVA"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 21772.295000000773,
            "Net_Revenue": 9920.138596875353,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 11.88000000000001,
            "Net_Revenue": 9.088200000000008,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1582502400
        },
        {
            "Gross_Revenue": 4.8576,
            "Net_Revenue": 1.9612559999999997,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1582675200
        },
        {
            "Gross_Revenue": 88.80000000000011,
            "Net_Revenue": 51.46848000000006,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 78.75,
            "Net_Revenue": 44.65125,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1582502400
        },
        {
            "Gross_Revenue": 5190.318000000972,
            "Net_Revenue": 2137.5027103504003,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 1063.7825000000032,
            "Net_Revenue": 484.68590156250156,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1582329600
        },
        {
            "Gross_Revenue": 4020.009000000549,
            "Net_Revenue": 1655.5402064252257,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581897600
        },
        {
            "Gross_Revenue": 0.384,
            "Net_Revenue": 0.14932131839999999,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1580860800
        },
        {
            "Gross_Revenue": 16561.705000000504,
            "Net_Revenue": 7546.06352812523,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 76.35600000000002,
            "Net_Revenue": 24.98673744000001,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1582416000
        },
        {
            "Gross_Revenue": 499.1999999999967,
            "Net_Revenue": 263.50271999999825,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581897600
        },
        {
            "Gross_Revenue": 234,
            "Net_Revenue": 132.678,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581033600
        },
        {
            "Gross_Revenue": 4.747199999999999,
            "Net_Revenue": 1.9166819999999998,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1581552000
        },
        {
            "Gross_Revenue": 96.00000000000013,
            "Net_Revenue": 55.641600000000075,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 0.384,
            "Net_Revenue": 0.14932131839999999,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 4479.1500000009555,
            "Net_Revenue": 1821.6703050003885,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1580860800
        },
        {
            "Gross_Revenue": 475.4400000000165,
            "Net_Revenue": 363.7116000000126,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 17811.58000000057,
            "Net_Revenue": 8115.5378250002595,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 467.5800000000162,
            "Net_Revenue": 357.69870000001237,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581033600
        },
        {
            "Gross_Revenue": 19389.20000000065,
            "Net_Revenue": 8834.340937500298,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581033600
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.6277372,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 54.000000000000036,
            "Net_Revenue": 31.29840000000002,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581033600
        },
        {
            "Gross_Revenue": 105.08399999999949,
            "Net_Revenue": 34.38768815999983,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1582243200
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1580860800
        },
        {
            "Gross_Revenue": 6837.8700000025265,
            "Net_Revenue": 2780.9617290010274,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 0.5760000000000001,
            "Net_Revenue": 0.22398197760000005,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 2.445,
            "Net_Revenue": 1.2225,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "COMVIVA"
            },
            "time": 1582416000
        },
        {
            "Gross_Revenue": 16.860000000000024,
            "Net_Revenue": 12.89790000000002,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1582329600
        },
        {
            "Gross_Revenue": 463.080000000016,
            "Net_Revenue": 354.25620000001226,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 5.216,
            "Net_Revenue": 2.22984,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581984000
        },
        {
            "Gross_Revenue": 248.10000000000787,
            "Net_Revenue": 189.796500000006,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581638400
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 93.88800000000002,
            "Net_Revenue": 40.13712000000001,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581120000
        },
        {
            "Gross_Revenue": 1626.8200000001866,
            "Net_Revenue": 661.6276940000758,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 3268.461000000277,
            "Net_Revenue": 1346.0339513251142,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582070400
        },
        {
            "Gross_Revenue": 11.040000000000015,
            "Net_Revenue": 4.457400000000006,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1582070400
        },
        {
            "Gross_Revenue": 129.02399999999912,
            "Net_Revenue": 42.221813759999705,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581984000
        },
        {
            "Gross_Revenue": 22644.43000000082,
            "Net_Revenue": 10317.505106250372,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581465600
        },
        {
            "Gross_Revenue": 870.3499999999738,
            "Net_Revenue": 353.9713449999893,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581984000
        },
        {
            "Gross_Revenue": 17608.82250000056,
            "Net_Revenue": 8023.156439062755,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 15.180000000000037,
            "Net_Revenue": 11.612700000000029,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581811200
        },
        {
            "Gross_Revenue": 61.200000000000045,
            "Net_Revenue": 35.471520000000034,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 82.9079999999999,
            "Net_Revenue": 27.130813919999966,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 2319.2124999999774,
            "Net_Revenue": 1056.6911953124898,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581984000
        },
        {
            "Gross_Revenue": 1372.5,
            "Net_Revenue": 778.2075000000001,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581897600
        },
        {
            "Gross_Revenue": 191.25,
            "Net_Revenue": 108.43875000000001,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1582156800
        },
        {
            "Gross_Revenue": 693.7559999999795,
            "Net_Revenue": 227.0247134399933,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 94.66800000000009,
            "Net_Revenue": 38.98664910000004,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582675200
        },
        {
            "Gross_Revenue": 880.8000000000105,
            "Net_Revenue": 464.93028000000555,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581206400
        },
        {
            "Gross_Revenue": 129.6000000000002,
            "Net_Revenue": 75.11616000000011,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1582070400
        },
        {
            "Gross_Revenue": 468.1200000000162,
            "Net_Revenue": 358.11180000001235,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581465600
        },
        {
            "Gross_Revenue": 443.9999999999972,
            "Net_Revenue": 234.36539999999854,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 7.838400000000007,
            "Net_Revenue": 3.1647540000000025,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 265.5,
            "Net_Revenue": 150.5385,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1582416000
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1582156800
        },
        {
            "Gross_Revenue": 8.611200000000009,
            "Net_Revenue": 3.476772000000003,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 627.227999999992,
            "Net_Revenue": 205.25409071999735,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 45.600000000000016,
            "Net_Revenue": 26.429760000000005,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1582675200
        },
        {
            "Gross_Revenue": 100.8000000000001,
            "Net_Revenue": 58.42368000000006,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1582502400
        },
        {
            "Gross_Revenue": 7.824,
            "Net_Revenue": 3.34476,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1582329600
        },
        {
            "Gross_Revenue": 943.2000000000129,
            "Net_Revenue": 497.8681200000068,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1582243200
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.6277372,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 101.80799999999955,
            "Net_Revenue": 33.315649919999856,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1582329600
        },
        {
            "Gross_Revenue": 7.396800000000006,
            "Net_Revenue": 2.986458000000002,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1582329600
        },
        {
            "Gross_Revenue": 777.7000000000005,
            "Net_Revenue": 354.3395625000003,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1582416000
        },
        {
            "Gross_Revenue": 529.8510000000017,
            "Net_Revenue": 218.20588807500067,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582416000
        },
        {
            "Gross_Revenue": 375.75,
            "Net_Revenue": 213.05025,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 0.384,
            "Net_Revenue": 0.14932131839999999,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 2.445,
            "Net_Revenue": 1.2225,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "COMVIVA"
            },
            "time": 1581120000
        },
        {
            "Gross_Revenue": 719.4599999999747,
            "Net_Revenue": 235.4360903999917,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 4043.3900000007475,
            "Net_Revenue": 1644.4467130003038,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1580947200
        },
        {
            "Gross_Revenue": 18208.76250000059,
            "Net_Revenue": 8296.504101562768,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1580860800
        },
        {
            "Gross_Revenue": 86.40000000000012,
            "Net_Revenue": 50.07744000000007,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581552000
        },
        {
            "Gross_Revenue": 475.2720000000033,
            "Net_Revenue": 195.72889140000134,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 327.95700000000204,
            "Net_Revenue": 135.06089152500084,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582243200
        },
        {
            "Gross_Revenue": 140.40000000000018,
            "Net_Revenue": 81.3758400000001,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1580860800
        },
        {
            "Gross_Revenue": 461.10000000001594,
            "Net_Revenue": 352.7415000000122,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 606.059999999996,
            "Net_Revenue": 198.32707439999868,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581552000
        },
        {
            "Gross_Revenue": 170.4,
            "Net_Revenue": 98.76383999999999,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 230.39100000000124,
            "Net_Revenue": 94.8807735750005,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582502400
        },
        {
            "Gross_Revenue": 2433.0899999999747,
            "Net_Revenue": 1108.5766312499884,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581811200
        },
        {
            "Gross_Revenue": 93.88800000000002,
            "Net_Revenue": 40.13712000000001,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1580774400
        },
        {
            "Gross_Revenue": 3945.5000000007744,
            "Net_Revenue": 1604.6348500003148,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 549.599999999998,
            "Net_Revenue": 290.1063599999989,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581033600
        },
        {
            "Gross_Revenue": 109.20000000000017,
            "Net_Revenue": 63.2923200000001,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581120000
        },
        {
            "Gross_Revenue": 614.1239999999945,
            "Net_Revenue": 200.96593775999816,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 9.16320000000001,
            "Net_Revenue": 3.699642000000004,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1582156800
        },
        {
            "Gross_Revenue": 1078.800000000018,
            "Net_Revenue": 569.4445800000094,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1582416000
        },
        {
            "Gross_Revenue": 427.5,
            "Net_Revenue": 242.3925,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1582070400
        },
        {
            "Gross_Revenue": 5.216,
            "Net_Revenue": 2.22984,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581897600
        },
        {
            "Gross_Revenue": 490.5,
            "Net_Revenue": 278.1135,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1580947200
        },
        {
            "Gross_Revenue": 7.824,
            "Net_Revenue": 3.34476,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581811200
        },
        {
            "Gross_Revenue": 1.4352000000000005,
            "Net_Revenue": 0.5794620000000001,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1582502400
        },
        {
            "Gross_Revenue": 485.9999999999968,
            "Net_Revenue": 256.53509999999835,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581552000
        },
        {
            "Gross_Revenue": 23202.707500000848,
            "Net_Revenue": 10571.870292187887,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581552000
        },
        {
            "Gross_Revenue": 17.46000000000001,
            "Net_Revenue": 13.356900000000008,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 225,
            "Net_Revenue": 127.57499999999999,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 696.7799999999789,
            "Net_Revenue": 228.0142871999931,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581033600
        },
        {
            "Gross_Revenue": 6487.17300000144,
            "Net_Revenue": 2671.580020725593,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581033600
        },
        {
            "Gross_Revenue": 49.660000000000174,
            "Net_Revenue": 20.196722000000072,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582675200
        },
        {
            "Gross_Revenue": 553.1999999999981,
            "Net_Revenue": 292.006619999999,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1580947200
        },
        {
            "Gross_Revenue": 1479.7900000001357,
            "Net_Revenue": 601.8305930000552,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581811200
        },
        {
            "Gross_Revenue": 69.60000000000008,
            "Net_Revenue": 40.34016000000005,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1582329600
        },
        {
            "Gross_Revenue": 10.432,
            "Net_Revenue": 4.45968,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 8.315999999999995,
            "Net_Revenue": 2.7213278399999985,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1582675200
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1580947200
        },
        {
            "Gross_Revenue": 0.552,
            "Net_Revenue": 0.22286999999999998,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 2.5392,
            "Net_Revenue": 1.025202,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1581465600
        },
        {
            "Gross_Revenue": 472.6799999999871,
            "Net_Revenue": 192.23895599999474,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1582329600
        },
        {
            "Gross_Revenue": 1460.9649999999986,
            "Net_Revenue": 665.6521781249993,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1582156800
        },
        {
            "Gross_Revenue": 7260.110000002851,
            "Net_Revenue": 2952.686737001159,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 13.920000000000027,
            "Net_Revenue": 10.648800000000021,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1582243200
        },
        {
            "Gross_Revenue": 808.8000000000078,
            "Net_Revenue": 426.92508000000413,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581292800
        },
        {
            "Gross_Revenue": 18.839999999999982,
            "Net_Revenue": 14.412599999999987,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1582416000
        },
        {
            "Gross_Revenue": 2.608,
            "Net_Revenue": 1.11492,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1582416000
        },
        {
            "Gross_Revenue": 62.40000000000005,
            "Net_Revenue": 36.167040000000036,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 44.400000000000006,
            "Net_Revenue": 25.73424,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1582416000
        },
        {
            "Gross_Revenue": 2.445,
            "Net_Revenue": 1.2225,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "COMVIVA"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 2.445,
            "Net_Revenue": 1.2225,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "COMVIVA"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 1757.1539999998677,
            "Net_Revenue": 723.6399460499455,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581638400
        },
        {
            "Gross_Revenue": 498.84000000001737,
            "Net_Revenue": 381.6126000000133,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 44.40000000000002,
            "Net_Revenue": 25.73424000000001,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581638400
        },
        {
            "Gross_Revenue": 155.98800000000014,
            "Net_Revenue": 51.045513120000045,
            "groupBy": {
                "country": "IQ",
                "mno": "ZAIN",
                "aggregator": "CENTILI"
            },
            "time": 1581724800
        },
        {
            "Gross_Revenue": 8.3325,
            "Net_Revenue": 3.9416057999999996,
            "groupBy": {
                "country": "PL",
                "mno": "ORANGE",
                "aggregator": "CENTILI"
            },
            "time": 1580601600
        },
        {
            "Gross_Revenue": 837,
            "Net_Revenue": 474.579,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581811200
        },
        {
            "Gross_Revenue": 1454.7959999999007,
            "Net_Revenue": 599.121362699959,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581552000
        },
        {
            "Gross_Revenue": 672.0000000000026,
            "Net_Revenue": 354.7152000000014,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1582070400
        },
        {
            "Gross_Revenue": 22161.14500000079,
            "Net_Revenue": 10097.308378125363,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 4165.875000000601,
            "Net_Revenue": 1715.6114718752474,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1580688000
        },
        {
            "Gross_Revenue": 434.3999999999973,
            "Net_Revenue": 229.29803999999857,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581984000
        },
        {
            "Gross_Revenue": 5.555,
            "Net_Revenue": 2.530996875,
            "groupBy": {
                "country": "PL",
                "mno": "T-MOBILE",
                "aggregator": "CENTILI"
            },
            "time": 1582675200
        },
        {
            "Gross_Revenue": 870.75,
            "Net_Revenue": 493.71524999999997,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 0.5760000000000001,
            "Net_Revenue": 0.22398197760000005,
            "groupBy": {
                "country": "EG",
                "mno": "VODAFONE",
                "aggregator": "TPAY"
            },
            "time": 1581552000
        },
        {
            "Gross_Revenue": 7.617600000000007,
            "Net_Revenue": 3.0756060000000027,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1582416000
        },
        {
            "Gross_Revenue": 732.0000000000049,
            "Net_Revenue": 386.3862000000026,
            "groupBy": {
                "country": "MY",
                "mno": "DIGI",
                "aggregator": "CENTILI"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 40.80000000000001,
            "Net_Revenue": 23.647680000000012,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1582243200
        },
        {
            "Gross_Revenue": 140.4000000000002,
            "Net_Revenue": 81.37584000000012,
            "groupBy": {
                "country": "MY",
                "mno": "CELCOM",
                "aggregator": "APIGATE"
            },
            "time": 1581379200
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1582675200
        },
        {
            "Gross_Revenue": 393.75,
            "Net_Revenue": 223.25625,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1580860800
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581552000
        },
        {
            "Gross_Revenue": 589.5,
            "Net_Revenue": 334.24649999999997,
            "groupBy": {
                "country": "PL",
                "mno": "PLAY",
                "aggregator": "CENTILI"
            },
            "time": 1581638400
        },
        {
            "Gross_Revenue": 86.06400000000001,
            "Net_Revenue": 36.79236,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1581552000
        },
        {
            "Gross_Revenue": 4505.907000000724,
            "Net_Revenue": 1855.645150275298,
            "groupBy": {
                "country": "MM",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1580947200
        },
        {
            "Gross_Revenue": 7.824,
            "Net_Revenue": 3.34476,
            "groupBy": {
                "country": "KW",
                "mno": "OOREDOO",
                "aggregator": "CENTILI"
            },
            "time": 1582156800
        },
        {
            "Gross_Revenue": 9.715200000000012,
            "Net_Revenue": 3.9225120000000047,
            "groupBy": {
                "country": "MM",
                "mno": "OOREDOO",
                "aggregator": "IMIMOBILE"
            },
            "time": 1581638400
        },
        {
            "groupBy": {
                "country": "DE",
                "mno": "VODAFONE",
                "aggregator": "ONEBIP"
            },
            "time": 1581379200
        },
        {
            "Gross_Revenue": 13.740000000000025,
            "Net_Revenue": 10.51110000000002,
            "groupBy": {
                "country": "BD",
                "mno": "ROBI",
                "aggregator": "CENTILI"
            },
            "time": 1582588800
        },
        {
            "Gross_Revenue": 6477.380000002278,
            "Net_Revenue": 2634.3504460009267,
            "groupBy": {
                "country": "PK",
                "mno": "TELENOR",
                "aggregator": "CENTILI"
            },
            "time": 1581465600
        }
    ]
});

export const revenues = (timeFrame = 30) => {
	let res = [];

	for (let i = 0; i < timeFrame; i++) {
		const obj = {
			Gross_Revenue: Math.round(Math.random() * 1000),
			Net_Revenue: Math.round(Math.random() * 1000),
			groupBy: {
				country: Math.round(Math.random() * 1000),
				product: Math.round(Math.random() * 1000),
				mno: Math.round(Math.random() * 1000),
				aggregator: Math.round(Math.random() * 1000)
			},
			time: moment('2020/11/01').add(i, 'd')
		};

		res = res.concat(obj);
	}

	return {
		data: res
	};
};
