export default () => ({
    "data": [
        {
            "cost": 195765756,
            "groupBy": {
                "country": "MM"
            },
            "time": 1580601600
        },
        {
            "cost": 8323997,
            "groupBy": {
                "country": "MY"
            },
            "time": 1580601600
        },
        {
            "cost": 29764059,
            "groupBy": {
                "country": "PK"
            },
            "time": 1580601600
        },
        {
            "cost": 18268698,
            "groupBy": {
                "country": "KW"
            },
            "time": 1581724800
        },
        {
            "cost": 46087586,
            "groupBy": {
                "country": "MM"
            },
            "time": 1581724800
        },
        {
            "cost": 5659406,
            "groupBy": {
                "country": "MW"
            },
            "time": 1581724800
        },
        {
            "cost": 13004999,
            "groupBy": {
                "country": "PK"
            },
            "time": 1581724800
        },
        {
            "cost": 14952805,
            "groupBy": {
                "country": "KW"
            },
            "time": 1581811200
        },
        {
            "cost": 29316488,
            "groupBy": {
                "country": "MM"
            },
            "time": 1581811200
        },
        {
            "cost": 11342138,
            "groupBy": {
                "country": "MW"
            },
            "time": 1581811200
        },
        {
            "cost": 14830489,
            "groupBy": {
                "country": "PK"
            },
            "time": 1581811200
        },
        {
            "cost": 20873636,
            "groupBy": {
                "country": "MT"
            },
            "time": 1582070400
        },
        {
            "cost": 36362059,
            "groupBy": {
                "country": "MW"
            },
            "time": 1582070400
        },
        {
            "cost": 79552435,
            "groupBy": {
                "country": "MT"
            },
            "time": 1582156800
        },
        {
            "cost": 67953594,
            "groupBy": {
                "country": "MW"
            },
            "time": 1582156800
        },
        {
            "cost": 200992402,
            "groupBy": {
                "country": "MT"
            },
            "time": 1582243200
        },
        {
            "cost": 377251503,
            "groupBy": {
                "country": "MW"
            },
            "time": 1582243200
        },
        {
            "cost": 426214462,
            "groupBy": {
                "country": "MT"
            },
            "time": 1582329600
        },
        {
            "cost": 23823612,
            "groupBy": {
                "country": "PL"
            },
            "time": 1582329600
        },
        {
            "cost": 366301840,
            "groupBy": {
                "country": "MT"
            },
            "time": 1582416000
        },
        {
            "cost": 182895050,
            "groupBy": {
                "country": "MW"
            },
            "time": 1582416000
        },
        {
            "cost": 38401798,
            "groupBy": {
                "country": "PL"
            },
            "time": 1582416000
        },
        {
            "cost": 194412091,
            "groupBy": {
                "country": "MT"
            },
            "time": 1582502400
        },
        {
            "cost": 283569494,
            "groupBy": {
                "country": "MW"
            },
            "time": 1582502400
        },
        {
            "cost": 520104900,
            "groupBy": {
                "country": "PL"
            },
            "time": 1582502400
        },
        {
            "cost": 161401450,
            "groupBy": {
                "country": "MT"
            },
            "time": 1582588800
        },
        {
            "cost": 80907299,
            "groupBy": {
                "country": "MW"
            },
            "time": 1582588800
        },
        {
            "cost": 653267600,
            "groupBy": {
                "country": "PL"
            },
            "time": 1582588800
        }
    ]
})