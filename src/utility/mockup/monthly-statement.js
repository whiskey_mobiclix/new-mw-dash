import Number from "../helpers/number";

export const IP_OWNERS_RECEIVABLE = [
	{
		name: "New users",
		data: [
			{
				time: "Jan 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Feb 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Mar 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Apr 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jun 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jul 2019",
				value: Math.round(Math.random() * 2000),
			},
		]
	},
	{
		name: "Active users",
		eog: false,
		data: [
			{
				time: "Jan 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Feb 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Mar 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Apr 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jun 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jul 2019",
				value: Math.round(Math.random() * 2000),
			},
		]
	},
	{
		name: "Unsubscribed users",
		eog: true,
		data: [
			{
				time: "Jan 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Feb 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Mar 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Apr 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jun 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jul 2019",
				value: Math.round(Math.random() * 2000),
			},
		]
	},
	{
		name: "Total transactions",
		eog: true,
		data: [
			{
				time: "Jan 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Feb 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Mar 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Apr 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jun 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jul 2019",
				value: Math.round(Math.random() * 2000),
			},
		]
	},
	{
		name: "Gross revenue",
		data: [
			{
				time: "Jan 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Feb 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Mar 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Apr 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jun 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jul 2019",
				value: Math.round(Math.random() * 2000),
			},
		]
	},
	{
		name: "Net revenue",
		data: [
			{
				time: "Jan 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Feb 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Mar 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Apr 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jun 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jul 2019",
				value: Math.round(Math.random() * 2000),
			},
		]
	},
	// {
	// 	name: "Taxtation",
	// 	data: [
	// 		{
	// 			time: "Jan 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Feb 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Mar 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Apr 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jun 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jul 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 	]
	// },
	// {
	// 	name: "Marketing spend",
	// 	data: [
	// 		{
	// 			time: "Jan 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Feb 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Mar 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Apr 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jun 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jul 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 	]
	// },
	// {
	// 	name: "Loss Carried Forward",
	// 	data: [
	// 		{
	// 			time: "Jan 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Feb 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Mar 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Apr 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jun 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jul 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 	]
	// },
	// {
	// 	name: "Profit",
	// 	eog: true,
	// 	data: [
	// 		{
	// 			time: "Jan 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Feb 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Mar 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Apr 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jun 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jul 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 	]
	// },
	// {
	// 	name: "Ip owner Receivable",
	// 	data: [
	// 		{
	// 			time: "Jan 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Feb 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Mar 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Apr 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jun 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jul 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 	]
	// },
	// {
	// 	name: "Ip owner Received",
	// 	data: [
	// 		{
	// 			time: "Jan 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Feb 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Mar 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Apr 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jun 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 		{
	// 			time: "Jul 2019",
	// 			value: Math.round(Math.random() * 2000),
	// 		},
	// 	]
	// },
]

export const IP_OWNERS_ROYALTIES = [
	{
		name: "Royalties",
		data: [
			{
				time: "Jan 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Feb 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Mar 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Apr 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jun 2019",
				value: Math.round(Math.random() * 2000),
			},
			{
				time: "Jul 2019",
				value: Math.round(Math.random() * 2000),
			},
		]
	},
]