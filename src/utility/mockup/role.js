export default {
	"role": "C-Level",
	"permissions": [
	    {
	        "resource": "stats/users", 
	        "actions": [
	            {"action": "read", allow: true},
	        ]
	    },
	    {
	        "resource": "stats/transactions", 
	        "actions": [
	            {"action": "read", allow: true},
	        ]
	    },
	    {
	        "resource": "stats/revenues", 
	        "actions": [
	            {"action": "read", allow: true},
	        ]
	    }
	]
}