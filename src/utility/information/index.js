import * as gameon from "./gameon"
import * as vanguard from "./vanguard"

export default {
	gameon,
	vanguard,
}