export const general = {
	about: `Vanguard is a product with popular anime content in Japan. Using Vanguard will bring you moments of relaxation with hundreds of anime videos such as Vanguard, Buddy Fight. Vanguard is provided by Mobiclix Pte. Ltd. The service is £3 per week and your subscription will be renewed automatically. These charges will be due from your pre-pay balance weekly after you have subscribed or added to your monthly mobile bill for this phone. You can unsubscribe by clicking this link here, or alternatively by texting the keyword you received in your welcome or reminder message to the shortcode also contained in your text message. Operator data charges may apply as per your plan. By signing up for and/or using the service you acknowledge and confirm that you have read the Terms & Conditions and Privacy Policy, that you are a resident of the United Kingdom, you are 18 years or older and are the mobile account holder or you have consent from the mobile account holder.<br /><br />If you cancel the subscription within 14 days of the date of subscription you are entitled to a refund of sums paid pursuant to s.37 of the Consumer Contracts (Information, Cancellation and Additional Charges) Regulations 2013. Help desk email: support@vanguard-cardfight.com. Help contact number: 0330 777 2760 Full Terms and Conditions MobiClix Pte LTD is committed to protecting and respecting your privacy. This policy sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us. <br /><br />Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. If you are under 18, please do not send any personal data about yourself to us. MobiClix Pte LTD is located at 1 George St, Singapore 049145 Vanguard Cardfight has a 24 hour free trial period, then costs £3.00 per week, until you Unsubscribe. To Unsubscribe please use our Customer Self-Service portal (http://mobilepaymentshelp.com/) where you can manage your account, find out more about our products and other FAQs. Alternatively, you can unsubscribe by referring to the SMS you received as a welcome or reminder message, and either: Send STOP VAN to 83463. For further support, please contact our support team: 0330 777 2760 Support@ Vanguard-Cardfight.com Vanguard is a product with popular anime content in Japan. Using Vanguard will bring you moments of relaxation with hundreds of anime videos such as Vanguard, Buddy Fight. Data charges may apply.`,
	address: `https://vanguard-cardfight.com`,
	vertical: "SVODs",
	documents: [
		{
			name: "Terms & Conditions",
			url: "https://www.vanguard-cardfight.com/terms-conditions",
		},
		{
			name: "Cookies Policy",
			url: "https://www.vanguard-cardfight.com/cookie-policy",
		},
		{
			name: "Privacy Policy",
			url: "https://www.vanguard-cardfight.com/privacy-policy",
		},
		{
			name: "Help Center",
			url: "https://www.vanguard-cardfight.com/help",
		},
	],
	contacts: [
		{
			name: "Ido Bukin",
			role: "CEO",
			email: "ido@weventure.global",
			permissions: "All permissions"
		},
		{
			name: "Whiskey",
			role: "Developer",
			email: "whiskey.nguyen@weventure.global",
			permissions: "Update dashboard contents & features"
		}
	]
}