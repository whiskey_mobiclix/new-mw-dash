export const general = {
	about: `Hello and welcome to GAME ON! GAME ON is a subscription service that provides subscribers with access to Football Content through the applicable mobile site to compatible handsets (the "Service").<br /><br />The GAME ON Service is provided to you by Perform Media Services Ltd, Sussex House, Plane Tree Crescent, Feltham, TW13 7HE United Kingdom. Tel: (0)20 3372 0600, registered no: 03426471 ("We", "Us", "Our"). We are part of Perform Group (<b><a href="http://www.performgroup.com" target="_blank">http://www.performgroup.com</a></b>) (collectively "Perform").`,
	address: `https://game-on.live`,
	vertical: "Sport",
	documents: [
		{
			name: "Terms & Conditions",
			url: "https://game-on.live/terms-conditions",
		},
		{
			name: "Cookies Policy",
			url: "https://game-on.live/terms-conditions",
		},
		{
			name: "Privacy Policy",
			url: "https://game-on.live/terms-conditions",
		},
		{
			name: "Help Center",
			url: "https://game-on.live/terms-conditions",
		},
	],
	contacts: [
		{
			name: "Ido Bukin",
			role: "CEO",
			email: "ido@weventure.global",
			permissions: "All permissions"
		},
		{
			name: "Whiskey",
			role: "Developer",
			email: "whiskey.nguyen@weventure.global",
			permissions: "Update dashboard contents & features"
		}
	]
}