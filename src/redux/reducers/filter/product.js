export const product = (state = {
	value: "",
}, action) => {
  switch (action.type) {
    case "SET_PRODUCT":
    	return {
    		...state,
    		value: action.payload,
    	}
    default:
    	return state
  }
}
