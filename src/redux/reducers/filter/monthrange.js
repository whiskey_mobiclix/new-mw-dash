export const monthrange = (state = {
	options: [
		{
			name: "Last 3 months",
			value: 3,
		},
		{
			name: "Last 6 months",
			value: 6,
		},
	],
	value: 6,
}, action) => {
  switch (action.type) {
    case "SET_MONTH_RANGE":
    	return {
    		...state,
    		value: action.payload,
    	}
    default:
    	return state
  }
}
