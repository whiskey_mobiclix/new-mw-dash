export const daterange = (state = {
	options: [
		{
			name: "Last 7 days",
			value: 7,
		},
		{
			name: "Last 2 weeks",
			value: 14,	
		},
		{
			name: "Last 30 days",
			value: 30,
		},
		{
			name: "Last 90 days",
			value: 90,
		}
	],
	value: 90,
}, action) => {
  switch (action.type) {
    case "SET_DATE_RANGE":
    	return {
    		...state,
    		value: action.payload,
    	}
   	case "SET_DATE_RANGE_OPTIONS":
    	return {
    		...state,
    		options: action.payload,
    	}

    default:
    	return state
  }
}
