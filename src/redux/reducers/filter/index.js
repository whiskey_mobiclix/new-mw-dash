import { combineReducers } from "redux"
import { daterange } from "./daterange"
import { monthrange } from "./monthrange"
import { product } from "./product"

const dateRangeReducer = combineReducers({
  daterange,
  monthrange,
  product,
})

export default dateRangeReducer
