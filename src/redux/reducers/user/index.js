import { combineReducers } from "redux"
import { data } from "./user"

const userReducer = combineReducers({
  data
})

export default userReducer
