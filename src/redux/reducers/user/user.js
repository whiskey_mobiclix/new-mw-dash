export const data = (state = {}, action) => {
  switch (action.type) {
    case "SET_USER_INFO":
    	return action.payload
    case "UPDATE_USER_INFO":
    	return {}
    default:
    	return state
  }
}
