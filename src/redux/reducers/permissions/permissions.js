export const data = (state = [], action) => {
  switch (action.type) {
    case "SET_PERMISSIONS":
    	return action.payload
    case "UPDATE_PERMISSIONS":
    	return {}
    default:
    	return state
  }
}
