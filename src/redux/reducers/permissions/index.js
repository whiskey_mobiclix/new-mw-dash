import { combineReducers } from "redux"
import { data } from "./permissions"

const permissionsReducer = combineReducers({
  data
})

export default permissionsReducer
