import { combineReducers } from 'redux';
import app from './app';
import customizer from './customizer/';
import auth from './auth/';
import navbar from './navbar/Index';
import user from './user';
import permissions from './permissions';
import sidebar from './sidebar';
import statistics from './statistics';
import filter from './filter';
import userForm from './form/user';

const rootReducer = combineReducers({
	app,
	customizer: customizer,
	auth: auth,
	navbar: navbar,
	user,
	permissions,
	sidebar,
	statistics,
	filter,
	userForm
});

export default rootReducer;
