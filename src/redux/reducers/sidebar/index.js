import { combineReducers } from "redux"
import { data } from "./sidebar"

const sidebarReducer = combineReducers({
  data
})

export default sidebarReducer
