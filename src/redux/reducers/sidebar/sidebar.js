export const data = (
	state = {
		header: {
			logo: "",
			name: "",
            url: "",
		},
		items: [],
	},
	action
) => {
  switch (action.type) {
    case "SET_SIDEBAR_ITEMS":
    	return {
    		...state,
    		items: action.payload,
    	}
    case "SET_SIDEBAR_HEADER":
    	return {
    		...state,
    		header: action.payload,
    	}
    default:
    	return state
  }
}
