const init = {
	editing: false,
	email: '',
	name: '',
	password: '',
	confirmPassword: ''
};

export const data = (
	state = {
		init,
		current: init
	},
	action
) => {
	switch (action.type) {
		case 'SET_USER_FORM_DATA':
			return {
				current: action.payload,
				init: state.init
			};
		default:
			return state;
	}
};
