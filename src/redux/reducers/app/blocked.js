export const blocked = (state = false, action) => {
  switch (action.type) {
    case "BLOCK_APP":
    	return true
    case "UNBLOCK_APP":
    	return false
    default:
    	return state
  }
}
