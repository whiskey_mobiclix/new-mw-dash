export const config = (state = {}, action) => {
  switch (action.type) {
    case "SET_APP_CONFIG":
    	return action.payload
    default:
    	return state
  }
}
