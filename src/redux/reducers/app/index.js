import { combineReducers } from "redux"
import { blocked } from "./blocked"
import { config } from "./config"

const appReducer = combineReducers({
  locked: blocked,
  config,
})

export default appReducer
