import { combineReducers } from 'redux';
import users from './users';
import transactions from './transactions';
import revenue from './revenue';
import marketing from "./marketing";
import {
	usersMonthlyStatement,
	revenueMonthlyStatement,
	transactionsMonthlyStatement
} from './monthly-statement';

const statisticsReducer = combineReducers({
	users,
	transactions,
	revenue,
	marketing,
	usersMonthlyStatement,
	revenueMonthlyStatement,
	transactionsMonthlyStatement
});

export default statisticsReducer;
