export default (
	state = {
		fetching: true,
		rawData: [],
		data: [],
		rawPrevious: [],
		previous: [],
	},
	action
) => {
	switch (action.type) {
		case 'GET_MARKETING_STATISTICS_DATA':
			return {
				...state,
				fetching: true
			};
		case 'SET_MARKETING_STATISTICS_DATA':
			return {
				...state,
				fetching: false,
				rawData: action.payload.data,
				data: action.payload.data,
				rawPrevious: action.payload.previous,
				previous: action.payload.previous,
			};
		case 'FILTER_MARKETING_STATISTICS_DATA':
			return {
				...state,
				data: action.payload.data,
				previous: action.payload.previous,
			}
		default:
			return state;
	}
};
