export default (
	state = {
		fetching: true,
		rawData: [],
		data: [],
		rawPrevious: [],
		previous: []
	},
	action
) => {
	switch (action.type) {
		case 'GET_USERS_STATISTICS_DATA':
			return {
				...state,
				fetching: true
			};
		case 'SET_USERS_STATISTICS_DATA':
			return {
				...state,
				fetching: false,
				rawData: action.payload.data,
				data: action.payload.data,
				rawPrevious: action.payload.previous,
				previous: action.payload.previous
			};
		case 'FILTER_USERS_STATISTICS_DATA':
			return {
				...state,
				data: action.payload.data,
				previous: action.payload.previous,
			}
		default:
			return state
	}
};
