export const usersMonthlyStatement = (
	state = {
		fetching: true,
		rawData: [],
		data: [],
		rawPrevious: [],
		previous: []
	},
	action
) => {
	switch (action.type) {
		case 'GET_USERS_MONTHLY_STATEMENT':
			return {
				...state,
				fetching: true
			};
		case 'SET_USERS_MONTHLY_STATEMENT':
			return {
				...state,
				fetching: false,
				data: action.payload.data,
				rawData: action.payload.rawData,
				previous: action.payload.previous,
				rawPrevious: action.payload.rawPrevious,
			};
		default:
			return state;
	}
};

export const revenueMonthlyStatement = (
	state = {
		fetching: true,
		data: [],
		previous: []
	},
	action
) => {
	switch (action.type) {
		case 'GET_REVENUE_MONTHLY_STATEMENT':
			return {
				...state,
				fetching: true
			};
		case 'SET_REVENUE_MONTHLY_STATEMENT':
			return {
				...state,
				fetching: false,
				rawData: action.payload.rawData,
				data: action.payload.data,
				rawPrevious: action.payload.rawPrevious,
				previous: action.payload.previous
			};
		default:
			return state;
	}
};

export const transactionsMonthlyStatement = (
	state = {
		fetching: true,
		data: [],
		previous: []
	},
	action
) => {
	switch (action.type) {
		case 'GET_TRANSACTIONS_MONTHLY_STATEMENT':
			return {
				...state,
				fetching: true
			};
		case 'SET_TRANSACTIONS_MONTHLY_STATEMENT':
			return {
				...state,
				fetching: false,
				rawData: action.payload.rawData,
				rawPrevious: action.payload.rawPrevious,
				data: action.payload.data,
				previous: action.payload.previous
			};
		default:
			return state;
	}
};
