import Request from '../../../utility/request';
import Date from '../../../utility/helpers/date';

export const getRevenueStatistics = filters => {
	const params = {
		groups: ['country', 'mno', 'aggregator', 'product'],
		metrics: ['gross_revenue', 'Net_revenue']
	};

	if (filters.product) {
		params.filters = {
			product: filters.product.toUpperCase()
		};
	}

	return new Promise((resolve, reject) => {
		Request.POST(`/v1/stats/revenues`, {
			...params,
			from_time: Date.toUnix(filters.dates.current.from),
			to_time: Date.toUnix(filters.dates.current.to)
		})
			.then(res => {
				Request.POST(`/v1/stats/revenues`, {
					...params,
					from_time: Date.toUnix(filters.dates.previous.from),
					to_time: Date.toUnix(filters.dates.previous.to)
				})
					.then(resp => {
						resolve({
							data: res.data.data || [],
							previous: resp.data.data || []
						});
					})
					.catch(err => {
						resolve({
							data: [],
							previous: []
						});
					});
			})
			.catch(e => {
				resolve({
					data: [],
					previous: []
				});
			});
	});
};

export const setRevenueStatisticsData = data => dispatch => {
	return dispatch({
		type: 'FILTER_REVENUE_STATISTICS_DATA',
		payload: data
	});
};
