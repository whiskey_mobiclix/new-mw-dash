import { getUsersStatistics } from './users';
import { getRevenueStatistics } from './revenue';
import { getTransactionsStatistics } from './transactions';
import { getMarketingStatistics } from './marketing';

import Data from '../../../utility/helpers/data';

export const getStatisticsData = filters => dispatch => {
	dispatch({
		type: 'BLOCK_APP'
	});

	return new Promise((resolve, reject) => {
		getUsersStatistics(filters)
			.then(users => {
				dispatch({
					type: 'SET_USERS_STATISTICS_DATA',
					payload: users
				});
			})
			.finally(() => {
				getRevenueStatistics(filters)
					.then(revenue => {
						dispatch({
							type: 'SET_REVENUE_STATISTICS_DATA',
							payload: revenue
						});
					})
					.finally(() => {
						getTransactionsStatistics(filters)
							.then(transactions => {
								dispatch({
									type: 'SET_TRANSACTIONS_STATISTICS_DATA',
									payload: transactions
								});
							})
							.finally(() => {
								getMarketingStatistics(filters)
									.then(marketing => {
										dispatch({
											type: 'SET_MARKETING_STATISTICS_DATA',
											payload: marketing
										});
									})
									.finally(() => {
										dispatch({
											type: 'UNBLOCK_APP'
										});
									});
							});
					});
			});
	});
};

export const getMonthlyStatementData = filters => dispatch => {
	dispatch({
		type: 'BLOCK_APP'
	});

	return new Promise((resolve, reject) => {
		getUsersStatistics(filters)
			.then(users => {
				dispatch({
					type: 'SET_USERS_MONTHLY_STATEMENT',
					payload: {
						rawData: users.data,
						data: Data.groupByMonths(users.data),
						rawPrevious: users.previous,
						previous: Data.groupByMonths(users.previous)
					}
				});
			})
			.finally(() => {
				getRevenueStatistics(filters)
					.then(revenue => {
						dispatch({
							type: 'SET_REVENUE_MONTHLY_STATEMENT',
							payload: {
								rawData: revenue.data,
								data: Data.groupByMonths(revenue.data),
								rawPrevious: revenue.previous,
								previous: Data.groupByMonths(revenue.previous)
							}
						});
					})
					.finally(() => {
						getTransactionsStatistics(filters)
							.then(transactions => {
								dispatch({
									type: 'SET_TRANSACTIONS_MONTHLY_STATEMENT',
									payload: {
										rawData: transactions.data,
										data: Data.groupByMonths(transactions.data),
										rawPrevious: transactions.previous,
										previous: Data.groupByMonths(transactions.previous)
									}
								});
							})
							.finally(() => {
								dispatch({
									type: 'UNBLOCK_APP'
								});
								// getMarketingStatistics(filters)
								// 	.then(marketing => {
								// 		dispatch({
								// 			type: 'SET_MARKETING_STATISTICS_DATA',
								// 			payload: marketing
								// 		});
								// 	})
								// 	.finally(() => {
								// 		dispatch({
								// 			type: 'UNBLOCK_APP'
								// 		});
								// 	});
							});
					});
			});
	});
};