import USERS from '../../../utility/mockup/statistics/users';
import Request from "../../../utility/request"
import axios from "axios"
import Date from "../../../utility/helpers/date"

export const getUsersStatistics = filters => {
	let params = {
	  "groups": [
	    "country",
	    "mno",
	    "product",
	  ],
	  "metrics": [
	    "new_users",
	    "new_paid_users",
	    "churn_users",
	    "new_active_users",
	    "renewal_users",
	  ],
	}

	if(filters.product){
		params.filters = {
			product: filters.product.toUpperCase(),
		}
	}

	return new Promise((resolve, reject) => {
		Request.POST(`/v1/stats/users`, {
		  ...params,
		  "from_time": Date.toUnix(filters.dates.current.from),
		  "to_time": Date.toUnix(filters.dates.current.to),
		}).then(res => {
			Request.POST(`/v1/stats/users`, {
			  ...params,
			  "from_time": Date.toUnix(filters.dates.previous.from),
			  "to_time": Date.toUnix(filters.dates.previous.to),
			}).then(resp => {
				resolve({
					data: res.data.data || [],
					previous: resp.data.data || [],
				});
			}).catch(err => {
				resolve({
					data: [],
					previous: []
				});
			})
		}).catch(e => {
			resolve({
				data: [],
				previous: []
			});
		});
	});
};

export const setUsersStatisticsData = data => dispatch => {
	return dispatch({
		type: "FILTER_USERS_STATISTICS_DATA",
		payload: data,
	})
}