import MARKETING from '../../../utility/mockup/statistics/marketing';
import Request from '../../../utility/request';
import Date from '../../../utility/helpers/date';

export const getMarketingStatistics = filters => {
	const params = {
		groups: ['country', 'product'],
		metrics: ['cost']
	};

	if (filters.product) {
		params.filters = {
			product: filters.product.toUpperCase()
		};
	}

	return new Promise((resolve, reject) => {
		Request.POST(`/v1/stats/marketing-spends`, {
			...params,
			from_time: Date.toUnix(filters.dates.current.from),
			to_time: Date.toUnix(filters.dates.current.to)
		})
			.then(res => {
				Request.POST(`/v1/stats/marketing-spends`, {
					...params,
					from_time: Date.toUnix(filters.dates.previous.from),
					to_time: Date.toUnix(filters.dates.previous.to)
				})
					.then(resp => {
						resolve({
							data: res.data.data || [],
							previous: resp.data.data || []
						});
					})
					.catch(err => {
						resolve({
							data: [],
							previous: []
						});
					});
			})
			.catch(e => {
				resolve({
					data: [],
					previous: []
				});
			});
	});
};

export const setMarketingStatisticsData = data => dispatch => {
	return dispatch({
		type: "FILTER_MARKETING_STATISTICS_DATA",
		payload: data,
	})
}