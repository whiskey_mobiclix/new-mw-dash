import TRANSACTION from '../../../utility/mockup/statistics/transactions';
import Request from '../../../utility/request';
import Date from '../../../utility/helpers/date';

export const getTransactionsStatistics = filters => {
	const params = {
		groups: ['country', 'mno', 'product'],
		metrics: ['success_trans', 'total_trans']
	};

	if (filters.product) {
		params.filters = {
			product: filters.product.toUpperCase()
		};
	}

	return new Promise((resolve, reject) => {
		Request.POST(`/v1/stats/transactions`, {
			...params,
			from_time: Date.toUnix(filters.dates.current.from),
			to_time: Date.toUnix(filters.dates.current.to)
		})
			.then(res => {
				Request.POST(`/v1/stats/transactions`, {
					...params,
					from_time: Date.toUnix(filters.dates.previous.from),
					to_time: Date.toUnix(filters.dates.previous.to)
				})
					.then(resp => {
						resolve({
							data: res.data.data || [],
							previous: resp.data.data || []
						});
					})
					.catch(err => {
						resolve({
							data: [],
							previous: []
						});
					});
			})
			.catch(e => {
				resolve({
					data: [],
					previous: []
				});
			});
	});
};

export const setTransactionsStatisticsData = data => dispatch => {
	return dispatch({
		type: 'FILTER_TRANSACTIONS_STATISTICS_DATA',
		payload: data
	});
};
