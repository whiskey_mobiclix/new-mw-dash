// import UserModel from "../../../models/User";
import { Cookies } from "../../../utility/helpers";
import Request from "../../../utility/request";
// import FAKE_ROLE from "../../../utility/mockup/role";
import USER from "../../../utility/mockup/me"
import ROLE from "../../../utility/mockup/role"
import { getUserById } from "../form/user"

export const getRoleDetails = token => {
	return new Promise((resolve, reject) => {
		// Request.GET("/v1/users/me/roles", token).then(res => {
		// 	resolve(ROLE)
		// }).catch(e => {
		// 	reject(e)
		// })

		resolve(ROLE)
	})
}

export const getAccountInfo = (token, role) => {
	return new Promise((resolve, reject) => {
		Request.GET("/v1/users/me", token).then(res => {
			resolve(res.data)
		}).catch(e => {
			reject(e)
		})
	})
}

export const auth = (username, password) => dispatch => {
	if(!username && !password){
		return new Promise((resolve, reject) => {
			getAccountInfo().then(resp => {
				const user = resp.user;

				if(user.group === "USER_GROUP_PARTNER"){
					if(user.managed_product_code){
						dispatch({
							type: "SET_PRODUCT",
							payload: user.managed_product_code || "",
						})
					} else {
						getUserById(user.managed_by).then(parent => {
							dispatch({
								type: "SET_PRODUCT",
								payload: parent.user.managed_product_code || "none",
							})
						}).catch(error => {
							dispatch({
								type: "SET_PRODUCT",
								payload: "none"
							})	
						})
					}
				}

				getRoleDetails().then(role => {
					dispatch({
						type: "SET_PERMISSIONS",
						payload: role.permissions,
					})

					resolve(user);
				}).catch(() => {
					reject();
				}).finally(() => {
					setTimeout(() => {
						dispatch({
							type: "SET_USER_INFO",
							payload: user,
						});
					}, 500)
				})
			}).catch(err => {
				reject(err);
			});
		})
	}

	return new Promise((resolve, reject) => {
		Request.POST(`/v1/login/basic`, {
			email: username,
			password,
		}).then(res => {
			const token = res.data.access_token

			Cookies.set("access_token", token, 86400 * 30 * 1000).then(() => {
				getAccountInfo(token).then(resp => {
					const user = resp.user;

					if(user.group === "USER_GROUP_PARTNER"){
						if(user.managed_product_code){
							dispatch({
								type: "SET_PRODUCT",
								payload: user.managed_product_code || "",
							})
						} else {
							getUserById(user.managed_by, token).then(parent => {
								dispatch({
									type: "SET_PRODUCT",
									payload: parent.user.managed_product_code || "none",
								})
							}).catch(error => {
								dispatch({
									type: "SET_PRODUCT",
									payload: "none"
								})	
							})
						}
					}

					getRoleDetails(token).then(role => {
						dispatch({
							type: "SET_PERMISSIONS",
							payload: role.permissions,
						})

						resolve(user);
					}).catch(() => {
						reject();
					}).finally(() => {
						setTimeout(() => {
							dispatch({
								type: "SET_USER_INFO",
								payload: user,
							});
						}, 500)
					})
				}).catch(err => {
					reject(err);
				});
			})
		}).catch(e => {
			reject(e);
		})
	});
}

export const out = () => dispatch => {
	return new Promise(resolve => {
		Cookies.delete("access_token").then(() => {
			dispatch({
				type: "UPDATE_USER_INFO",
				payload: null,
			});

			resolve();
		});
	})
}