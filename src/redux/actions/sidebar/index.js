export const setSidebarItems = items => dispatch => {
	return dispatch({
		type: "SET_SIDEBAR_ITEMS",
		payload: items
	})
}

export const setSidebarHeader = data => dispatch => {
	return dispatch({
		type: "SET_SIDEBAR_HEADER",
		payload: data
	})
}