import Request from "../../../utility/request"

// export const lock = () => dispatch => {
// 	return dispatch({
// 		type: "BLOCK_APP"
// 	})
// }

// export const unlock = () => dispatch => {
// 	return dispatch({
// 		type: "UNBLOCK_APP"
// 	})
// }

export const getConfig = () => dispatch => {
	return new Promise((resolve, reject) => {
		Request.GET(`/v1/products`).then(res => {
			dispatch({
				type: "SET_APP_CONFIG",
				payload: res.data.data,
			})
		}).catch(e => {
			
		})
	})
}