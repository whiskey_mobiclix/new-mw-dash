import Request from "../../../utility/request"
import Cookies from "../../../utility/helpers/cookies"

export const getManagedUsers = () => {
	return new Promise((resolve, reject) => {
		// Request.GET(`/v1/roles`)

		Request.GET(`/v1/users/me/managed_users`, Cookies.get("access_token")).then(res => {
			resolve(res.data)
		}).catch(e => {
			reject(e)
		})
	})
}

export const createNewUser = data => {
	return new Promise((resolve, reject) => {
		const { email, name, password, product } = data

		Request.POST(`/v1/users`, {
			email,
			name,
			password,
		}).then(res => {
			Request.PUT(`/v1/users/me/managed_product`, {
				userID: res.data.user.id,
				product_code: product,
			}).then(resp => {
				resolve()
			}).catch(err => {
				reject(err)
			})
		}).catch(e => {
			reject(e)
		})
	})
}