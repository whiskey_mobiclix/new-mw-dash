export const setDateRange = value => dispatch => {
	return dispatch({
		type: "SET_DATE_RANGE",
		payload: value,
	})
}

export const setDateRangeOptions = options => dispatch => {
	return dispatch({
		type: "SET_DATE_RANGE_OPTIONS",
		payload: options,
	})
}

export const setMonthRange = value => dispatch => {
	return dispatch({
		type: "SET_MONTH_RANGE",
		payload: value,
	})
}

export const setProduct = value => dispatch => {
	return dispatch({
		type: "SET_PRODUCT",
		payload: value,
	})
}