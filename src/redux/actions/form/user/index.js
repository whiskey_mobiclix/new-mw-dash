import Request from '../../../../utility/request';

export const setUserFormData = data => dispatch => {
	return dispatch({
		type: 'SET_USER_FORM_DATA',
		payload: data
	});
};

export const createUser = data => {
	return new Promise((resolve, reject) => {
		const { name, email, password } = data;

		Request.POST(`/v1/users`, {
			name,
			email,
			password
		})
			.then(res => {
				resolve(res.data);
			})
			.catch(e => {
				reject(e);
			});
	});
};

export const getUserById = (id, token) => {
	return new Promise((resolve, reject) => {
		Request.GET(`/v1/users/${id}`, token)
			.then(res => {
				resolve(res.data);
			})
			.catch(e => {
				reject(e);
			});
	});
};

export const changePassword = (id, newPassword) => {
	return new Promise((resolve, reject) => {
		Request.PUT(`/v1/users/${id}/password`, {
			userID: id,
			password: newPassword
		})
			.then(res => {
				resolve(res.data);
			})
			.catch(e => {
				reject(e);
			});
	});
};

export const changeStatus = (id, newStatus) => {
	return new Promise((resolve, reject) => {
		Request.PUT(`/v1/users/${id}/status`, {
			userID: id,
			status: newStatus,
		})
			.then(res => {
				resolve(res.data);
			})
			.catch(e => {
				reject(e);
			});
	});
};
