export default data => {
	return {
		...data,
		id: data.user_id,
	}
}