import React from "react"
import { connect } from "react-redux"
import { Input, FormGroup } from "reactstrap"
import { Search } from "react-feather"
import AutoComplete from "../../../components/@vuexy/autoComplete/AutoCompleteComponent"
import classnames from "classnames"
import { history } from "../../../history"

class NavbarSearch extends React.Component{
	render(){
		let suggestions = []
		this.props.sidebar.data.items.forEach(o => {
			if(o.title){
				if(o.children){
					suggestions = suggestions.concat(o.children.map(oo => ({title: oo.title, link: oo.navLink})))
				}

				else {
					suggestions = suggestions.concat({title: o.title, link: o.navLink})
				}
			}
		})

		return (
			<div className="navbar-search d-flex align-items-center">
				<FormGroup className="has-icon-left position-relative mb-0">
	              	<AutoComplete
	                  suggestions={suggestions}
	                  className="form-control"
	                  filterKey="title"
	                  suggestionLimit={5}
	                  placeholder="Quick navigation..."
	                  customRender={(suggestion,
	                  	i,
	                  	filteredData,
				        activeSuggestion,
				        onSuggestionItemClick,
				        onSuggestionItemHover
				       ) => (
				          <li key={i}
				          	className={classnames("suggestion-item", {
				              active:
				                filteredData.indexOf(suggestion) === activeSuggestion
				            })}
				            onMouseEnter={() =>
				              onSuggestionItemHover(filteredData.indexOf(suggestion))
				            }
				            onClick={e => {
				              onSuggestionItemClick(null, e);
				              history.push(suggestion.link)
				            }}>
				          	{suggestion.title}
				          </li>
				        )}
	                />

	              	<div className="form-control-position">
		                  <button style={{
		                  	border: 0,
		                  	background: 0,
		                  	padding: 0,
		                  	outline: 0,
		                  	color: "#aaa",
		                  }} onClick={() => {
		                  }}>
		                  	<Search size={20}/>
		                  </button>
		              </div>
	            </FormGroup>
			</div>
		)
	}
}

export default connect(
	state => ({
		sidebar: state.sidebar,
	}),
)(NavbarSearch)