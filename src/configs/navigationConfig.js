import { history } from "../history";
import Menu from "../utility/helpers/menu";

const navigationConfig = Menu.list(history.location.pathname);

export default navigationConfig
